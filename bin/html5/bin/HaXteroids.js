(function () { "use strict";
var $_, $hxClasses = {}, $estr = function() { return js.Boot.__string_rec(this,''); }
function $extend(from, fields) {
	function inherit() {}; inherit.prototype = from; var proto = new inherit();
	for (var name in fields) proto[name] = fields[name];
	return proto;
}
var ApplicationMain = function() { }
$hxClasses["ApplicationMain"] = ApplicationMain;
ApplicationMain.__name__ = ["ApplicationMain"];
ApplicationMain.completed = null;
ApplicationMain.preloader = null;
ApplicationMain.total = null;
ApplicationMain.loaders = null;
ApplicationMain.urlLoaders = null;
ApplicationMain.main = function() {
	ApplicationMain.completed = 0;
	ApplicationMain.loaders = new Hash();
	ApplicationMain.urlLoaders = new Hash();
	ApplicationMain.total = 0;
	ApplicationMain.preloader = new NMEPreloader();
	jeash.Lib.jeashGetCurrent().addChild(ApplicationMain.preloader);
	ApplicationMain.preloader.onInit();
	var loader = new jeash.display.Loader();
	ApplicationMain.loaders.set("img/ship.png",loader);
	ApplicationMain.total++;
	if(ApplicationMain.total == 0) ApplicationMain.begin(); else {
		var $it0 = ApplicationMain.loaders.keys();
		while( $it0.hasNext() ) {
			var path = $it0.next();
			var loader1 = ApplicationMain.loaders.get(path);
			loader1.contentLoaderInfo.addEventListener("complete",ApplicationMain.loader_onComplete);
			loader1.load(new jeash.net.URLRequest(path));
		}
		var $it1 = ApplicationMain.urlLoaders.keys();
		while( $it1.hasNext() ) {
			var path = $it1.next();
			var urlLoader = ApplicationMain.urlLoaders.get(path);
			urlLoader.addEventListener("complete",ApplicationMain.loader_onComplete);
			urlLoader.load(new jeash.net.URLRequest(path));
		}
	}
}
ApplicationMain.begin = function() {
	ApplicationMain.preloader.addEventListener(jeash.events.Event.COMPLETE,ApplicationMain.preloader_onComplete);
	ApplicationMain.preloader.onLoaded();
}
ApplicationMain.loader_onComplete = function(event) {
	ApplicationMain.completed++;
	ApplicationMain.preloader.onUpdate(ApplicationMain.completed,ApplicationMain.total);
	if(ApplicationMain.completed == ApplicationMain.total) ApplicationMain.begin();
}
ApplicationMain.preloader_onComplete = function(event) {
	ApplicationMain.preloader.removeEventListener(jeash.events.Event.COMPLETE,ApplicationMain.preloader_onComplete);
	jeash.Lib.jeashGetCurrent().removeChild(ApplicationMain.preloader);
	ApplicationMain.preloader = null;
	if(Reflect.field(com.damir.haxteroids.Main,"main") == null) jeash.Lib.jeashGetCurrent().addChild(new com.damir.haxteroids.Main()); else Reflect.field(com.damir.haxteroids.Main,"main").apply(com.damir.haxteroids.Main,[]);
}
ApplicationMain.prototype = {
	__class__: ApplicationMain
}
var EReg = function(r,opt) {
	opt = opt.split("u").join("");
	this.r = new RegExp(r,opt);
};
$hxClasses["EReg"] = EReg;
EReg.__name__ = ["EReg"];
EReg.prototype = {
	r: null
	,match: function(s) {
		this.r.m = this.r.exec(s);
		this.r.s = s;
		return this.r.m != null;
	}
	,matched: function(n) {
		return this.r.m != null && n >= 0 && n < this.r.m.length?this.r.m[n]:(function($this) {
			var $r;
			throw "EReg::matched";
			return $r;
		}(this));
	}
	,replace: function(s,by) {
		return s.replace(this.r,by);
	}
	,__class__: EReg
}
var Hash = function() {
	this.h = { };
};
$hxClasses["Hash"] = Hash;
Hash.__name__ = ["Hash"];
Hash.prototype = {
	h: null
	,set: function(key,value) {
		this.h["$" + key] = value;
	}
	,get: function(key) {
		return this.h["$" + key];
	}
	,keys: function() {
		var a = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) a.push(key.substr(1));
		}
		return a.iterator();
	}
	,__class__: Hash
}
var Selection = function() { }
$hxClasses["Selection"] = Selection;
Selection.__name__ = ["Selection"];
Selection.prototype = {
	anchorNode: null
	,anchorOffset: null
	,focusNode: null
	,focusOffset: null
	,isCollapsed: null
	,rangeCount: null
	,collapse: null
	,collapseToStart: null
	,collapseToEnd: null
	,selectAllChildren: null
	,deleteFromDocument: null
	,getRangeAt: null
	,addRange: null
	,removeRange: null
	,removeAllRanges: null
	,stringifier: null
	,__class__: Selection
}
var MessagePortArray = function() { }
$hxClasses["MessagePortArray"] = MessagePortArray;
MessagePortArray.__name__ = ["MessagePortArray"];
MessagePortArray.prototype = {
	__class__: MessagePortArray
}
var MessagePort = function() { }
$hxClasses["MessagePort"] = MessagePort;
MessagePort.__name__ = ["MessagePort"];
MessagePort.prototype = {
	postMessage: null
	,start: null
	,close: null
	,onmessage: null
	,__class__: MessagePort
}
var IntHash = function() {
	this.h = { };
};
$hxClasses["IntHash"] = IntHash;
IntHash.__name__ = ["IntHash"];
IntHash.prototype = {
	h: null
	,set: function(key,value) {
		this.h[key] = value;
	}
	,__class__: IntHash
}
var List = function() {
	this.length = 0;
};
$hxClasses["List"] = List;
List.__name__ = ["List"];
List.prototype = {
	h: null
	,q: null
	,length: null
	,add: function(item) {
		var x = [item];
		if(this.h == null) this.h = x; else this.q[1] = x;
		this.q = x;
		this.length++;
	}
	,__class__: List
}
var jeash = {}
jeash.events = {}
jeash.events.IEventDispatcher = function() { }
$hxClasses["jeash.events.IEventDispatcher"] = jeash.events.IEventDispatcher;
jeash.events.IEventDispatcher.__name__ = ["jeash","events","IEventDispatcher"];
jeash.events.IEventDispatcher.prototype = {
	addEventListener: null
	,dispatchEvent: null
	,hasEventListener: null
	,removeEventListener: null
	,willTrigger: null
	,__class__: jeash.events.IEventDispatcher
}
jeash.events.EventDispatcher = function(target) {
	if(target != null) this.jeashTarget = target; else this.jeashTarget = this;
	this.jeashEventMap = [];
};
$hxClasses["jeash.events.EventDispatcher"] = jeash.events.EventDispatcher;
jeash.events.EventDispatcher.__name__ = ["jeash","events","EventDispatcher"];
jeash.events.EventDispatcher.__interfaces__ = [jeash.events.IEventDispatcher];
jeash.events.EventDispatcher.compareListeners = function(l1,l2) {
	return l1.mPriority == l2.mPriority?0:l1.mPriority > l2.mPriority?-1:1;
}
jeash.events.EventDispatcher.prototype = {
	jeashTarget: null
	,jeashEventMap: null
	,getList: function(type) {
		return this.jeashEventMap[type];
	}
	,setList: function(type,list) {
		this.jeashEventMap[type] = list;
	}
	,existList: function(type) {
		return this.jeashEventMap[type] != undefined;
	}
	,addEventListener: function(type,inListener,useCapture,inPriority,useWeakReference) {
		var capture = useCapture == null?false:useCapture;
		var priority = inPriority == null?0:inPriority;
		var list = this.getList(type);
		if(!this.existList(type)) {
			list = [];
			this.setList(type,list);
		}
		list.push(new jeash.events.Listener(inListener,capture,priority));
		list.sort(jeash.events.EventDispatcher.compareListeners);
	}
	,dispatchEvent: function(event) {
		if(event.target == null) event.target = this.jeashTarget;
		var capture = event.eventPhase == jeash.events.EventPhase.CAPTURING_PHASE;
		if(this.existList(event.type)) {
			var list = this.getList(event.type);
			var idx = 0;
			while(idx < list.length) {
				var listener = list[idx];
				if(listener.mUseCapture == capture) {
					listener.dispatchEvent(event);
					if(event.jeashGetIsCancelledNow()) return true;
				}
				if(idx < list.length && listener != list[idx]) {
				} else idx++;
			}
			return true;
		}
		return false;
	}
	,hasEventListener: function(type) {
		return this.existList(type);
	}
	,removeEventListener: function(type,listener,inCapture) {
		if(!this.existList(type)) return;
		var list = this.getList(type);
		var capture = inCapture == null?false:inCapture;
		var _g1 = 0, _g = list.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(list[i].Is(listener,capture)) {
				list.splice(i,1);
				return;
			}
		}
	}
	,toString: function() {
		return "[ " + this.__name__ + " ]";
	}
	,willTrigger: function(type) {
		return this.hasEventListener(type);
	}
	,__class__: jeash.events.EventDispatcher
}
jeash.display = {}
jeash.display.IBitmapDrawable = function() { }
$hxClasses["jeash.display.IBitmapDrawable"] = jeash.display.IBitmapDrawable;
jeash.display.IBitmapDrawable.__name__ = ["jeash","display","IBitmapDrawable"];
jeash.display.IBitmapDrawable.prototype = {
	drawToSurface: null
	,__class__: jeash.display.IBitmapDrawable
}
jeash.display.DisplayObject = function() {
	this.parent = null;
	jeash.events.EventDispatcher.call(this,null);
	this.jeashSetX(this.jeashSetY(0));
	this.jeashScaleX = this.jeashScaleY = 1.0;
	this.alpha = 1.0;
	this.jeashSetRotation(0.0);
	this.__swf_depth = 0;
	this.mMatrix = new jeash.geom.Matrix();
	this.mFullMatrix = new jeash.geom.Matrix();
	this.mMask = null;
	this.mMaskingObj = null;
	this.mBoundsRect = new jeash.geom.Rectangle();
	this.mBoundsDirty = true;
	this.mGraphicsBounds = null;
	this.mMaskHandle = null;
	this.name = "DisplayObject " + jeash.display.DisplayObject.mNameID++;
	this.jeashFilters = [];
	this.jeashSetVisible(true);
};
$hxClasses["jeash.display.DisplayObject"] = jeash.display.DisplayObject;
jeash.display.DisplayObject.__name__ = ["jeash","display","DisplayObject"];
jeash.display.DisplayObject.__interfaces__ = [jeash.display.IBitmapDrawable];
jeash.display.DisplayObject.__super__ = jeash.events.EventDispatcher;
jeash.display.DisplayObject.prototype = $extend(jeash.events.EventDispatcher.prototype,{
	x: null
	,y: null
	,scaleX: null
	,scaleY: null
	,rotation: null
	,alpha: null
	,name: null
	,width: null
	,height: null
	,visible: null
	,mouseX: null
	,mouseY: null
	,parent: null
	,stage: null
	,loaderInfo: null
	,__swf_depth: null
	,transform: null
	,mBoundsDirty: null
	,mMtxChainDirty: null
	,mMtxDirty: null
	,mBoundsRect: null
	,mGraphicsBounds: null
	,mScale9Grid: null
	,mMatrix: null
	,mFullMatrix: null
	,jeashX: null
	,jeashY: null
	,jeashScaleX: null
	,jeashScaleY: null
	,jeashRotation: null
	,jeashVisible: null
	,mScrollRect: null
	,mOpaqueBackground: null
	,mMask: null
	,mMaskingObj: null
	,mMaskHandle: null
	,jeashFilters: null
	,toString: function() {
		return this.name;
	}
	,jeashDoAdded: function(inObj) {
		if(inObj == this) {
			var evt = new jeash.events.Event(jeash.events.Event.ADDED,true,false);
			evt.target = inObj;
			this.dispatchEvent(evt);
		}
		if(this.jeashIsOnStage()) {
			var evt = new jeash.events.Event(jeash.events.Event.ADDED_TO_STAGE,false,false);
			evt.target = inObj;
			this.dispatchEvent(evt);
		}
	}
	,jeashDoRemoved: function(inObj) {
		if(inObj == this) {
			var evt = new jeash.events.Event(jeash.events.Event.REMOVED,true,false);
			evt.target = inObj;
			this.dispatchEvent(evt);
		}
		var evt = new jeash.events.Event(jeash.events.Event.REMOVED_FROM_STAGE,false,false);
		evt.target = inObj;
		this.dispatchEvent(evt);
		var gfx = this.jeashGetGraphics();
		if(gfx != null) jeash.Lib.jeashRemoveSurface(gfx.jeashSurface);
	}
	,jeashSetParent: function(parent) {
		if(parent == this.parent) return;
		this.mMtxChainDirty = true;
		if(this.parent != null) {
			this.parent.__removeChild(this);
			this.parent.jeashInvalidateBounds();
		}
		if(parent != null) parent.jeashInvalidateBounds();
		if(this.parent == null && parent != null) {
			this.parent = parent;
			this.jeashDoAdded(this);
		} else if(this.parent != null && parent == null) {
			this.parent = parent;
			this.jeashDoRemoved(this);
		} else this.parent = parent;
	}
	,GetStage: function() {
		return jeash.Lib.jeashGetStage();
	}
	,AsContainer: function() {
		return null;
	}
	,GetScrollRect: function() {
		if(this.mScrollRect == null) return null;
		return this.mScrollRect.clone();
	}
	,jeashAsInteractiveObject: function() {
		return null;
	}
	,SetScrollRect: function(inRect) {
		this.mScrollRect = inRect;
		return this.GetScrollRect();
	}
	,hitTestObject: function(obj) {
		return false;
	}
	,jeashGetMouseX: function() {
		return this.globalToLocal(new jeash.geom.Point(this.GetStage().jeashGetMouseX(),0)).x;
	}
	,jeashSetMouseX: function(x) {
		return null;
	}
	,jeashGetMouseY: function() {
		return this.globalToLocal(new jeash.geom.Point(0,this.GetStage().jeashGetMouseY())).y;
	}
	,jeashSetMouseY: function(y) {
		return null;
	}
	,GetTransform: function() {
		return new jeash.geom.Transform(this);
	}
	,SetTransform: function(trans) {
		this.mMatrix = trans.jeashGetMatrix().clone();
		return trans;
	}
	,getFullMatrix: function(childMatrix) {
		if(childMatrix == null) return this.mFullMatrix.clone(); else return childMatrix.mult(this.mFullMatrix);
	}
	,getBounds: function(targetCoordinateSpace) {
		if(this.mMtxDirty || this.mMtxChainDirty) this.jeashValidateMatrix();
		if(this.mBoundsDirty) this.BuildBounds();
		var mtx = this.mFullMatrix.clone();
		mtx.concat(targetCoordinateSpace.mFullMatrix.clone().invert());
		var rect = this.mBoundsRect.transform(mtx);
		return rect;
	}
	,globalToLocal: function(inPos) {
		return this.mFullMatrix.clone().invert().transformPoint(inPos);
	}
	,jeashGetNumChildren: function() {
		return 0;
	}
	,jeashGetMatrix: function() {
		return this.mMatrix.clone();
	}
	,jeashSetMatrix: function(inMatrix) {
		this.mMatrix = inMatrix.clone();
		return inMatrix;
	}
	,jeashGetGraphics: function() {
		return null;
	}
	,GetOpaqueBackground: function() {
		return this.mOpaqueBackground;
	}
	,SetOpaqueBackground: function(inBG) {
		this.mOpaqueBackground = inBG;
		return this.mOpaqueBackground;
	}
	,GetBackgroundRect: function() {
		if(this.mGraphicsBounds == null) {
			var gfx = this.jeashGetGraphics();
			if(gfx != null) this.mGraphicsBounds = gfx.jeashExtent.clone();
		}
		return this.mGraphicsBounds;
	}
	,jeashInvalidateBounds: function() {
		this.mBoundsDirty = true;
		if(this.parent != null) this.parent.jeashInvalidateBounds();
	}
	,jeashInvalidateMatrix: function(local) {
		if(local == null) local = false;
		this.mMtxChainDirty = this.mMtxChainDirty || !local;
		this.mMtxDirty = this.mMtxDirty || local;
	}
	,jeashValidateMatrix: function() {
		if(this.mMtxDirty || this.mMtxChainDirty && this.parent != null) {
			if(this.mMtxChainDirty && this.parent != null) this.parent.jeashValidateMatrix();
			if(this.mMtxDirty) {
				this.mMatrix.b = this.mMatrix.c = this.mMatrix.tx = this.mMatrix.ty = 0;
				this.mMatrix.a = this.jeashScaleX;
				this.mMatrix.d = this.jeashScaleY;
				var rad = this.jeashRotation * Math.PI / 180.0;
				if(rad != 0.0) this.mMatrix.rotate(rad);
				this.mMatrix.tx = this.jeashX;
				this.mMatrix.ty = this.jeashY;
			}
			if(this.parent != null) this.mFullMatrix = this.parent.getFullMatrix(this.mMatrix); else this.mFullMatrix = this.mMatrix;
			this.mMtxDirty = this.mMtxChainDirty = false;
		}
	}
	,jeashRender: function(parentMatrix,inMask) {
		var gfx = this.jeashGetGraphics();
		if(gfx != null) {
			if(!this.jeashVisible) return;
			if(this.mMtxDirty || this.mMtxChainDirty) this.jeashValidateMatrix();
			var m = this.mFullMatrix.clone();
			if(this.jeashFilters != null && (gfx.jeashChanged || inMask != null)) {
				if(gfx.jeashRender(inMask,m)) this.jeashInvalidateBounds();
				var _g = 0, _g1 = this.jeashFilters;
				while(_g < _g1.length) {
					var filter = _g1[_g];
					++_g;
					filter.jeashApplyFilter(gfx.jeashSurface);
				}
			} else if(gfx.jeashRender(inMask,m)) this.jeashInvalidateBounds();
			m.tx = m.tx + gfx.jeashExtent.x * m.a + gfx.jeashExtent.y * m.c;
			m.ty = m.ty + gfx.jeashExtent.x * m.b + gfx.jeashExtent.y * m.d;
			if(inMask != null) jeash.Lib.jeashDrawToSurface(gfx.jeashSurface,inMask,m,(this.parent != null?this.parent.alpha:1) * this.alpha); else {
				jeash.Lib.jeashSetSurfaceTransform(gfx.jeashSurface,m);
				jeash.Lib.jeashSetSurfaceOpacity(gfx.jeashSurface,(this.parent != null?this.parent.alpha:1) * this.alpha);
			}
		} else if(this.mMtxDirty || this.mMtxChainDirty) this.jeashValidateMatrix();
	}
	,drawToSurface: function(inSurface,matrix,colorTransform,blendMode,clipRect,smoothing) {
		if(matrix == null) matrix = new jeash.geom.Matrix();
		this.jeashRender(matrix,inSurface);
	}
	,jeashGetObjectUnderPoint: function(point) {
		if(!this.jeashGetVisible()) return null;
		var gfx = this.jeashGetGraphics();
		if(gfx != null) {
			var extX = gfx.jeashExtent.x;
			var extY = gfx.jeashExtent.y;
			var local = this.globalToLocal(point);
			if(local.x - extX < 0 || local.y - extY < 0 || (local.x - extX) * this.jeashGetScaleX() > this.jeashGetWidth() || (local.y - extY) * this.jeashGetScaleY() > this.jeashGetHeight()) return null;
			switch( (this.GetStage().jeashPointInPathMode)[1] ) {
			case 0:
				if(gfx.jeashHitTest(local.x,local.y)) return this;
				break;
			case 1:
				if(gfx.jeashHitTest(local.x * this.jeashGetScaleX(),local.y * this.jeashGetScaleY())) return this;
				break;
			}
		}
		return null;
	}
	,GetMask: function() {
		return this.mMask;
	}
	,SetMask: function(inMask) {
		if(this.mMask != null) this.mMask.mMaskingObj = null;
		this.mMask = inMask;
		if(this.mMask != null) this.mMask.mMaskingObj = this;
		return this.mMask;
	}
	,jeashSetFilters: function(filters) {
		if(filters == null) this.jeashFilters = null; else {
			this.jeashFilters = new Array();
			var _g = 0;
			while(_g < filters.length) {
				var filter = filters[_g];
				++_g;
				this.jeashFilters.push(filter.clone());
			}
		}
		return filters;
	}
	,jeashGetFilters: function() {
		if(this.jeashFilters == null) return [];
		var result = new Array();
		var _g = 0, _g1 = this.jeashFilters;
		while(_g < _g1.length) {
			var filter = _g1[_g];
			++_g;
			result.push(filter.clone());
		}
		return result;
	}
	,BuildBounds: function() {
		var gfx = this.jeashGetGraphics();
		if(gfx == null) this.mBoundsRect = new jeash.geom.Rectangle(this.jeashGetX(),this.jeashGetY(),0,0); else {
			this.mBoundsRect = gfx.jeashExtent.clone();
			if(this.mScale9Grid != null) {
				this.mBoundsRect.width *= this.jeashGetScaleX();
				this.mBoundsRect.height *= this.jeashGetScaleY();
			}
		}
		this.mBoundsDirty = false;
	}
	,jeashGetInteractiveObjectStack: function(outStack) {
		var io = this.jeashAsInteractiveObject();
		if(io != null) outStack.push(io);
		if(this.parent != null) this.parent.jeashGetInteractiveObjectStack(outStack);
	}
	,jeashFireEvent: function(event) {
		var stack = [];
		if(this.parent != null) this.parent.jeashGetInteractiveObjectStack(stack);
		var l = stack.length;
		if(l > 0) {
			event.jeashSetPhase(jeash.events.EventPhase.CAPTURING_PHASE);
			stack.reverse();
			var _g = 0;
			while(_g < stack.length) {
				var obj = stack[_g];
				++_g;
				event.currentTarget = obj;
				obj.jeashDispatchEvent(event);
				if(event.jeashGetIsCancelled()) return;
			}
		}
		event.jeashSetPhase(jeash.events.EventPhase.AT_TARGET);
		event.currentTarget = this;
		this.jeashDispatchEvent(event);
		if(event.jeashGetIsCancelled()) return;
		if(event.bubbles) {
			event.jeashSetPhase(jeash.events.EventPhase.BUBBLING_PHASE);
			stack.reverse();
			var _g = 0;
			while(_g < stack.length) {
				var obj = stack[_g];
				++_g;
				event.currentTarget = obj;
				obj.jeashDispatchEvent(event);
				if(event.jeashGetIsCancelled()) return;
			}
		}
	}
	,jeashBroadcast: function(event) {
		this.jeashDispatchEvent(event);
	}
	,jeashDispatchEvent: function(event) {
		if(event.target == null) event.target = this;
		event.currentTarget = this;
		return jeash.events.EventDispatcher.prototype.dispatchEvent.call(this,event);
	}
	,dispatchEvent: function(event) {
		var result = this.jeashDispatchEvent(event);
		if(event.jeashGetIsCancelled()) return true;
		if(event.bubbles && this.parent != null) this.parent.dispatchEvent(event);
		return result;
	}
	,jeashAddToStage: function() {
		var gfx = this.jeashGetGraphics();
		if(gfx != null) {
			jeash.Lib.jeashSetSurfaceId(gfx.jeashSurface,this.name);
			jeash.Lib.jeashAppendSurface(gfx.jeashSurface);
		}
	}
	,jeashInsertBefore: function(obj) {
		var gfx1 = this.jeashGetGraphics();
		var gfx2 = obj.jeashIsOnStage()?obj.jeashGetGraphics():null;
		if(gfx1 != null) {
			if(gfx2 != null) jeash.Lib.jeashAppendSurface(gfx1.jeashSurface,gfx2.jeashSurface); else jeash.Lib.jeashAppendSurface(gfx1.jeashSurface);
		}
	}
	,jeashIsOnStage: function() {
		var gfx = this.jeashGetGraphics();
		if(gfx != null) return jeash.Lib.jeashIsOnStage(gfx.jeashSurface);
		return false;
	}
	,jeashGetVisible: function() {
		return this.jeashVisible;
	}
	,jeashSetVisible: function(visible) {
		var gfx = this.jeashGetGraphics();
		if(gfx != null) {
			if(gfx.jeashSurface != null) jeash.Lib.jeashSetSurfaceVisible(gfx.jeashSurface,visible);
		}
		this.jeashVisible = visible;
		return visible;
	}
	,jeashGetHeight: function() {
		this.BuildBounds();
		return this.jeashScaleY * this.mBoundsRect.height;
	}
	,jeashSetHeight: function(inHeight) {
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		if(this.mBoundsDirty) this.BuildBounds();
		var h = this.mBoundsRect.height;
		if(this.jeashScaleY * h != inHeight) {
			if(h <= 0) return 0;
			this.jeashScaleY = inHeight / h;
			this.jeashInvalidateMatrix(true);
		}
		return inHeight;
	}
	,jeashGetWidth: function() {
		if(this.mBoundsDirty) this.BuildBounds();
		return this.jeashScaleX * this.mBoundsRect.width;
	}
	,jeashSetWidth: function(inWidth) {
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		if(this.mBoundsDirty) this.BuildBounds();
		var w = this.mBoundsRect.width;
		if(this.jeashScaleX * w != inWidth) {
			if(w <= 0) return 0;
			this.jeashScaleX = inWidth / w;
			this.jeashInvalidateMatrix(true);
		}
		return inWidth;
	}
	,jeashGetX: function() {
		return this.jeashX;
	}
	,jeashGetY: function() {
		return this.jeashY;
	}
	,jeashSetX: function(n) {
		this.jeashInvalidateMatrix(true);
		this.jeashX = n;
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		return n;
	}
	,jeashSetY: function(n) {
		this.jeashInvalidateMatrix(true);
		this.jeashY = n;
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		return n;
	}
	,jeashGetScaleX: function() {
		return this.jeashScaleX;
	}
	,jeashGetScaleY: function() {
		return this.jeashScaleY;
	}
	,jeashSetScaleX: function(inS) {
		if(this.jeashScaleX == inS) return inS;
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		if(this.mBoundsDirty) this.BuildBounds();
		if(!this.mMtxDirty) this.jeashInvalidateMatrix(true);
		this.jeashScaleX = inS;
		return inS;
	}
	,jeashSetScaleY: function(inS) {
		if(this.jeashScaleY == inS) return inS;
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		if(this.mBoundsDirty) this.BuildBounds();
		if(!this.mMtxDirty) this.jeashInvalidateMatrix(true);
		this.jeashScaleY = inS;
		return inS;
	}
	,jeashSetRotation: function(n) {
		if(!this.mMtxDirty) this.jeashInvalidateMatrix(true);
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		this.jeashRotation = n;
		return n;
	}
	,jeashGetRotation: function() {
		return this.jeashRotation;
	}
	,__class__: jeash.display.DisplayObject
	,__properties__: {set_transform:"SetTransform",get_transform:"GetTransform",get_stage:"GetStage",set_mouseY:"jeashSetMouseY",get_mouseY:"jeashGetMouseY",set_mouseX:"jeashSetMouseX",get_mouseX:"jeashGetMouseX",set_visible:"jeashSetVisible",get_visible:"jeashGetVisible",set_height:"jeashSetHeight",get_height:"jeashGetHeight",set_width:"jeashSetWidth",get_width:"jeashGetWidth",set_rotation:"jeashSetRotation",get_rotation:"jeashGetRotation",set_scaleY:"jeashSetScaleY",get_scaleY:"jeashGetScaleY",set_scaleX:"jeashSetScaleX",get_scaleX:"jeashGetScaleX",set_y:"jeashSetY",get_y:"jeashGetY",set_x:"jeashSetX",get_x:"jeashGetX"}
});
jeash.display.InteractiveObject = function() {
	jeash.display.DisplayObject.call(this);
	this.tabEnabled = false;
	this.mouseEnabled = true;
	this.doubleClickEnabled = true;
	this.jeashSetTabIndex(0);
	this.name = "InteractiveObject";
};
$hxClasses["jeash.display.InteractiveObject"] = jeash.display.InteractiveObject;
jeash.display.InteractiveObject.__name__ = ["jeash","display","InteractiveObject"];
jeash.display.InteractiveObject.__super__ = jeash.display.DisplayObject;
jeash.display.InteractiveObject.prototype = $extend(jeash.display.DisplayObject.prototype,{
	doubleClickEnabled: null
	,mouseEnabled: null
	,tabEnabled: null
	,tabIndex: null
	,jeashTabIndex: null
	,toString: function() {
		return this.name;
	}
	,jeashAsInteractiveObject: function() {
		return this;
	}
	,jeashGetTabIndex: function() {
		return this.jeashTabIndex;
	}
	,jeashSetTabIndex: function(inIndex) {
		this.jeashTabIndex = inIndex;
		return inIndex;
	}
	,jeashGetObjectUnderPoint: function(point) {
		if(!this.mouseEnabled) return null; else return jeash.display.DisplayObject.prototype.jeashGetObjectUnderPoint.call(this,point);
	}
	,__class__: jeash.display.InteractiveObject
	,__properties__: $extend(jeash.display.DisplayObject.prototype.__properties__,{set_tabIndex:"jeashSetTabIndex",get_tabIndex:"jeashGetTabIndex"})
});
jeash.display.DisplayObjectContainer = function() {
	this.jeashChildren = new Array();
	this.mLastSetupObjs = new Array();
	this.mouseChildren = true;
	this.numChildren = 0;
	this.tabChildren = true;
	jeash.display.InteractiveObject.call(this);
	this.name = "DisplayObjectContainer " + jeash.display.DisplayObject.mNameID++;
};
$hxClasses["jeash.display.DisplayObjectContainer"] = jeash.display.DisplayObjectContainer;
jeash.display.DisplayObjectContainer.__name__ = ["jeash","display","DisplayObjectContainer"];
jeash.display.DisplayObjectContainer.__super__ = jeash.display.InteractiveObject;
jeash.display.DisplayObjectContainer.prototype = $extend(jeash.display.InteractiveObject.prototype,{
	jeashChildren: null
	,mLastSetupObjs: null
	,numChildren: null
	,mouseChildren: null
	,tabChildren: null
	,AsContainer: function() {
		return this;
	}
	,jeashBroadcast: function(event) {
		var _g = 0, _g1 = this.jeashChildren;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.jeashBroadcast(event);
		}
		this.dispatchEvent(event);
	}
	,BuildBounds: function() {
		jeash.display.InteractiveObject.prototype.BuildBounds.call(this);
		var _g = 0, _g1 = this.jeashChildren;
		while(_g < _g1.length) {
			var obj = _g1[_g];
			++_g;
			if(obj.jeashGetVisible()) {
				var r = obj.getBounds(this);
				if(r.width != 0 || r.height != 0) {
					if(this.mBoundsRect.width == 0 && this.mBoundsRect.height == 0) this.mBoundsRect = r.clone(); else this.mBoundsRect.extendBounds(r);
				}
			}
		}
	}
	,jeashInvalidateMatrix: function(local) {
		if(local == null) local = false;
		if(this.mMtxChainDirty == false && this.mMtxDirty == false) {
			var _g = 0, _g1 = this.jeashChildren;
			while(_g < _g1.length) {
				var child = _g1[_g];
				++_g;
				child.jeashInvalidateMatrix();
			}
		}
		this.mMtxChainDirty = this.mMtxChainDirty || !local;
		this.mMtxDirty = this.mMtxDirty || local;
	}
	,jeashDoAdded: function(inObj) {
		jeash.display.InteractiveObject.prototype.jeashDoAdded.call(this,inObj);
		var _g = 0, _g1 = this.jeashChildren;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.jeashDoAdded(inObj);
		}
	}
	,jeashDoRemoved: function(inObj) {
		jeash.display.InteractiveObject.prototype.jeashDoRemoved.call(this,inObj);
		var _g = 0, _g1 = this.jeashChildren;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.jeashDoRemoved(inObj);
		}
	}
	,GetBackgroundRect: function() {
		var r = jeash.display.InteractiveObject.prototype.GetBackgroundRect.call(this);
		if(r != null) r = r.clone();
		var _g = 0, _g1 = this.jeashChildren;
		while(_g < _g1.length) {
			var obj = _g1[_g];
			++_g;
			if(obj.jeashGetVisible()) {
				var o = obj.GetBackgroundRect();
				if(o != null) {
					var trans = o.transform(obj.mMatrix);
					if(r == null || r.width == 0 || r.height == 0) r = trans; else if(trans.width != 0 && trans.height != 0) r.extendBounds(trans);
				}
			}
		}
		return r;
	}
	,jeashGetNumChildren: function() {
		return this.jeashChildren.length;
	}
	,jeashRender: function(inParentMatrix,inMask) {
		if(!this.jeashGetVisible()) return;
		jeash.display.InteractiveObject.prototype.jeashRender.call(this,inParentMatrix,inMask);
		var _g = 0, _g1 = this.jeashChildren;
		while(_g < _g1.length) {
			var obj = _g1[_g];
			++_g;
			if(obj.jeashGetVisible()) obj.jeashRender(this.mFullMatrix,inMask);
		}
	}
	,addChild: function(object) {
		if(object == this) throw "Adding to self";
		if(object.parent == this) {
			this.setChildIndex(object,this.jeashChildren.length - 1);
			return object;
		}
		var _g1 = 0, _g = this.jeashChildren.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.jeashChildren[i] == object) throw "Internal error: child already existed at index " + i;
		}
		if(this.jeashIsOnStage()) object.jeashAddToStage();
		this.jeashChildren.push(object);
		object.jeashSetParent(this);
		return object;
	}
	,jeashAddToStage: function() {
		jeash.display.InteractiveObject.prototype.jeashAddToStage.call(this);
		var _g1 = 0, _g = this.jeashChildren.length;
		while(_g1 < _g) {
			var i = _g1++;
			this.jeashChildren[i].jeashAddToStage();
		}
	}
	,jeashInsertBefore: function(obj) {
		jeash.display.InteractiveObject.prototype.jeashInsertBefore.call(this,obj);
		var _g1 = 0, _g = this.jeashChildren.length;
		while(_g1 < _g) {
			var i = _g1++;
			this.jeashChildren[i].jeashAddToStage();
		}
	}
	,getChildIndex: function(child) {
		var _g1 = 0, _g = this.jeashChildren.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.jeashChildren[i] == child) return i;
		}
		return -1;
	}
	,removeChild: function(child) {
		var _g1 = 0, _g = this.jeashChildren.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.jeashChildren[i] == child) {
				child.jeashSetParent(null);
				if(this.getChildIndex(child) >= 0) throw "Not removed properly";
				return;
			}
		}
		throw "removeChild : none found?";
	}
	,__removeChild: function(child) {
		var i = this.getChildIndex(child);
		if(i >= 0) this.jeashChildren.splice(i,1);
	}
	,setChildIndex: function(child,index) {
		if(index > this.jeashChildren.length) throw "Invalid index position " + index;
		var s = null;
		var orig = this.getChildIndex(child);
		if(orig < 0) {
			var msg = "setChildIndex : object " + child.name + " not found.";
			if(child.parent == this) {
				var realindex = -1;
				var _g1 = 0, _g = this.jeashChildren.length;
				while(_g1 < _g) {
					var i = _g1++;
					if(this.jeashChildren[i] == child) {
						realindex = i;
						break;
					}
				}
				if(realindex != -1) msg += "Internal error: Real child index was " + Std.string(realindex); else msg += "Internal error: Child was not in jeashChildren array!";
			}
			throw msg;
		}
		if(index < orig) {
			var i = orig;
			while(i > index) {
				this.swapChildren(this.jeashChildren[i],this.jeashChildren[i - 1]);
				i--;
			}
		} else if(orig < index) {
			var i = orig;
			while(i < index) {
				this.swapChildren(this.jeashChildren[i],this.jeashChildren[i + 1]);
				i++;
			}
		}
	}
	,jeashSwapSurface: function(c1,c2) {
		if(this.jeashChildren[c1] == null) throw "Null element at index " + c1 + " length " + this.jeashChildren.length;
		if(this.jeashChildren[c2] == null) throw "Null element at index " + c2 + " length " + this.jeashChildren.length;
		var gfx1 = this.jeashChildren[c1].jeashGetGraphics();
		var gfx2 = this.jeashChildren[c2].jeashGetGraphics();
		if(gfx1 != null && gfx2 != null) jeash.Lib.jeashSwapSurface(gfx1.jeashSurface,gfx2.jeashSurface);
	}
	,swapChildren: function(child1,child2) {
		var c1 = -1;
		var c2 = -1;
		var swap;
		var _g1 = 0, _g = this.jeashChildren.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.jeashChildren[i] == child1) c1 = i; else if(this.jeashChildren[i] == child2) c2 = i;
		}
		if(c1 != -1 && c2 != -1) {
			swap = this.jeashChildren[c1];
			this.jeashChildren[c1] = this.jeashChildren[c2];
			this.jeashChildren[c2] = swap;
			swap = null;
			this.jeashSwapSurface(c1,c2);
		}
	}
	,jeashGetObjectUnderPoint: function(point) {
		if(!this.jeashGetVisible()) return null;
		var l = this.jeashChildren.length - 1;
		var _g1 = 0, _g = this.jeashChildren.length;
		while(_g1 < _g) {
			var i = _g1++;
			var result = this.jeashChildren[l - i].jeashGetObjectUnderPoint(point);
			if(result != null) return this.mouseChildren?result:this;
		}
		return jeash.display.InteractiveObject.prototype.jeashGetObjectUnderPoint.call(this,point);
	}
	,jeashSetFilters: function(filters) {
		jeash.display.InteractiveObject.prototype.jeashSetFilters.call(this,filters);
		var _g = 0, _g1 = this.jeashChildren;
		while(_g < _g1.length) {
			var obj = _g1[_g];
			++_g;
			obj.jeashSetFilters(filters);
		}
		return filters;
	}
	,jeashSetVisible: function(visible) {
		jeash.display.InteractiveObject.prototype.jeashSetVisible.call(this,visible);
		var _g1 = 0, _g = this.jeashChildren.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.jeashChildren[i].jeashIsOnStage()) this.jeashChildren[i].jeashSetVisible(visible);
		}
		return visible;
	}
	,__class__: jeash.display.DisplayObjectContainer
	,__properties__: $extend(jeash.display.InteractiveObject.prototype.__properties__,{get_numChildren:"jeashGetNumChildren"})
});
jeash.display.Sprite = function() {
	this.jeashGraphics = new jeash.display.Graphics();
	jeash.display.DisplayObjectContainer.call(this);
	this.buttonMode = false;
	this.name = "Sprite " + jeash.display.DisplayObject.mNameID++;
	jeash.Lib.jeashSetSurfaceId(this.jeashGraphics.jeashSurface,this.name);
};
$hxClasses["jeash.display.Sprite"] = jeash.display.Sprite;
jeash.display.Sprite.__name__ = ["jeash","display","Sprite"];
jeash.display.Sprite.__super__ = jeash.display.DisplayObjectContainer;
jeash.display.Sprite.prototype = $extend(jeash.display.DisplayObjectContainer.prototype,{
	jeashGraphics: null
	,graphics: null
	,useHandCursor: null
	,buttonMode: null
	,jeashCursorCallbackOver: null
	,jeashCursorCallbackOut: null
	,jeashDropTarget: null
	,jeashGetGraphics: function() {
		return this.jeashGraphics;
	}
	,jeashSetUseHandCursor: function(cursor) {
		if(cursor == this.useHandCursor) return cursor;
		if(this.jeashCursorCallbackOver != null) this.removeEventListener(jeash.events.MouseEvent.ROLL_OVER,this.jeashCursorCallbackOver);
		if(this.jeashCursorCallbackOut != null) this.removeEventListener(jeash.events.MouseEvent.ROLL_OUT,this.jeashCursorCallbackOut);
		if(!cursor) jeash.Lib.jeashSetCursor(jeash._Lib.CursorType.Default); else {
			this.jeashCursorCallbackOver = function(_) {
				jeash.Lib.jeashSetCursor(jeash._Lib.CursorType.Pointer);
			};
			this.jeashCursorCallbackOut = function(_) {
				jeash.Lib.jeashSetCursor(jeash._Lib.CursorType.Default);
			};
			this.addEventListener(jeash.events.MouseEvent.ROLL_OVER,this.jeashCursorCallbackOver);
			this.addEventListener(jeash.events.MouseEvent.ROLL_OUT,this.jeashCursorCallbackOut);
		}
		this.useHandCursor = cursor;
		return cursor;
	}
	,jeashGetDropTarget: function() {
		return this.jeashDropTarget;
	}
	,jeashSetX: function(n) {
		this.jeashInvalidateMatrix(true);
		this.jeashX = n;
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		return n;
	}
	,jeashSetY: function(n) {
		this.jeashInvalidateMatrix(true);
		this.jeashY = n;
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		return n;
	}
	,jeashSetScaleX: function(inS) {
		if(this.jeashScaleX == inS) return inS;
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		if(this.mBoundsDirty) this.BuildBounds();
		if(!this.mMtxDirty) this.jeashInvalidateMatrix(true);
		this.jeashScaleX = inS;
		return inS;
	}
	,jeashSetScaleY: function(inS) {
		if(this.jeashScaleY == inS) return inS;
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		if(this.mBoundsDirty) this.BuildBounds();
		if(!this.mMtxDirty) this.jeashInvalidateMatrix(true);
		this.jeashScaleY = inS;
		return inS;
	}
	,jeashSetRotation: function(n) {
		if(!this.mMtxDirty) this.jeashInvalidateMatrix(true);
		if(this.parent != null) this.parent.jeashInvalidateBounds();
		this.jeashRotation = n;
		return n;
	}
	,__class__: jeash.display.Sprite
	,__properties__: $extend(jeash.display.DisplayObjectContainer.prototype.__properties__,{set_useHandCursor:"jeashSetUseHandCursor",get_graphics:"jeashGetGraphics"})
});
var NMEPreloader = function() {
	jeash.display.Sprite.call(this);
	var backgroundColor = this.getBackgroundColor();
	var r = backgroundColor >> 16 & 255;
	var g = backgroundColor >> 8 & 255;
	var b = backgroundColor & 255;
	var perceivedLuminosity = 0.299 * r + 0.587 * g + 0.114 * b;
	var color = 0;
	if(perceivedLuminosity < 70) color = 16777215;
	var x = 30;
	var height = 9;
	var y = this.getHeight() / 2 - height / 2;
	var width = this.getWidth() - x * 2;
	var padding = 3;
	this.outline = new jeash.display.Sprite();
	this.outline.jeashGetGraphics().lineStyle(1,color,0.15,true);
	this.outline.jeashGetGraphics().drawRoundRect(0,0,width,height,padding * 2,padding * 2);
	this.outline.jeashSetX(x);
	this.outline.jeashSetY(y);
	this.addChild(this.outline);
	this.progress = new jeash.display.Sprite();
	this.progress.jeashGetGraphics().beginFill(color,0.35);
	this.progress.jeashGetGraphics().drawRect(0,0,width - padding * 2,height - padding * 2);
	this.progress.jeashSetX(x + padding);
	this.progress.jeashSetY(y + padding);
	this.progress.jeashSetScaleX(0);
	this.addChild(this.progress);
};
$hxClasses["NMEPreloader"] = NMEPreloader;
NMEPreloader.__name__ = ["NMEPreloader"];
NMEPreloader.__super__ = jeash.display.Sprite;
NMEPreloader.prototype = $extend(jeash.display.Sprite.prototype,{
	outline: null
	,progress: null
	,getBackgroundColor: function() {
		return 0;
	}
	,getHeight: function() {
		return 0;
	}
	,getWidth: function() {
		return 0;
	}
	,onInit: function() {
	}
	,onLoaded: function() {
		this.dispatchEvent(new jeash.events.Event(jeash.events.Event.COMPLETE));
	}
	,onUpdate: function(bytesLoaded,bytesTotal) {
		var percentLoaded = bytesLoaded / bytesTotal;
		if(percentLoaded > 1) percentLoaded == 1;
		this.progress.jeashSetScaleX(percentLoaded);
	}
	,__class__: NMEPreloader
});
var Reflect = function() { }
$hxClasses["Reflect"] = Reflect;
Reflect.__name__ = ["Reflect"];
Reflect.field = function(o,field) {
	var v = null;
	try {
		v = o[field];
	} catch( e ) {
	}
	return v;
}
Reflect.setField = function(o,field,value) {
	o[field] = value;
}
Reflect.callMethod = function(o,func,args) {
	return func.apply(o,args);
}
Reflect.fields = function(o) {
	var a = [];
	if(o != null) {
		var hasOwnProperty = Object.prototype.hasOwnProperty;
		for( var f in o ) {
		if(hasOwnProperty.call(o,f)) a.push(f);
		}
	}
	return a;
}
Reflect.isFunction = function(f) {
	return typeof(f) == "function" && f.__name__ == null;
}
Reflect.compareMethods = function(f1,f2) {
	if(f1 == f2) return true;
	if(!Reflect.isFunction(f1) || !Reflect.isFunction(f2)) return false;
	return f1.scope == f2.scope && f1.method == f2.method && f1.method != null;
}
Reflect.prototype = {
	__class__: Reflect
}
var Std = function() { }
$hxClasses["Std"] = Std;
Std.__name__ = ["Std"];
Std["is"] = function(v,t) {
	return js.Boot.__instanceof(v,t);
}
Std.string = function(s) {
	return js.Boot.__string_rec(s,"");
}
Std["int"] = function(x) {
	return x | 0;
}
Std.parseInt = function(x) {
	var v = parseInt(x,10);
	if(v == 0 && x.charCodeAt(1) == 120) v = parseInt(x);
	if(isNaN(v)) return null;
	return v;
}
Std.parseFloat = function(x) {
	return parseFloat(x);
}
Std.prototype = {
	__class__: Std
}
var StringTools = function() { }
$hxClasses["StringTools"] = StringTools;
StringTools.__name__ = ["StringTools"];
StringTools.urlEncode = function(s) {
	return encodeURIComponent(s);
}
StringTools.urlDecode = function(s) {
	return decodeURIComponent(s.split("+").join(" "));
}
StringTools.startsWith = function(s,start) {
	return s.length >= start.length && s.substr(0,start.length) == start;
}
StringTools.fastCodeAt = function(s,index) {
	return s.cca(index);
}
StringTools.isEOF = function(c) {
	return c != c;
}
StringTools.prototype = {
	__class__: StringTools
}
var ValueType = $hxClasses["ValueType"] = { __ename__ : ["ValueType"], __constructs__ : ["TNull","TInt","TFloat","TBool","TObject","TFunction","TClass","TEnum","TUnknown"] }
ValueType.TNull = ["TNull",0];
ValueType.TNull.toString = $estr;
ValueType.TNull.__enum__ = ValueType;
ValueType.TInt = ["TInt",1];
ValueType.TInt.toString = $estr;
ValueType.TInt.__enum__ = ValueType;
ValueType.TFloat = ["TFloat",2];
ValueType.TFloat.toString = $estr;
ValueType.TFloat.__enum__ = ValueType;
ValueType.TBool = ["TBool",3];
ValueType.TBool.toString = $estr;
ValueType.TBool.__enum__ = ValueType;
ValueType.TObject = ["TObject",4];
ValueType.TObject.toString = $estr;
ValueType.TObject.__enum__ = ValueType;
ValueType.TFunction = ["TFunction",5];
ValueType.TFunction.toString = $estr;
ValueType.TFunction.__enum__ = ValueType;
ValueType.TClass = function(c) { var $x = ["TClass",6,c]; $x.__enum__ = ValueType; $x.toString = $estr; return $x; }
ValueType.TEnum = function(e) { var $x = ["TEnum",7,e]; $x.__enum__ = ValueType; $x.toString = $estr; return $x; }
ValueType.TUnknown = ["TUnknown",8];
ValueType.TUnknown.toString = $estr;
ValueType.TUnknown.__enum__ = ValueType;
var Type = function() { }
$hxClasses["Type"] = Type;
Type.__name__ = ["Type"];
Type.resolveClass = function(name) {
	var cl = $hxClasses[name];
	if(cl == null || cl.__name__ == null) return null;
	return cl;
}
Type.resolveEnum = function(name) {
	var e = $hxClasses[name];
	if(e == null || e.__ename__ == null) return null;
	return e;
}
Type.createEmptyInstance = function(cl) {
	function empty() {}; empty.prototype = cl.prototype;
	return new empty();
}
Type.createEnum = function(e,constr,params) {
	var f = Reflect.field(e,constr);
	if(f == null) throw "No such constructor " + constr;
	if(Reflect.isFunction(f)) {
		if(params == null) throw "Constructor " + constr + " need parameters";
		return f.apply(e,params);
	}
	if(params != null && params.length != 0) throw "Constructor " + constr + " does not need parameters";
	return f;
}
Type.getEnumConstructs = function(e) {
	var a = e.__constructs__;
	return a.copy();
}
Type.prototype = {
	__class__: Type
}
var Xml = function() { }
$hxClasses["Xml"] = Xml;
Xml.__name__ = ["Xml"];
Xml.Element = null;
Xml.PCData = null;
Xml.CData = null;
Xml.Comment = null;
Xml.DocType = null;
Xml.Prolog = null;
Xml.Document = null;
Xml.prototype = {
	nodeType: null
	,_nodeName: null
	,_nodeValue: null
	,_parent: null
	,getNodeName: function() {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		return this._nodeName;
	}
	,setNodeName: function(n) {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		return this._nodeName = n;
	}
	,getNodeValue: function() {
		if(this.nodeType == Xml.Element || this.nodeType == Xml.Document) throw "bad nodeType";
		return this._nodeValue;
	}
	,setNodeValue: function(v) {
		if(this.nodeType == Xml.Element || this.nodeType == Xml.Document) throw "bad nodeType";
		return this._nodeValue = v;
	}
	,getParent: function() {
		return this._parent;
	}
	,__class__: Xml
}
var com = {}
com.damir = {}
com.damir.haxteroids = {}
com.damir.haxteroids.Asteroid = function(x,y,width,height) {
	if(height == null) height = 10;
	if(width == null) width = 10;
	jeash.display.Sprite.call(this);
	this.jeashGetGraphics().beginFill(16777215);
	this.jeashGetGraphics().drawRect(-width / 2,-height / 2,width,height);
	this.jeashSetX(x);
	this.jeashSetY(y);
	this.velocity = new jeash.geom.Vector3D(Math.random() * 8 - 4,Math.random() * 8 - 4);
	this.rotationSpeed = Math.random() * 4 - 2;
};
$hxClasses["com.damir.haxteroids.Asteroid"] = com.damir.haxteroids.Asteroid;
com.damir.haxteroids.Asteroid.__name__ = ["com","damir","haxteroids","Asteroid"];
com.damir.haxteroids.Asteroid.__super__ = jeash.display.Sprite;
com.damir.haxteroids.Asteroid.prototype = $extend(jeash.display.Sprite.prototype,{
	velocity: null
	,rotationSpeed: null
	,manager: null
	,update: function() {
		this.handleInput();
		var _g = this;
		_g.jeashSetX(_g.jeashGetX() + this.velocity.x);
		var _g = this;
		_g.jeashSetY(_g.jeashGetY() + this.velocity.y);
		this.jeashSetRotation((this.jeashGetRotation() + this.rotationSpeed) % 360);
		if(this.parent == null) return;
		if(this.jeashGetX() < 0) this.jeashSetX(this.GetStage().jeashGetStageWidth()); else if(this.jeashGetX() > this.GetStage().jeashGetStageWidth()) this.jeashSetX(0);
		if(this.jeashGetY() < 0) this.jeashSetY(this.GetStage().jeashGetStageHeight()); else if(this.jeashGetY() > this.GetStage().jeashGetStageHeight()) this.jeashSetY(0);
	}
	,handleInput: function() {
		if(com.damir.haxteroids.InputState.keyState[jeash.ui.Keyboard.A]) this.destroy();
	}
	,destroy: function() {
		this.manager.destroy(this);
	}
	,__class__: com.damir.haxteroids.Asteroid
});
com.damir.haxteroids.AsteroidManager = function(count,container) {
	this.asteroids = new Array();
	this.screen = container;
	var _g = 0;
	while(_g < count) {
		var i = _g++;
		this.asteroids.push(new com.damir.haxteroids.Asteroid(Math.random() * jeash.Lib.jeashGetCurrent().GetStage().jeashGetStageWidth(),Math.random() * jeash.Lib.jeashGetCurrent().GetStage().jeashGetStageHeight(),com.damir.haxteroids.AsteroidManager.startHeight,com.damir.haxteroids.AsteroidManager.startWidth));
		this.asteroids[i].manager = this;
		this.screen.addChild(this.asteroids[i]);
	}
};
$hxClasses["com.damir.haxteroids.AsteroidManager"] = com.damir.haxteroids.AsteroidManager;
com.damir.haxteroids.AsteroidManager.__name__ = ["com","damir","haxteroids","AsteroidManager"];
com.damir.haxteroids.AsteroidManager.prototype = {
	asteroids: null
	,screen: null
	,update: function() {
		var length = this.asteroids.length;
		var _g = 0;
		while(_g < length) {
			var i = _g++;
			this.asteroids[i].update();
		}
	}
	,destroy: function(asteroid) {
		this.asteroids.remove(asteroid);
		this.asteroids.push(new com.damir.haxteroids.Asteroid(asteroid.jeashGetX() + 10,asteroid.jeashGetY() + 10,asteroid.jeashGetWidth() / 2,asteroid.jeashGetHeight()));
		this.asteroids.push(new com.damir.haxteroids.Asteroid(asteroid.jeashGetX() - 10,asteroid.jeashGetY() - 10,asteroid.jeashGetWidth(),asteroid.jeashGetHeight() / 2));
		this.asteroids[this.asteroids.length - 2].manager = this;
		this.screen.addChild(this.asteroids[this.asteroids.length - 2]);
		this.asteroids[this.asteroids.length - 1].manager = this;
		this.screen.addChild(this.asteroids[this.asteroids.length - 1]);
		this.screen.removeChild(asteroid);
	}
	,hitTest: function(object) {
		var _g = 0, _g1 = this.asteroids;
		while(_g < _g1.length) {
			var asteroid = _g1[_g];
			++_g;
			if(object.hitTestObject(asteroid)) haxe.Log.trace("HIT",{ fileName : "AsteroidManager.hx", lineNumber : 84, className : "com.damir.haxteroids.AsteroidManager", methodName : "hitTest"});
		}
	}
	,__class__: com.damir.haxteroids.AsteroidManager
}
com.damir.haxteroids.Bullet = function(x,y,direction) {
	if(y == null) y = 0;
	if(x == null) x = 0;
	jeash.display.Sprite.call(this);
	this.jeashSetX(x);
	this.jeashSetY(y);
	this.jeashGetGraphics().beginFill(16711680);
	this.jeashGetGraphics().drawRect(-1.5,-4,3,8);
	if(direction != null) {
		this.velocity = direction;
		this.velocity.normalize(com.damir.haxteroids.Bullet.speed);
	} else this.velocity = new jeash.geom.Point();
	this.jeashSetVisible(false);
};
$hxClasses["com.damir.haxteroids.Bullet"] = com.damir.haxteroids.Bullet;
com.damir.haxteroids.Bullet.__name__ = ["com","damir","haxteroids","Bullet"];
com.damir.haxteroids.Bullet.__super__ = jeash.display.Sprite;
com.damir.haxteroids.Bullet.prototype = $extend(jeash.display.Sprite.prototype,{
	velocity: null
	,update: function() {
		var _g = this;
		_g.jeashSetX(_g.jeashGetX() + this.velocity.x);
		var _g = this;
		_g.jeashSetY(_g.jeashGetY() + this.velocity.y);
		if(this.jeashGetX() < 0 || this.jeashGetX() > this.GetStage().jeashGetStageWidth() || this.jeashGetY() < 0 || this.jeashGetY() > this.GetStage().jeashGetStageHeight()) this.jeashSetVisible(false);
	}
	,fire: function(x,y,direction) {
		if(y == null) y = 0;
		if(x == null) x = 0;
		this.jeashSetX(x);
		this.jeashSetY(y);
		this.velocity = direction;
		this.velocity.normalize(com.damir.haxteroids.Bullet.speed);
		if(this.velocity.x != 0) {
			haxe.Log.trace("rotation " + Math.acos(1 / this.velocity.x),{ fileName : "Bullet.hx", lineNumber : 60, className : "com.damir.haxteroids.Bullet", methodName : "fire"});
			this.jeashSetRotation(Math.atan2(this.velocity.y,this.velocity.x) * 180 / Math.PI + 90);
		} else this.jeashSetRotation(0);
		this.jeashSetVisible(true);
		haxe.Log.trace("FIRE",{ fileName : "Bullet.hx", lineNumber : 66, className : "com.damir.haxteroids.Bullet", methodName : "fire"});
	}
	,__class__: com.damir.haxteroids.Bullet
});
com.damir.haxteroids.InputState = function() { }
$hxClasses["com.damir.haxteroids.InputState"] = com.damir.haxteroids.InputState;
com.damir.haxteroids.InputState.__name__ = ["com","damir","haxteroids","InputState"];
com.damir.haxteroids.InputState.prototype = {
	__class__: com.damir.haxteroids.InputState
}
com.damir.haxteroids.Main = function() {
	jeash.display.Sprite.call(this);
	this.addEventListener(jeash.events.Event.ADDED_TO_STAGE,this.init.$bind(this));
};
$hxClasses["com.damir.haxteroids.Main"] = com.damir.haxteroids.Main;
com.damir.haxteroids.Main.__name__ = ["com","damir","haxteroids","Main"];
com.damir.haxteroids.Main.__super__ = jeash.display.Sprite;
com.damir.haxteroids.Main.prototype = $extend(jeash.display.Sprite.prototype,{
	ship: null
	,asteroidManager: null
	,init: function(e) {
		this.ship = new com.damir.haxteroids.Ship(this.GetStage().jeashGetStageWidth() / 2,this.GetStage().jeashGetStageHeight() / 2);
		this.asteroidManager = new com.damir.haxteroids.AsteroidManager(1,this);
		this.addChild(this.ship);
		jeash.Lib.jeashGetCurrent().GetStage().addEventListener(jeash.events.KeyboardEvent.KEY_DOWN,this.onKeyDown.$bind(this));
		jeash.Lib.jeashGetCurrent().GetStage().addEventListener(jeash.events.KeyboardEvent.KEY_UP,this.onKeyUp.$bind(this));
		this.addEventListener(jeash.events.Event.ENTER_FRAME,this.update.$bind(this));
	}
	,deltaTime: null
	,previousTime: null
	,update: function(e) {
		this.deltaTime = jeash.Lib.getTimer() - this.previousTime;
		this.previousTime += this.deltaTime;
		this.ship.update(this.deltaTime);
		this.asteroidManager.update();
		this.asteroidManager.hitTest(this.ship);
	}
	,onKeyUp: function(e) {
		com.damir.haxteroids.InputState.keyState[e.keyCode] = false;
	}
	,onKeyDown: function(e) {
		haxe.Log.trace("KeyCode: " + e.keyCode,{ fileName : "Main.hx", lineNumber : 82, className : "com.damir.haxteroids.Main", methodName : "onKeyDown"});
		com.damir.haxteroids.InputState.keyState[e.keyCode] = true;
	}
	,__class__: com.damir.haxteroids.Main
});
com.damir.haxteroids.Ship = function(x,y) {
	jeash.display.Sprite.call(this);
	this.sprite = new jeash.display.Bitmap(nme.installer.Assets.getBitmapData("img/ship.png"));
	this.velocity = new jeash.geom.Vector3D();
	this.acceleration = 0.0;
	this.bullet = new com.damir.haxteroids.Bullet();
	this.addEventListener(jeash.events.Event.ADDED_TO_STAGE,this.onAdded.$bind(this));
	this.jeashSetX(x);
	this.jeashSetY(y);
	haxe.Log.trace("X: " + x + " this.x: " + this.jeashGetX(),{ fileName : "Ship.hx", lineNumber : 52, className : "com.damir.haxteroids.Ship", methodName : "new"});
	this.addChild(this.sprite);
	this.sprite.jeashSetX(this.sprite.jeashGetWidth() / -2);
	this.sprite.jeashSetY(this.sprite.jeashGetHeight() / -2);
	this.mat = new jeash.geom.Matrix(1,0,0,1,x,y);
	this.GetTransform().jeashSetMatrix(this.mat);
};
$hxClasses["com.damir.haxteroids.Ship"] = com.damir.haxteroids.Ship;
com.damir.haxteroids.Ship.__name__ = ["com","damir","haxteroids","Ship"];
com.damir.haxteroids.Ship.__super__ = jeash.display.Sprite;
com.damir.haxteroids.Ship.prototype = $extend(jeash.display.Sprite.prototype,{
	sprite: null
	,velocity: null
	,acceleration: null
	,bullet: null
	,mat: null
	,onAdded: function(e) {
		this.parent.addChild(this.bullet);
		this.removeEventListener(jeash.events.Event.ADDED_TO_STAGE,this.onAdded.$bind(this));
	}
	,turnLeft: function() {
		this.mat.translate(-this.jeashGetX(),-this.jeashGetY());
		this.mat.rotate(-com.damir.haxteroids.Ship.rotationSpeed);
		this.mat.translate(this.jeashGetX(),this.jeashGetY());
		this.GetTransform().jeashSetMatrix(this.mat);
	}
	,turnRight: function() {
		this.mat.translate(-this.jeashGetX(),-this.jeashGetY());
		this.mat.rotate(com.damir.haxteroids.Ship.rotationSpeed);
		this.mat.translate(this.jeashGetX(),this.jeashGetY());
		this.GetTransform().jeashSetMatrix(this.mat);
	}
	,accelerate: function() {
		this.acceleration = -2;
		haxe.Log.trace("Accel: " + this.acceleration,{ fileName : "Ship.hx", lineNumber : 92, className : "com.damir.haxteroids.Ship", methodName : "accelerate"});
	}
	,decelerate: function() {
		this.acceleration = 1;
	}
	,update: function(deltaTime) {
		this.acceleration = 0;
		this.handleInput();
		var deltaTimeInSeconds = deltaTime / 1000.0;
		this.velocity.x += this.acceleration * this.mat.c;
		if(this.velocity.x != 0) this.velocity.x -= com.damir.haxteroids.Util.sign(this.velocity.x) * com.damir.haxteroids.Ship.friction;
		this.velocity.y += this.acceleration * this.mat.d;
		if(this.velocity.y != 0) this.velocity.y -= com.damir.haxteroids.Util.sign(this.velocity.y) * com.damir.haxteroids.Ship.friction;
		if(Math.abs(jeash.geom.Vector3D.distance(this.velocity,new jeash.geom.Vector3D())) > com.damir.haxteroids.Ship.maxVelocity) {
			this.velocity.x = com.damir.haxteroids.Util.sign(this.velocity.x) * com.damir.haxteroids.Ship.maxVelocity * this.mat.b;
			this.velocity.y = com.damir.haxteroids.Util.sign(this.velocity.y) * com.damir.haxteroids.Ship.maxVelocity * this.mat.d;
		}
		this.mat.translate(this.velocity.x * deltaTimeInSeconds,this.velocity.y * deltaTimeInSeconds);
		if(this.mat.tx < 0) this.mat.tx = this.GetStage().jeashGetStageWidth(); else if(this.mat.tx > this.GetStage().jeashGetStageWidth()) this.mat.tx = 0;
		if(this.mat.ty < 0) this.mat.ty = this.GetStage().jeashGetStageHeight(); else if(this.mat.ty > this.GetStage().jeashGetStageHeight()) this.mat.ty = 0;
		this.GetTransform().jeashSetMatrix(this.mat);
		if(this.bullet.jeashGetVisible()) this.bullet.update();
	}
	,fire: function() {
		if(!this.bullet.jeashGetVisible()) {
			var direction = new jeash.geom.Point(-this.mat.c,-this.mat.d);
			this.bullet.fire(this.jeashGetX() - this.jeashGetWidth() / 2 * this.mat.c,this.jeashGetY() - this.jeashGetHeight() / 2 * this.mat.d,direction);
		}
	}
	,handleInput: function() {
		if(com.damir.haxteroids.InputState.keyState[jeash.ui.Keyboard.LEFT]) this.turnLeft();
		if(com.damir.haxteroids.InputState.keyState[jeash.ui.Keyboard.RIGHT]) this.turnRight();
		if(com.damir.haxteroids.InputState.keyState[jeash.ui.Keyboard.UP]) this.accelerate();
		if(com.damir.haxteroids.InputState.keyState[jeash.ui.Keyboard.DOWN]) this.decelerate();
		if(com.damir.haxteroids.InputState.keyState[jeash.ui.Keyboard.ENTER]) {
			haxe.Log.trace("ENTER",{ fileName : "Ship.hx", lineNumber : 191, className : "com.damir.haxteroids.Ship", methodName : "handleInput"});
			this.fire();
		}
	}
	,__class__: com.damir.haxteroids.Ship
});
com.damir.haxteroids.Util = function() { }
$hxClasses["com.damir.haxteroids.Util"] = com.damir.haxteroids.Util;
com.damir.haxteroids.Util.__name__ = ["com","damir","haxteroids","Util"];
com.damir.haxteroids.Util.sign = function(number) {
	return number < 0?-1:1;
}
com.damir.haxteroids.Util.prototype = {
	__class__: com.damir.haxteroids.Util
}
var haxe = {}
haxe.Log = function() { }
$hxClasses["haxe.Log"] = haxe.Log;
haxe.Log.__name__ = ["haxe","Log"];
haxe.Log.trace = function(v,infos) {
	js.Boot.__trace(v,infos);
}
haxe.Log.prototype = {
	__class__: haxe.Log
}
haxe.Resource = function() { }
$hxClasses["haxe.Resource"] = haxe.Resource;
haxe.Resource.__name__ = ["haxe","Resource"];
haxe.Resource.content = null;
haxe.Resource.getString = function(name) {
	var _g = 0, _g1 = haxe.Resource.content;
	while(_g < _g1.length) {
		var x = _g1[_g];
		++_g;
		if(x.name == name) {
			if(x.str != null) return x.str;
			var b = haxe.Unserializer.run(x.data);
			return b.toString();
		}
	}
	return null;
}
haxe.Resource.prototype = {
	__class__: haxe.Resource
}
haxe.StackItem = $hxClasses["haxe.StackItem"] = { __ename__ : ["haxe","StackItem"], __constructs__ : ["CFunction","Module","FilePos","Method","Lambda"] }
haxe.StackItem.CFunction = ["CFunction",0];
haxe.StackItem.CFunction.toString = $estr;
haxe.StackItem.CFunction.__enum__ = haxe.StackItem;
haxe.StackItem.Module = function(m) { var $x = ["Module",1,m]; $x.__enum__ = haxe.StackItem; $x.toString = $estr; return $x; }
haxe.StackItem.FilePos = function(s,file,line) { var $x = ["FilePos",2,s,file,line]; $x.__enum__ = haxe.StackItem; $x.toString = $estr; return $x; }
haxe.StackItem.Method = function(classname,method) { var $x = ["Method",3,classname,method]; $x.__enum__ = haxe.StackItem; $x.toString = $estr; return $x; }
haxe.StackItem.Lambda = function(v) { var $x = ["Lambda",4,v]; $x.__enum__ = haxe.StackItem; $x.toString = $estr; return $x; }
haxe._Template = {}
haxe._Template.TemplateExpr = $hxClasses["haxe._Template.TemplateExpr"] = { __ename__ : ["haxe","_Template","TemplateExpr"], __constructs__ : ["OpVar","OpExpr","OpIf","OpStr","OpBlock","OpForeach","OpMacro"] }
haxe._Template.TemplateExpr.OpVar = function(v) { var $x = ["OpVar",0,v]; $x.__enum__ = haxe._Template.TemplateExpr; $x.toString = $estr; return $x; }
haxe._Template.TemplateExpr.OpExpr = function(expr) { var $x = ["OpExpr",1,expr]; $x.__enum__ = haxe._Template.TemplateExpr; $x.toString = $estr; return $x; }
haxe._Template.TemplateExpr.OpIf = function(expr,eif,eelse) { var $x = ["OpIf",2,expr,eif,eelse]; $x.__enum__ = haxe._Template.TemplateExpr; $x.toString = $estr; return $x; }
haxe._Template.TemplateExpr.OpStr = function(str) { var $x = ["OpStr",3,str]; $x.__enum__ = haxe._Template.TemplateExpr; $x.toString = $estr; return $x; }
haxe._Template.TemplateExpr.OpBlock = function(l) { var $x = ["OpBlock",4,l]; $x.__enum__ = haxe._Template.TemplateExpr; $x.toString = $estr; return $x; }
haxe._Template.TemplateExpr.OpForeach = function(expr,loop) { var $x = ["OpForeach",5,expr,loop]; $x.__enum__ = haxe._Template.TemplateExpr; $x.toString = $estr; return $x; }
haxe._Template.TemplateExpr.OpMacro = function(name,params) { var $x = ["OpMacro",6,name,params]; $x.__enum__ = haxe._Template.TemplateExpr; $x.toString = $estr; return $x; }
haxe.Timer = function() { }
$hxClasses["haxe.Timer"] = haxe.Timer;
haxe.Timer.__name__ = ["haxe","Timer"];
haxe.Timer.stamp = function() {
	return Date.now().getTime() / 1000;
}
haxe.Timer.prototype = {
	__class__: haxe.Timer
}
haxe.Unserializer = function(buf) {
	this.buf = buf;
	this.length = buf.length;
	this.pos = 0;
	this.scache = new Array();
	this.cache = new Array();
	var r = haxe.Unserializer.DEFAULT_RESOLVER;
	if(r == null) {
		r = Type;
		haxe.Unserializer.DEFAULT_RESOLVER = r;
	}
	this.setResolver(r);
};
$hxClasses["haxe.Unserializer"] = haxe.Unserializer;
haxe.Unserializer.__name__ = ["haxe","Unserializer"];
haxe.Unserializer.initCodes = function() {
	var codes = new Array();
	var _g1 = 0, _g = haxe.Unserializer.BASE64.length;
	while(_g1 < _g) {
		var i = _g1++;
		codes[haxe.Unserializer.BASE64.cca(i)] = i;
	}
	return codes;
}
haxe.Unserializer.run = function(v) {
	return new haxe.Unserializer(v).unserialize();
}
haxe.Unserializer.prototype = {
	buf: null
	,pos: null
	,length: null
	,cache: null
	,scache: null
	,resolver: null
	,setResolver: function(r) {
		if(r == null) this.resolver = { resolveClass : function(_) {
			return null;
		}, resolveEnum : function(_) {
			return null;
		}}; else this.resolver = r;
	}
	,get: function(p) {
		return this.buf.cca(p);
	}
	,readDigits: function() {
		var k = 0;
		var s = false;
		var fpos = this.pos;
		while(true) {
			var c = this.buf.cca(this.pos);
			if(c != c) break;
			if(c == 45) {
				if(this.pos != fpos) break;
				s = true;
				this.pos++;
				continue;
			}
			if(c < 48 || c > 57) break;
			k = k * 10 + (c - 48);
			this.pos++;
		}
		if(s) k *= -1;
		return k;
	}
	,unserializeObject: function(o) {
		while(true) {
			if(this.pos >= this.length) throw "Invalid object";
			if(this.buf.cca(this.pos) == 103) break;
			var k = this.unserialize();
			if(!Std["is"](k,String)) throw "Invalid object key";
			var v = this.unserialize();
			o[k] = v;
		}
		this.pos++;
	}
	,unserializeEnum: function(edecl,tag) {
		if(this.buf.cca(this.pos++) != 58) throw "Invalid enum format";
		var nargs = this.readDigits();
		if(nargs == 0) return Type.createEnum(edecl,tag);
		var args = new Array();
		while(nargs-- > 0) args.push(this.unserialize());
		return Type.createEnum(edecl,tag,args);
	}
	,unserialize: function() {
		switch(this.buf.cca(this.pos++)) {
		case 110:
			return null;
		case 116:
			return true;
		case 102:
			return false;
		case 122:
			return 0;
		case 105:
			return this.readDigits();
		case 100:
			var p1 = this.pos;
			while(true) {
				var c = this.buf.cca(this.pos);
				if(c >= 43 && c < 58 || c == 101 || c == 69) this.pos++; else break;
			}
			return Std.parseFloat(this.buf.substr(p1,this.pos - p1));
		case 121:
			var len = this.readDigits();
			if(this.buf.cca(this.pos++) != 58 || this.length - this.pos < len) throw "Invalid string length";
			var s = this.buf.substr(this.pos,len);
			this.pos += len;
			s = StringTools.urlDecode(s);
			this.scache.push(s);
			return s;
		case 107:
			return Math.NaN;
		case 109:
			return Math.NEGATIVE_INFINITY;
		case 112:
			return Math.POSITIVE_INFINITY;
		case 97:
			var buf = this.buf;
			var a = new Array();
			this.cache.push(a);
			while(true) {
				var c = this.buf.cca(this.pos);
				if(c == 104) {
					this.pos++;
					break;
				}
				if(c == 117) {
					this.pos++;
					var n = this.readDigits();
					a[a.length + n - 1] = null;
				} else a.push(this.unserialize());
			}
			return a;
		case 111:
			var o = { };
			this.cache.push(o);
			this.unserializeObject(o);
			return o;
		case 114:
			var n = this.readDigits();
			if(n < 0 || n >= this.cache.length) throw "Invalid reference";
			return this.cache[n];
		case 82:
			var n = this.readDigits();
			if(n < 0 || n >= this.scache.length) throw "Invalid string reference";
			return this.scache[n];
		case 120:
			throw this.unserialize();
			break;
		case 99:
			var name = this.unserialize();
			var cl = this.resolver.resolveClass(name);
			if(cl == null) throw "Class not found " + name;
			var o = Type.createEmptyInstance(cl);
			this.cache.push(o);
			this.unserializeObject(o);
			return o;
		case 119:
			var name = this.unserialize();
			var edecl = this.resolver.resolveEnum(name);
			if(edecl == null) throw "Enum not found " + name;
			var e = this.unserializeEnum(edecl,this.unserialize());
			this.cache.push(e);
			return e;
		case 106:
			var name = this.unserialize();
			var edecl = this.resolver.resolveEnum(name);
			if(edecl == null) throw "Enum not found " + name;
			this.pos++;
			var index = this.readDigits();
			var tag = Type.getEnumConstructs(edecl)[index];
			if(tag == null) throw "Unknown enum index " + name + "@" + index;
			var e = this.unserializeEnum(edecl,tag);
			this.cache.push(e);
			return e;
		case 108:
			var l = new List();
			this.cache.push(l);
			var buf = this.buf;
			while(this.buf.cca(this.pos) != 104) l.add(this.unserialize());
			this.pos++;
			return l;
		case 98:
			var h = new Hash();
			this.cache.push(h);
			var buf = this.buf;
			while(this.buf.cca(this.pos) != 104) {
				var s = this.unserialize();
				h.set(s,this.unserialize());
			}
			this.pos++;
			return h;
		case 113:
			var h = new IntHash();
			this.cache.push(h);
			var buf = this.buf;
			var c = this.buf.cca(this.pos++);
			while(c == 58) {
				var i = this.readDigits();
				h.set(i,this.unserialize());
				c = this.buf.cca(this.pos++);
			}
			if(c != 104) throw "Invalid IntHash format";
			return h;
		case 118:
			var d = Date.fromString(this.buf.substr(this.pos,19));
			this.cache.push(d);
			this.pos += 19;
			return d;
		case 115:
			var len = this.readDigits();
			var buf = this.buf;
			if(this.buf.cca(this.pos++) != 58 || this.length - this.pos < len) throw "Invalid bytes length";
			var codes = haxe.Unserializer.CODES;
			if(codes == null) {
				codes = haxe.Unserializer.initCodes();
				haxe.Unserializer.CODES = codes;
			}
			var i = this.pos;
			var rest = len & 3;
			var size = (len >> 2) * 3 + (rest >= 2?rest - 1:0);
			var max = i + (len - rest);
			var bytes = haxe.io.Bytes.alloc(size);
			var bpos = 0;
			while(i < max) {
				var c1 = codes[buf.cca(i++)];
				var c2 = codes[buf.cca(i++)];
				bytes.b[bpos++] = (c1 << 2 | c2 >> 4) & 255;
				var c3 = codes[buf.cca(i++)];
				bytes.b[bpos++] = (c2 << 4 | c3 >> 2) & 255;
				var c4 = codes[buf.cca(i++)];
				bytes.b[bpos++] = (c3 << 6 | c4) & 255;
			}
			if(rest >= 2) {
				var c1 = codes[buf.cca(i++)];
				var c2 = codes[buf.cca(i++)];
				bytes.b[bpos++] = (c1 << 2 | c2 >> 4) & 255;
				if(rest == 3) {
					var c3 = codes[buf.cca(i++)];
					bytes.b[bpos++] = (c2 << 4 | c3 >> 2) & 255;
				}
			}
			this.pos += len;
			this.cache.push(bytes);
			return bytes;
		case 67:
			var name = this.unserialize();
			var cl = this.resolver.resolveClass(name);
			if(cl == null) throw "Class not found " + name;
			var o = Type.createEmptyInstance(cl);
			this.cache.push(o);
			o.hxUnserialize(this);
			if(this.buf.cca(this.pos++) != 103) throw "Invalid custom data";
			return o;
		default:
		}
		this.pos--;
		throw "Invalid char " + this.buf.charAt(this.pos) + " at position " + this.pos;
	}
	,__class__: haxe.Unserializer
}
haxe.io = {}
haxe.io.Bytes = function(length,b) {
	this.length = length;
	this.b = b;
};
$hxClasses["haxe.io.Bytes"] = haxe.io.Bytes;
haxe.io.Bytes.__name__ = ["haxe","io","Bytes"];
haxe.io.Bytes.alloc = function(length) {
	var a = new Array();
	var _g = 0;
	while(_g < length) {
		var i = _g++;
		a.push(0);
	}
	return new haxe.io.Bytes(length,a);
}
haxe.io.Bytes.prototype = {
	length: null
	,b: null
	,set: function(pos,v) {
		this.b[pos] = v & 255;
	}
	,readString: function(pos,len) {
		if(pos < 0 || len < 0 || pos + len > this.length) throw haxe.io.Error.OutsideBounds;
		var s = "";
		var b = this.b;
		var fcc = String.fromCharCode;
		var i = pos;
		var max = pos + len;
		while(i < max) {
			var c = b[i++];
			if(c < 128) {
				if(c == 0) break;
				s += fcc(c);
			} else if(c < 224) s += fcc((c & 63) << 6 | b[i++] & 127); else if(c < 240) {
				var c2 = b[i++];
				s += fcc((c & 31) << 12 | (c2 & 127) << 6 | b[i++] & 127);
			} else {
				var c2 = b[i++];
				var c3 = b[i++];
				s += fcc((c & 15) << 18 | (c2 & 127) << 12 | c3 << 6 & 127 | b[i++] & 127);
			}
		}
		return s;
	}
	,toString: function() {
		return this.readString(0,this.length);
	}
	,__class__: haxe.io.Bytes
}
haxe.io.Error = $hxClasses["haxe.io.Error"] = { __ename__ : ["haxe","io","Error"], __constructs__ : ["Blocked","Overflow","OutsideBounds","Custom"] }
haxe.io.Error.Blocked = ["Blocked",0];
haxe.io.Error.Blocked.toString = $estr;
haxe.io.Error.Blocked.__enum__ = haxe.io.Error;
haxe.io.Error.Overflow = ["Overflow",1];
haxe.io.Error.Overflow.toString = $estr;
haxe.io.Error.Overflow.__enum__ = haxe.io.Error;
haxe.io.Error.OutsideBounds = ["OutsideBounds",2];
haxe.io.Error.OutsideBounds.toString = $estr;
haxe.io.Error.OutsideBounds.__enum__ = haxe.io.Error;
haxe.io.Error.Custom = function(e) { var $x = ["Custom",3,e]; $x.__enum__ = haxe.io.Error; $x.toString = $estr; return $x; }
haxe.io.Input = function() { }
$hxClasses["haxe.io.Input"] = haxe.io.Input;
haxe.io.Input.__name__ = ["haxe","io","Input"];
haxe.io.Input.prototype = {
	bigEndian: null
	,setEndian: function(b) {
		this.bigEndian = b;
		return b;
	}
	,__class__: haxe.io.Input
	,__properties__: {set_bigEndian:"setEndian"}
}
haxe.xml = {}
haxe.xml.Filter = $hxClasses["haxe.xml.Filter"] = { __ename__ : ["haxe","xml","Filter"], __constructs__ : ["FInt","FBool","FEnum","FReg"] }
haxe.xml.Filter.FInt = ["FInt",0];
haxe.xml.Filter.FInt.toString = $estr;
haxe.xml.Filter.FInt.__enum__ = haxe.xml.Filter;
haxe.xml.Filter.FBool = ["FBool",1];
haxe.xml.Filter.FBool.toString = $estr;
haxe.xml.Filter.FBool.__enum__ = haxe.xml.Filter;
haxe.xml.Filter.FEnum = function(values) { var $x = ["FEnum",2,values]; $x.__enum__ = haxe.xml.Filter; $x.toString = $estr; return $x; }
haxe.xml.Filter.FReg = function(matcher) { var $x = ["FReg",3,matcher]; $x.__enum__ = haxe.xml.Filter; $x.toString = $estr; return $x; }
haxe.xml.Attrib = $hxClasses["haxe.xml.Attrib"] = { __ename__ : ["haxe","xml","Attrib"], __constructs__ : ["Att"] }
haxe.xml.Attrib.Att = function(name,filter,defvalue) { var $x = ["Att",0,name,filter,defvalue]; $x.__enum__ = haxe.xml.Attrib; $x.toString = $estr; return $x; }
haxe.xml.Rule = $hxClasses["haxe.xml.Rule"] = { __ename__ : ["haxe","xml","Rule"], __constructs__ : ["RNode","RData","RMulti","RList","RChoice","ROptional"] }
haxe.xml.Rule.RNode = function(name,attribs,childs) { var $x = ["RNode",0,name,attribs,childs]; $x.__enum__ = haxe.xml.Rule; $x.toString = $estr; return $x; }
haxe.xml.Rule.RData = function(filter) { var $x = ["RData",1,filter]; $x.__enum__ = haxe.xml.Rule; $x.toString = $estr; return $x; }
haxe.xml.Rule.RMulti = function(rule,atLeastOne) { var $x = ["RMulti",2,rule,atLeastOne]; $x.__enum__ = haxe.xml.Rule; $x.toString = $estr; return $x; }
haxe.xml.Rule.RList = function(rules,ordered) { var $x = ["RList",3,rules,ordered]; $x.__enum__ = haxe.xml.Rule; $x.toString = $estr; return $x; }
haxe.xml.Rule.RChoice = function(choices) { var $x = ["RChoice",4,choices]; $x.__enum__ = haxe.xml.Rule; $x.toString = $estr; return $x; }
haxe.xml.Rule.ROptional = function(rule) { var $x = ["ROptional",5,rule]; $x.__enum__ = haxe.xml.Rule; $x.toString = $estr; return $x; }
haxe.xml._Check = {}
haxe.xml._Check.CheckResult = $hxClasses["haxe.xml._Check.CheckResult"] = { __ename__ : ["haxe","xml","_Check","CheckResult"], __constructs__ : ["CMatch","CMissing","CExtra","CElementExpected","CDataExpected","CExtraAttrib","CMissingAttrib","CInvalidAttrib","CInvalidData","CInElement"] }
haxe.xml._Check.CheckResult.CMatch = ["CMatch",0];
haxe.xml._Check.CheckResult.CMatch.toString = $estr;
haxe.xml._Check.CheckResult.CMatch.__enum__ = haxe.xml._Check.CheckResult;
haxe.xml._Check.CheckResult.CMissing = function(r) { var $x = ["CMissing",1,r]; $x.__enum__ = haxe.xml._Check.CheckResult; $x.toString = $estr; return $x; }
haxe.xml._Check.CheckResult.CExtra = function(x) { var $x = ["CExtra",2,x]; $x.__enum__ = haxe.xml._Check.CheckResult; $x.toString = $estr; return $x; }
haxe.xml._Check.CheckResult.CElementExpected = function(name,x) { var $x = ["CElementExpected",3,name,x]; $x.__enum__ = haxe.xml._Check.CheckResult; $x.toString = $estr; return $x; }
haxe.xml._Check.CheckResult.CDataExpected = function(x) { var $x = ["CDataExpected",4,x]; $x.__enum__ = haxe.xml._Check.CheckResult; $x.toString = $estr; return $x; }
haxe.xml._Check.CheckResult.CExtraAttrib = function(att,x) { var $x = ["CExtraAttrib",5,att,x]; $x.__enum__ = haxe.xml._Check.CheckResult; $x.toString = $estr; return $x; }
haxe.xml._Check.CheckResult.CMissingAttrib = function(att,x) { var $x = ["CMissingAttrib",6,att,x]; $x.__enum__ = haxe.xml._Check.CheckResult; $x.toString = $estr; return $x; }
haxe.xml._Check.CheckResult.CInvalidAttrib = function(att,x,f) { var $x = ["CInvalidAttrib",7,att,x,f]; $x.__enum__ = haxe.xml._Check.CheckResult; $x.toString = $estr; return $x; }
haxe.xml._Check.CheckResult.CInvalidData = function(x,f) { var $x = ["CInvalidData",8,x,f]; $x.__enum__ = haxe.xml._Check.CheckResult; $x.toString = $estr; return $x; }
haxe.xml._Check.CheckResult.CInElement = function(x,r) { var $x = ["CInElement",9,x,r]; $x.__enum__ = haxe.xml._Check.CheckResult; $x.toString = $estr; return $x; }
jeash.Lib = function(title,width,height) {
	this.mKilled = false;
	this.__scr = js.Lib.document.getElementById(title);
	if(this.__scr == null) throw "Element with id '" + title + "' not found";
	this.__scr.style.setProperty("overflow","hidden","");
	this.__scr.style.setProperty("position","absolute","");
	this.__scr.style.width = width + "px";
	this.__scr.style.height = height + "px";
};
$hxClasses["jeash.Lib"] = jeash.Lib;
jeash.Lib.__name__ = ["jeash","Lib"];
jeash.Lib.__properties__ = {get_current:"jeashGetCurrent"}
jeash.Lib.mMe = null;
jeash.Lib.current = null;
jeash.Lib.mStage = null;
jeash.Lib.mMainClassRoot = null;
jeash.Lib.mCurrent = null;
jeash.Lib.trace = function(arg) {
	if(window.console != null) window.console.log(arg);
}
jeash.Lib.jeashGetCurrent = function() {
	if(jeash.Lib.mMainClassRoot == null) {
		jeash.Lib.mMainClassRoot = new jeash.display.MovieClip();
		jeash.Lib.mCurrent = jeash.Lib.mMainClassRoot;
		jeash.Lib.mCurrent.name = "Root MovieClip";
		jeash.Lib.jeashGetStage().addChild(jeash.Lib.mCurrent);
	}
	return jeash.Lib.mMainClassRoot;
}
jeash.Lib.starttime = null;
jeash.Lib.getTimer = function() {
	return (haxe.Timer.stamp() - jeash.Lib.starttime) * 1000 | 0;
}
jeash.Lib.jeashGetStage = function() {
	if(jeash.Lib.mStage == null) {
		var width = jeash.Lib.jeashGetWidth();
		var height = jeash.Lib.jeashGetHeight();
		jeash.Lib.mStage = new jeash.display.Stage(width,height);
	}
	return jeash.Lib.mStage;
}
jeash.Lib.jeashAppendSurface = function(surface,before) {
	if(jeash.Lib.mMe.__scr != null) {
		surface.style.setProperty("position","absolute","");
		surface.style.setProperty("left","0px","");
		surface.style.setProperty("top","0px","");
		surface.style.setProperty("-moz-transform-origin","0 0","");
		surface.style.setProperty("-webkit-transform-origin","0 0","");
		surface.style.setProperty("-o-transform-origin","0 0","");
		surface.style.setProperty("-ms-transform-origin","0 0","");
		try {
			if(surface.localName == "canvas") surface.onmouseover = surface.onselectstart = function() {
				return false;
			};
		} catch( e ) {
		}
		if(before != null) jeash.Lib.mMe.__scr.insertBefore(surface,before); else jeash.Lib.mMe.__scr.appendChild(surface);
	}
}
jeash.Lib.jeashSwapSurface = function(surface1,surface2) {
	var c1 = -1;
	var c2 = -1;
	var swap;
	var _g1 = 0, _g = jeash.Lib.mMe.__scr.childNodes.length;
	while(_g1 < _g) {
		var i = _g1++;
		if(jeash.Lib.mMe.__scr.childNodes[i] == surface1) c1 = i; else if(jeash.Lib.mMe.__scr.childNodes[i] == surface2) c2 = i;
	}
	if(c1 != -1 && c2 != -1) {
		swap = jeash.Lib.jeashRemoveSurface(jeash.Lib.mMe.__scr.childNodes[c1]);
		if(c2 > c1) c2--;
		if(c2 < jeash.Lib.mMe.__scr.childNodes.length - 1) jeash.Lib.mMe.__scr.insertBefore(swap,jeash.Lib.mMe.__scr.childNodes[c2++]); else jeash.Lib.mMe.__scr.appendChild(swap);
		swap = jeash.Lib.jeashRemoveSurface(jeash.Lib.mMe.__scr.childNodes[c2]);
		if(c1 > c2) c1--;
		if(c1 < jeash.Lib.mMe.__scr.childNodes.length - 1) jeash.Lib.mMe.__scr.insertBefore(swap,jeash.Lib.mMe.__scr.childNodes[c1++]); else jeash.Lib.mMe.__scr.appendChild(swap);
	}
}
jeash.Lib.jeashIsOnStage = function(surface) {
	var _g1 = 0, _g = jeash.Lib.mMe.__scr.childNodes.length;
	while(_g1 < _g) {
		var i = _g1++;
		if(jeash.Lib.mMe.__scr.childNodes[i] == surface) return true;
	}
	return false;
}
jeash.Lib.jeashRemoveSurface = function(surface) {
	if(jeash.Lib.mMe.__scr != null) {
		var anim = surface.getAttribute("data-jeash-anim");
		if(anim != null) {
			var style = js.Lib.document.getElementById(anim);
			if(style != null) jeash.Lib.mMe.__scr.removeChild(style);
		}
		jeash.Lib.mMe.__scr.removeChild(surface);
	}
	return surface;
}
jeash.Lib.jeashSetSurfaceTransform = function(surface,matrix) {
	if(matrix.a == 1 && matrix.b == 0 && matrix.c == 0 && matrix.d == 1 && surface.getAttribute("data-jeash-anim") == null) {
		surface.style.left = matrix.tx + "px";
		surface.style.top = matrix.ty + "px";
	} else {
		surface.style.left = "0px";
		surface.style.top = "0px";
		surface.style.setProperty("-moz-transform","matrix(" + matrix.a.toFixed(4) + ", " + matrix.b.toFixed(4) + ", " + matrix.c.toFixed(4) + ", " + matrix.d.toFixed(4) + ", " + matrix.tx.toFixed(4) + "px, " + matrix.ty.toFixed(4) + "px)","");
		surface.style.setProperty("-webkit-transform","matrix(" + matrix.a.toFixed(4) + ", " + matrix.b.toFixed(4) + ", " + matrix.c.toFixed(4) + ", " + matrix.d.toFixed(4) + ", " + matrix.tx.toFixed(4) + ", " + matrix.ty.toFixed(4) + ")","");
		surface.style.setProperty("-o-transform","matrix(" + matrix.a.toFixed(4) + ", " + matrix.b.toFixed(4) + ", " + matrix.c.toFixed(4) + ", " + matrix.d.toFixed(4) + ", " + matrix.tx.toFixed(4) + ", " + matrix.ty.toFixed(4) + ")","");
		surface.style.setProperty("-ms-transform","matrix(" + matrix.a.toFixed(4) + ", " + matrix.b.toFixed(4) + ", " + matrix.c.toFixed(4) + ", " + matrix.d.toFixed(4) + ", " + matrix.tx.toFixed(4) + ", " + matrix.ty.toFixed(4) + ")","");
	}
}
jeash.Lib.jeashSetSurfaceOpacity = function(surface,alpha) {
	surface.style.setProperty("opacity",Std.string(alpha),"");
}
jeash.Lib.jeashCopyStyle = function(src,tgt) {
	tgt.id = src.id;
	var _g = 0, _g1 = ["left","top","-moz-transform","-moz-transform-origin","-webkit-transform","-webkit-transform-origin","-o-transform","-o-transform-origin","opacity","display"];
	while(_g < _g1.length) {
		var prop = _g1[_g];
		++_g;
		tgt.style.setProperty(prop,src.style.getPropertyValue(prop),"");
	}
}
jeash.Lib.jeashDrawToSurface = function(surface,tgt,matrix,alpha) {
	if(alpha == null) alpha = 1.0;
	var srcCtx = surface.getContext("2d");
	var tgtCtx = tgt.getContext("2d");
	if(alpha != 1.0) tgtCtx.globalAlpha = alpha;
	if(surface.width > 0 && surface.height > 0) {
		if(matrix != null) {
			tgtCtx.save();
			if(matrix.a == 1 && matrix.b == 0 && matrix.c == 0 && matrix.d == 1) tgtCtx.translate(matrix.tx,matrix.ty); else tgtCtx.setTransform(matrix.a,matrix.b,matrix.c,matrix.d,matrix.tx,matrix.ty);
			tgtCtx.drawImage(surface,0,0);
			tgtCtx.restore();
		} else tgtCtx.drawImage(surface,0,0);
	}
}
jeash.Lib.jeashDisableRightClick = function() {
	if(jeash.Lib.mMe != null) try {
		jeash.Lib.mMe.__scr.oncontextmenu = function() {
			return false;
		};
	} catch( e ) {
		jeash.Lib.trace("Disable right click not supported in this browser.");
	}
}
jeash.Lib.jeashEnableRightClick = function() {
	if(jeash.Lib.mMe != null) try {
		jeash.Lib.mMe.__scr.oncontextmenu = null;
	} catch( e ) {
	}
}
jeash.Lib.jeashEnableFullScreen = function() {
	if(jeash.Lib.mMe != null) {
		var origWidth = jeash.Lib.mMe.__scr.style.getPropertyValue("width");
		var origHeight = jeash.Lib.mMe.__scr.style.getPropertyValue("height");
		jeash.Lib.mMe.__scr.style.setProperty("width","100%","");
		jeash.Lib.mMe.__scr.style.setProperty("height","100%","");
		jeash.Lib.jeashDisableFullScreen = function() {
			jeash.Lib.mMe.__scr.style.setProperty("width",origWidth,"");
			jeash.Lib.mMe.__scr.style.setProperty("height",origHeight,"");
		};
	}
}
jeash.Lib.jeashDisableFullScreen = function() {
}
jeash.Lib.jeashFullScreenWidth = function() {
	var window = js.Lib.window;
	return window.innerWidth;
}
jeash.Lib.jeashFullScreenHeight = function() {
	var window = js.Lib.window;
	return window.innerHeight;
}
jeash.Lib.jeashSetCursor = function(type) {
	if(jeash.Lib.mMe != null) jeash.Lib.mMe.__scr.style.cursor = (function($this) {
		var $r;
		switch( (type)[1] ) {
		case 0:
			$r = "pointer";
			break;
		case 1:
			$r = "text";
			break;
		default:
			$r = "default";
		}
		return $r;
	}(this));
}
jeash.Lib.jeashSetSurfaceVisible = function(surface,visible) {
	if(visible) surface.style.setProperty("display","block",""); else surface.style.setProperty("display","none","");
}
jeash.Lib.jeashSetSurfaceId = function(surface,name) {
	if(surface.id == null || surface.id.length == 0) {
		var regex = new EReg("[^a-zA-Z0-9]","g");
		surface.id = regex.replace(name,"_");
	}
}
jeash.Lib.Run = function(tgt,width,height) {
	jeash.Lib.mMe = new jeash.Lib(tgt.id,width,height);
	var _g1 = 0, _g = tgt.attributes.length;
	while(_g1 < _g) {
		var i = _g1++;
		var attr = tgt.attributes.item(i);
		if(StringTools.startsWith(attr.name,"data-")) switch(attr.name) {
		case "data-" + "framerate":
			jeash.Lib.jeashGetStage().jeashSetFrameRate(Std.parseFloat(attr.value));
			break;
		default:
		}
	}
	var _g = 0, _g1 = ["touchstart","touchmove","touchend"];
	while(_g < _g1.length) {
		var type = _g1[_g];
		++_g;
		tgt.addEventListener(type,($_=jeash.Lib.jeashGetStage(),$_.jeashQueueStageEvent.$bind($_)),true);
	}
	var _g = 0, _g1 = ["resize","mouseup","mouseover","mouseout","mousemove","mousedown","mousewheel","dblclick","click"];
	while(_g < _g1.length) {
		var type = _g1[_g];
		++_g;
		tgt.addEventListener(type,($_=jeash.Lib.jeashGetStage(),$_.jeashQueueStageEvent.$bind($_)),true);
	}
	var window = js.Lib.window;
	var _g = 0, _g1 = ["keyup","keypress","keydown","resize"];
	while(_g < _g1.length) {
		var type = _g1[_g];
		++_g;
		window.addEventListener(type,($_=jeash.Lib.jeashGetStage(),$_.jeashQueueStageEvent.$bind($_)),false);
	}
	jeash.Lib.jeashGetStage().jeashSetBackgroundColour(tgt.style.backgroundColor != null && tgt.style.backgroundColor != ""?jeash.Lib.jeashParseColor(tgt.style.backgroundColor,function(res,pos,cur) {
		return (function($this) {
			var $r;
			switch(pos) {
			case 0:
				$r = res | cur << 16;
				break;
			case 1:
				$r = res | cur << 8;
				break;
			case 2:
				$r = res | cur;
				break;
			}
			return $r;
		}(this));
	}):16777215);
	jeash.Lib.jeashGetCurrent().jeashGetGraphics().beginFill(jeash.Lib.jeashGetStage().jeashGetBackgroundColour());
	jeash.Lib.jeashGetCurrent().jeashGetGraphics().drawRect(0,0,width,height);
	jeash.Lib.jeashSetSurfaceId(jeash.Lib.jeashGetCurrent().jeashGetGraphics().jeashSurface,"Root MovieClip");
	jeash.Lib.jeashGetStage().jeashUpdateNextWake();
	return jeash.Lib.mMe;
}
jeash.Lib.jeashParseColor = function(str,cb) {
	var re = new EReg("rgb\\(([0-9]*), ?([0-9]*), ?([0-9]*)\\)","");
	var hex = new EReg("#([0-9a-zA-Z][0-9a-zA-Z])([0-9a-zA-Z][0-9a-zA-Z])([0-9a-zA-Z][0-9a-zA-Z])","");
	if(re.match(str)) {
		var col = 0;
		var _g = 1;
		while(_g < 4) {
			var pos = _g++;
			var v = Std.parseInt(re.matched(pos));
			col = cb(col,pos - 1,v);
		}
		return col;
	} else if(hex.match(str)) {
		var col = 0;
		var _g = 1;
		while(_g < 4) {
			var pos = _g++;
			var v = "0x" + hex.matched(pos) & 255;
			v = cb(col,pos - 1,v);
		}
		return col;
	} else throw "Cannot parse color '" + str + "'.";
}
jeash.Lib.jeashGetWidth = function() {
	var tgt = jeash.Lib.mMe != null?jeash.Lib.mMe.__scr:js.Lib.document.getElementById("haxe:jeash");
	return tgt.clientWidth > 0?tgt.clientWidth:500;
}
jeash.Lib.jeashGetHeight = function() {
	var tgt = jeash.Lib.mMe != null?jeash.Lib.mMe.__scr:js.Lib.document.getElementById("haxe:jeash");
	return tgt.clientHeight > 0?tgt.clientHeight:500;
}
jeash.Lib.jeashBootstrap = function() {
	if(jeash.Lib.mMe == null) {
		var tgt = js.Lib.document.getElementById("haxe:jeash");
		jeash.Lib.Run(tgt,jeash.Lib.jeashGetWidth(),jeash.Lib.jeashGetHeight());
	}
}
jeash.Lib.prototype = {
	mKilled: null
	,__scr: null
	,__class__: jeash.Lib
}
jeash._Lib = {}
jeash._Lib.CursorType = $hxClasses["jeash._Lib.CursorType"] = { __ename__ : ["jeash","_Lib","CursorType"], __constructs__ : ["Pointer","Text","Default"] }
jeash._Lib.CursorType.Pointer = ["Pointer",0];
jeash._Lib.CursorType.Pointer.toString = $estr;
jeash._Lib.CursorType.Pointer.__enum__ = jeash._Lib.CursorType;
jeash._Lib.CursorType.Text = ["Text",1];
jeash._Lib.CursorType.Text.toString = $estr;
jeash._Lib.CursorType.Text.__enum__ = jeash._Lib.CursorType;
jeash._Lib.CursorType.Default = ["Default",2];
jeash._Lib.CursorType.Default.toString = $estr;
jeash._Lib.CursorType.Default.__enum__ = jeash._Lib.CursorType;
jeash.display.Bitmap = function(inBitmapData,inPixelSnapping,inSmoothing) {
	jeash.display.DisplayObject.call(this);
	this.pixelSnapping = inPixelSnapping;
	this.smoothing = inSmoothing;
	this.name = "Bitmap_" + jeash.display.DisplayObject.mNameID++;
	this.jeashGraphics = new jeash.display.Graphics();
	jeash.Lib.jeashSetSurfaceId(this.jeashGraphics.jeashSurface,this.name);
	if(inBitmapData != null) {
		this.jeashSetBitmapData(inBitmapData);
		this.jeashRender(null,null);
	}
};
$hxClasses["jeash.display.Bitmap"] = jeash.display.Bitmap;
jeash.display.Bitmap.__name__ = ["jeash","display","Bitmap"];
jeash.display.Bitmap.__super__ = jeash.display.DisplayObject;
jeash.display.Bitmap.prototype = $extend(jeash.display.DisplayObject.prototype,{
	bitmapData: null
	,pixelSnapping: null
	,smoothing: null
	,jeashGraphics: null
	,jeashCurrentLease: null
	,jeashSetBitmapData: function(inBitmapData) {
		this.jeashInvalidateBounds();
		this.bitmapData = inBitmapData;
		return inBitmapData;
	}
	,jeashGetGraphics: function() {
		return this.jeashGraphics;
	}
	,BuildBounds: function() {
		jeash.display.DisplayObject.prototype.BuildBounds.call(this);
		if(this.bitmapData != null) {
			var r = new jeash.geom.Rectangle(0,0,this.bitmapData.getWidth(),this.bitmapData.getHeight());
			if(r.width != 0 || r.height != 0) {
				if(this.mBoundsRect.width == 0 && this.mBoundsRect.height == 0) this.mBoundsRect = r.clone(); else this.mBoundsRect.extendBounds(r);
			}
		}
	}
	,jeashApplyFilters: function(surface) {
		if(this.jeashFilters != null) {
			var _g = 0, _g1 = this.jeashFilters;
			while(_g < _g1.length) {
				var filter = _g1[_g];
				++_g;
				filter.jeashApplyFilter(this.jeashGraphics.jeashSurface);
			}
		}
	}
	,jeashRender: function(parentMatrix,inMask) {
		if(this.bitmapData == null) return;
		if(this.mMtxDirty || this.mMtxChainDirty) this.jeashValidateMatrix();
		var m = this.mFullMatrix.clone();
		var imageDataLease = this.bitmapData.jeashLease;
		if(imageDataLease != null && (this.jeashCurrentLease == null || imageDataLease.seed != this.jeashCurrentLease.seed || imageDataLease.time != this.jeashCurrentLease.time)) {
			var srcCanvas = this.bitmapData.mTextureBuffer;
			this.jeashGraphics.jeashSurface.width = srcCanvas.width;
			this.jeashGraphics.jeashSurface.height = srcCanvas.height;
			this.jeashGraphics.clear();
			jeash.Lib.jeashDrawToSurface(srcCanvas,this.jeashGraphics.jeashSurface);
			this.jeashCurrentLease = imageDataLease.clone();
			this.jeashApplyFilters(this.jeashGraphics.jeashSurface);
		} else if(inMask != null) this.jeashApplyFilters(this.jeashGraphics.jeashSurface);
		if(inMask != null) jeash.Lib.jeashDrawToSurface(this.jeashGraphics.jeashSurface,inMask,m,(this.parent != null?this.parent.alpha:1) * this.alpha); else {
			jeash.Lib.jeashSetSurfaceTransform(this.jeashGraphics.jeashSurface,m);
			jeash.Lib.jeashSetSurfaceOpacity(this.jeashGraphics.jeashSurface,(this.parent != null?this.parent.alpha:1) * this.alpha);
		}
	}
	,jeashGetObjectUnderPoint: function(point) {
		if(!this.jeashGetVisible()) return null; else if(this.bitmapData != null) {
			var local = this.globalToLocal(point);
			if(local.x < 0 || local.y < 0 || local.x > this.jeashGetWidth() || local.y > this.jeashGetHeight()) return null; else return this;
		} else return jeash.display.DisplayObject.prototype.jeashGetObjectUnderPoint.call(this,point);
	}
	,__class__: jeash.display.Bitmap
	,__properties__: $extend(jeash.display.DisplayObject.prototype.__properties__,{set_bitmapData:"jeashSetBitmapData"})
});
jeash.display.ImageDataLease = function() {
};
$hxClasses["jeash.display.ImageDataLease"] = jeash.display.ImageDataLease;
jeash.display.ImageDataLease.__name__ = ["jeash","display","ImageDataLease"];
jeash.display.ImageDataLease.prototype = {
	seed: null
	,time: null
	,set: function(s,t) {
		this.seed = s;
		this.time = t;
	}
	,clone: function() {
		var leaseClone = new jeash.display.ImageDataLease();
		leaseClone.seed = this.seed;
		leaseClone.time = this.time;
		return leaseClone;
	}
	,__class__: jeash.display.ImageDataLease
}
jeash.display.BitmapData = function(inWidth,inHeight,inTransparent,inFillColor) {
	if(inTransparent == null) inTransparent = true;
	this.jeashLocked = false;
	this.jeashLeaseNum = 0;
	this.jeashLease = new jeash.display.ImageDataLease();
	this.jeashLease.set(this.jeashLeaseNum++,Date.now().getTime());
	this.mTextureBuffer = js.Lib.document.createElement("canvas");
	this.mTextureBuffer.width = inWidth;
	this.mTextureBuffer.height = inHeight;
	jeash.Lib.jeashSetSurfaceId(this.mTextureBuffer,"BitmapData " + jeash.display.BitmapData.mNameID++);
	this.jeashTransparent = inTransparent;
	this.rect = new jeash.geom.Rectangle(0,0,inWidth,inHeight);
	if(this.jeashTransparent) {
		this.jeashTransparentFiller = js.Lib.document.createElement("canvas");
		this.jeashTransparentFiller.width = inWidth;
		this.jeashTransparentFiller.height = inHeight;
		var ctx = this.jeashTransparentFiller.getContext("2d");
		ctx.fillStyle = "rgba(0,0,0,0);";
		ctx.fill();
	}
	if(inFillColor != null) {
		if(!this.jeashTransparent) inFillColor |= -16777216;
		this.jeashInitColor = inFillColor;
		this.jeashFillRect(this.rect,inFillColor);
	}
};
$hxClasses["jeash.display.BitmapData"] = jeash.display.BitmapData;
jeash.display.BitmapData.__name__ = ["jeash","display","BitmapData"];
jeash.display.BitmapData.__interfaces__ = [jeash.display.IBitmapDrawable];
jeash.display.BitmapData.prototype = {
	mTextureBuffer: null
	,jeashTransparent: null
	,width: null
	,height: null
	,rect: null
	,jeashImageData: null
	,jeashImageDataChanged: null
	,jeashLocked: null
	,jeashLease: null
	,jeashLeaseNum: null
	,jeashInitColor: null
	,jeashTransparentFiller: null
	,jeashFillRect: function(rect,color) {
		this.jeashLease.set(this.jeashLeaseNum++,Date.now().getTime());
		var ctx = this.mTextureBuffer.getContext("2d");
		var r = (color & 16711680) >>> 16;
		var g = (color & 65280) >>> 8;
		var b = color & 255;
		var a = this.jeashTransparent?color >>> 24:255;
		if(!this.jeashLocked) {
			if(this.jeashTransparent) {
				var trpCtx = this.jeashTransparentFiller.getContext("2d");
				var trpData = trpCtx.getImageData(rect.x,rect.y,rect.width,rect.height);
				ctx.putImageData(trpData,rect.x,rect.y);
			}
			var style = "rgba(";
			style += r;
			style += ", ";
			style += g;
			style += ", ";
			style += b;
			style += ", ";
			style += a / 256;
			style += ")";
			ctx.fillStyle = style;
			ctx.fillRect(rect.x,rect.y,rect.width,rect.height);
		} else {
			var s = 4 * (Math.round(rect.x) + Math.round(rect.y) * this.jeashImageData.width);
			var offsetY;
			var offsetX;
			var _g1 = 0, _g = Math.round(rect.height);
			while(_g1 < _g) {
				var i = _g1++;
				offsetY = i * this.jeashImageData.width;
				var _g3 = 0, _g2 = Math.round(rect.width);
				while(_g3 < _g2) {
					var j = _g3++;
					offsetX = 4 * (j + offsetY);
					this.jeashImageData.data[s + offsetX] = r;
					this.jeashImageData.data[s + offsetX + 1] = g;
					this.jeashImageData.data[s + offsetX + 2] = b;
					this.jeashImageData.data[s + offsetX + 3] = a;
				}
			}
			this.jeashImageDataChanged = true;
			ctx.putImageData(this.jeashImageData,0,0,rect.x,rect.y,rect.width,rect.height);
		}
	}
	,handle: function() {
		return this.mTextureBuffer;
	}
	,getWidth: function() {
		if(this.mTextureBuffer != null) return this.mTextureBuffer.width; else return 0;
	}
	,getHeight: function() {
		if(this.mTextureBuffer != null) return this.mTextureBuffer.height; else return 0;
	}
	,jeashOnLoad: function(data,e) {
		var canvas = data.texture;
		var width = data.image.width;
		var height = data.image.height;
		canvas.width = width;
		canvas.height = height;
		var ctx = canvas.getContext("2d");
		ctx.drawImage(data.image,0,0,width,height);
		data.bitmapData.width = width;
		data.bitmapData.height = height;
		data.bitmapData.rect = new jeash.geom.Rectangle(0,0,width,height);
		data.bitmapData.jeashBuildLease();
		if(data.inLoader != null) {
			var e1 = new jeash.events.Event(jeash.events.Event.COMPLETE);
			e1.target = data.inLoader;
			data.inLoader.dispatchEvent(e1);
		}
	}
	,jeashLoadFromFile: function(inFilename,inLoader) {
		var me = this;
		var image = js.Lib.document.createElement("img");
		if(inLoader != null) {
			var data = { image : image, texture : this.mTextureBuffer, inLoader : inLoader, bitmapData : this};
			image.addEventListener("load",(function(f,a1) {
				return function(a2) {
					return f(a1,a2);
				};
			})(this.jeashOnLoad.$bind(this),data),false);
			image.addEventListener("error",function(e) {
				if(!image.complete) me.jeashOnLoad(data,e);
			},false);
		}
		image.src = inFilename;
	}
	,drawToSurface: function(inSurface,matrix,colorTransform,blendMode,clipRect,smothing) {
		var ctx = inSurface.getContext("2d");
		ctx.save();
		if(matrix != null) {
			ctx.save();
			if(matrix.a == 1 && matrix.b == 0 && matrix.c == 0 && matrix.d == 1) ctx.translate(matrix.tx,matrix.ty); else ctx.setTransform(matrix.a,matrix.b,matrix.c,matrix.d,matrix.tx,matrix.ty);
			ctx.restore();
		}
		this.jeashLease.set(this.jeashLeaseNum++,Date.now().getTime());
		ctx.drawImage(this.mTextureBuffer,0,0);
		ctx.restore();
	}
	,jeashGetLease: function() {
		return this.jeashLease;
	}
	,jeashBuildLease: function() {
		this.jeashLease.set(this.jeashLeaseNum++,Date.now().getTime());
	}
	,__class__: jeash.display.BitmapData
	,__properties__: {get_height:"getHeight",get_width:"getWidth"}
}
jeash.display.CapsStyle = $hxClasses["jeash.display.CapsStyle"] = { __ename__ : ["jeash","display","CapsStyle"], __constructs__ : ["NONE","ROUND","SQUARE"] }
jeash.display.CapsStyle.NONE = ["NONE",0];
jeash.display.CapsStyle.NONE.toString = $estr;
jeash.display.CapsStyle.NONE.__enum__ = jeash.display.CapsStyle;
jeash.display.CapsStyle.ROUND = ["ROUND",1];
jeash.display.CapsStyle.ROUND.toString = $estr;
jeash.display.CapsStyle.ROUND.__enum__ = jeash.display.CapsStyle;
jeash.display.CapsStyle.SQUARE = ["SQUARE",2];
jeash.display.CapsStyle.SQUARE.toString = $estr;
jeash.display.CapsStyle.SQUARE.__enum__ = jeash.display.CapsStyle;
jeash.display.GradientType = $hxClasses["jeash.display.GradientType"] = { __ename__ : ["jeash","display","GradientType"], __constructs__ : ["RADIAL","LINEAR"] }
jeash.display.GradientType.RADIAL = ["RADIAL",0];
jeash.display.GradientType.RADIAL.toString = $estr;
jeash.display.GradientType.RADIAL.__enum__ = jeash.display.GradientType;
jeash.display.GradientType.LINEAR = ["LINEAR",1];
jeash.display.GradientType.LINEAR.toString = $estr;
jeash.display.GradientType.LINEAR.__enum__ = jeash.display.GradientType;
jeash.display.GfxPoint = function(inX,inY,inCX,inCY,inType) {
	this.x = inX;
	this.y = inY;
	this.cx = inCX;
	this.cy = inCY;
	this.type = inType;
};
$hxClasses["jeash.display.GfxPoint"] = jeash.display.GfxPoint;
jeash.display.GfxPoint.__name__ = ["jeash","display","GfxPoint"];
jeash.display.GfxPoint.prototype = {
	x: null
	,y: null
	,cx: null
	,cy: null
	,type: null
	,__class__: jeash.display.GfxPoint
}
jeash.display.LineJob = function(inGrad,inPoint_idx0,inPoint_idx1,inThickness,inAlpha,inColour,inPixel_hinting,inJoints,inCaps,inScale_mode,inMiter_limit) {
	this.grad = inGrad;
	this.point_idx0 = inPoint_idx0;
	this.point_idx1 = inPoint_idx1;
	this.thickness = inThickness;
	this.alpha = inAlpha;
	this.colour = inColour;
	this.pixel_hinting = inPixel_hinting;
	this.joints = inJoints;
	this.caps = inCaps;
	this.scale_mode = inScale_mode;
	this.miter_limit = inMiter_limit;
};
$hxClasses["jeash.display.LineJob"] = jeash.display.LineJob;
jeash.display.LineJob.__name__ = ["jeash","display","LineJob"];
jeash.display.LineJob.prototype = {
	grad: null
	,point_idx0: null
	,point_idx1: null
	,thickness: null
	,alpha: null
	,colour: null
	,pixel_hinting: null
	,joints: null
	,caps: null
	,scale_mode: null
	,miter_limit: null
	,__class__: jeash.display.LineJob
}
jeash.display.PointInPathMode = $hxClasses["jeash.display.PointInPathMode"] = { __ename__ : ["jeash","display","PointInPathMode"], __constructs__ : ["USER_SPACE","DEVICE_SPACE"] }
jeash.display.PointInPathMode.USER_SPACE = ["USER_SPACE",0];
jeash.display.PointInPathMode.USER_SPACE.toString = $estr;
jeash.display.PointInPathMode.USER_SPACE.__enum__ = jeash.display.PointInPathMode;
jeash.display.PointInPathMode.DEVICE_SPACE = ["DEVICE_SPACE",1];
jeash.display.PointInPathMode.DEVICE_SPACE.toString = $estr;
jeash.display.PointInPathMode.DEVICE_SPACE.__enum__ = jeash.display.PointInPathMode;
jeash.display.Graphics = function(inSurface) {
	jeash.Lib.jeashBootstrap();
	if(inSurface == null) {
		this.jeashSurface = js.Lib.document.createElement("canvas");
		this.jeashSurface.width = 0;
		this.jeashSurface.height = 0;
	} else this.jeashSurface = inSurface;
	this.mLastMoveID = 0;
	this.mPenX = 0.0;
	this.mPenY = 0.0;
	this.mDrawList = new Array();
	this.mPoints = [];
	this.mSolidGradient = null;
	this.mBitmap = null;
	this.mFilling = false;
	this.mFillColour = 0;
	this.mFillAlpha = 0.0;
	this.mLastMoveID = 0;
	this.jeashClearLine();
	this.mLineJobs = [];
	this.jeashChanged = true;
	this.nextDrawIndex = 0;
	this.jeashExtent = new jeash.geom.Rectangle();
	this.jeashClearNextCycle = true;
};
$hxClasses["jeash.display.Graphics"] = jeash.display.Graphics;
jeash.display.Graphics.__name__ = ["jeash","display","Graphics"];
jeash.display.Graphics.jeashDetectIsPointInPathMode = function() {
	var canvas = js.Lib.document.createElement("canvas");
	var ctx = canvas.getContext("2d");
	if(ctx.isPointInPath == null) return jeash.display.PointInPathMode.USER_SPACE;
	ctx.save();
	ctx.translate(1,0);
	ctx.beginPath();
	ctx.rect(0,0,1,1);
	var rv = ctx.isPointInPath(0.3,0.3)?jeash.display.PointInPathMode.USER_SPACE:jeash.display.PointInPathMode.DEVICE_SPACE;
	ctx.restore();
	return rv;
}
jeash.display.Graphics.prototype = {
	jeashSurface: null
	,jeashChanged: null
	,mPoints: null
	,mFilling: null
	,mFillColour: null
	,mFillAlpha: null
	,mSolidGradient: null
	,mBitmap: null
	,mCurrentLine: null
	,mLineJobs: null
	,mDrawList: null
	,mPenX: null
	,mPenY: null
	,mLastMoveID: null
	,jeashExtent: null
	,nextDrawIndex: null
	,jeashClearNextCycle: null
	,createCanvasColor: function(color,alpha) {
		var r;
		var g;
		var b;
		r = (16711680 & color) >> 16;
		g = (65280 & color) >> 8;
		b = 255 & color;
		return "rgba" + "(" + r + "," + g + "," + b + "," + alpha + ")";
	}
	,createCanvasGradient: function(ctx,g) {
		var gradient;
		var matrix = g.matrix;
		if((g.flags & 1) == 0) {
			var p1 = matrix.transformPoint(new jeash.geom.Point(-819.2,0));
			var p2 = matrix.transformPoint(new jeash.geom.Point(819.2,0));
			gradient = ctx.createLinearGradient(p1.x,p1.y,p2.x,p2.y);
		} else {
			var p1 = matrix.transformPoint(new jeash.geom.Point(g.focal * 819.2,0));
			var p2 = matrix.transformPoint(new jeash.geom.Point(0,819.2));
			gradient = ctx.createRadialGradient(p1.x,p1.y,0,p2.x,p1.y,p2.y);
		}
		var _g = 0, _g1 = g.points;
		while(_g < _g1.length) {
			var point = _g1[_g];
			++_g;
			var color = this.createCanvasColor(point.col,point.alpha);
			var pos = point.ratio / 255;
			gradient.addColorStop(pos,color);
		}
		return gradient;
	}
	,jeashRender: function(maskHandle,matrix) {
		if(!this.jeashChanged) return false;
		this.ClosePolygon(true);
		if(this.jeashExtent.width - this.jeashExtent.x > this.jeashSurface.width || this.jeashExtent.height - this.jeashExtent.y > this.jeashSurface.height) this.jeashAdjustSurface();
		if(this.jeashClearNextCycle) {
			this.jeashClearCanvas();
			this.jeashClearNextCycle = false;
		}
		var ctx = (function($this) {
			var $r;
			try {
				$r = $this.jeashSurface.getContext("2d");
			} catch( e ) {
				$r = null;
			}
			return $r;
		}(this));
		if(ctx == null) return false;
		var len = this.mDrawList.length;
		ctx.save();
		if(this.jeashExtent.x != 0 || this.jeashExtent.y != 0) ctx.translate(-this.jeashExtent.x,-this.jeashExtent.y);
		var _g = this.nextDrawIndex;
		while(_g < len) {
			var i = _g++;
			var d = this.mDrawList[len - 1 - i];
			if(d.lineJobs.length > 0) {
				var _g1 = 0, _g2 = d.lineJobs;
				while(_g1 < _g2.length) {
					var lj = _g2[_g1];
					++_g1;
					ctx.lineWidth = lj.thickness;
					switch(lj.joints) {
					case 0:
						ctx.lineJoin = "round";
						break;
					case 4096:
						ctx.lineJoin = "miter";
						break;
					case 8192:
						ctx.lineJoin = "bevel";
						break;
					}
					switch(lj.caps) {
					case 256:
						ctx.lineCap = "round";
						break;
					case 512:
						ctx.lineCap = "square";
						break;
					case 0:
						ctx.lineCap = "butt";
						break;
					}
					ctx.miterLimit = lj.miter_limit;
					if(lj.grad != null) ctx.strokeStyle = this.createCanvasGradient(ctx,lj.grad); else ctx.strokeStyle = this.createCanvasColor(lj.colour,lj.alpha);
					ctx.beginPath();
					var _g4 = lj.point_idx0, _g3 = lj.point_idx1 + 1;
					while(_g4 < _g3) {
						var i1 = _g4++;
						var p = d.points[i1];
						switch(p.type) {
						case 0:
							ctx.moveTo(p.x,p.y);
							break;
						case 2:
							ctx.quadraticCurveTo(p.cx,p.cy,p.x,p.y);
							break;
						default:
							ctx.lineTo(p.x,p.y);
						}
					}
					ctx.closePath();
					ctx.stroke();
				}
			} else {
				ctx.beginPath();
				var _g1 = 0, _g2 = d.points;
				while(_g1 < _g2.length) {
					var p = _g2[_g1];
					++_g1;
					switch(p.type) {
					case 0:
						ctx.moveTo(p.x,p.y);
						break;
					case 2:
						ctx.quadraticCurveTo(p.cx,p.cy,p.x,p.y);
						break;
					default:
						ctx.lineTo(p.x,p.y);
					}
				}
				ctx.closePath();
			}
			var fillColour = d.fillColour;
			var fillAlpha = d.fillAlpha;
			if(fillAlpha >= 0. && fillAlpha <= 1.) {
				var g = d.solidGradient;
				if(g != null) ctx.fillStyle = this.createCanvasGradient(ctx,g); else ctx.fillStyle = this.createCanvasColor(fillColour,fillAlpha);
			}
			ctx.fill();
			ctx.save();
			var bitmap = d.bitmap;
			if(bitmap != null) {
				ctx.clip();
				if(this.jeashExtent.x != 0 || this.jeashExtent.y != 0) ctx.translate(-this.jeashExtent.x,-this.jeashExtent.y);
				var img = bitmap.texture_buffer;
				var matrix1 = bitmap.matrix;
				if(matrix1 != null) ctx.transform(matrix1.a,matrix1.b,matrix1.c,matrix1.d,matrix1.tx,matrix1.ty);
				ctx.drawImage(img,0,0);
			}
			ctx.restore();
		}
		ctx.restore();
		this.jeashChanged = false;
		this.nextDrawIndex = len;
		return true;
	}
	,jeashHitTest: function(inX,inY) {
		var ctx = (function($this) {
			var $r;
			try {
				$r = $this.jeashSurface.getContext("2d");
			} catch( e ) {
				$r = null;
			}
			return $r;
		}(this));
		if(ctx == null) return false;
		ctx.save();
		var _g = 0, _g1 = this.mDrawList;
		while(_g < _g1.length) {
			var d = _g1[_g];
			++_g;
			ctx.beginPath();
			var _g2 = 0, _g3 = d.points;
			while(_g2 < _g3.length) {
				var p = _g3[_g2];
				++_g2;
				switch(p.type) {
				case 0:
					ctx.moveTo(p.x,p.y);
					break;
				case 2:
					ctx.quadraticCurveTo(p.cx,p.cy,p.x,p.y);
					break;
				default:
					ctx.lineTo(p.x,p.y);
				}
			}
			ctx.closePath();
			if(ctx.isPointInPath(inX,inY)) return true;
		}
		ctx.restore();
		return false;
	}
	,lineStyle: function(thickness,color,alpha,pixelHinting,scaleMode,caps,joints,miterLimit) {
		this.AddLineSegment();
		if(thickness == null) {
			this.jeashClearLine();
			return;
		} else {
			this.mCurrentLine.grad = null;
			this.mCurrentLine.thickness = thickness;
			this.mCurrentLine.colour = color == null?0:color;
			this.mCurrentLine.alpha = alpha == null?1.0:alpha;
			this.mCurrentLine.miter_limit = miterLimit == null?3.0:miterLimit;
			this.mCurrentLine.pixel_hinting = pixelHinting == null || !pixelHinting?0:16384;
		}
		if(caps != null) {
			switch( (caps)[1] ) {
			case 1:
				this.mCurrentLine.caps = 256;
				break;
			case 2:
				this.mCurrentLine.caps = 512;
				break;
			case 0:
				this.mCurrentLine.caps = 0;
				break;
			}
		}
		this.mCurrentLine.scale_mode = 3;
		if(scaleMode != null) {
			switch( (scaleMode)[1] ) {
			case 2:
				this.mCurrentLine.scale_mode = 3;
				break;
			case 3:
				this.mCurrentLine.scale_mode = 1;
				break;
			case 0:
				this.mCurrentLine.scale_mode = 2;
				break;
			case 1:
				this.mCurrentLine.scale_mode = 0;
				break;
			}
		}
		this.mCurrentLine.joints = 0;
		if(joints != null) {
			switch( (joints)[1] ) {
			case 1:
				this.mCurrentLine.joints = 0;
				break;
			case 0:
				this.mCurrentLine.joints = 4096;
				break;
			case 2:
				this.mCurrentLine.joints = 8192;
				break;
			}
		}
	}
	,beginFill: function(color,alpha) {
		this.ClosePolygon(true);
		this.mFillColour = color;
		this.mFillAlpha = alpha == null?1.0:alpha;
		this.mFilling = true;
		this.mSolidGradient = null;
		this.mBitmap = null;
	}
	,drawRect: function(x,y,width,height) {
		this.ClosePolygon(false);
		this.moveTo(x,y);
		this.lineTo(x + width,y);
		this.lineTo(x + width,y + height);
		this.lineTo(x,y + height);
		this.lineTo(x,y);
		this.ClosePolygon(false);
	}
	,drawRoundRect: function(x,y,width,height,rx,ry) {
		rx *= 0.5;
		ry *= 0.5;
		var w = width * 0.5;
		x += w;
		if(rx > w) rx = w;
		var lw = w - rx;
		var w_ = lw + rx * Math.sin(Math.PI / 4);
		var cw_ = lw + rx * Math.tan(Math.PI / 8);
		var h = height * 0.5;
		y += h;
		if(ry > h) ry = h;
		var lh = h - ry;
		var h_ = lh + ry * Math.sin(Math.PI / 4);
		var ch_ = lh + ry * Math.tan(Math.PI / 8);
		this.ClosePolygon(false);
		this.moveTo(x + w,y + lh);
		this.curveTo(x + w,y + ch_,x + w_,y + h_);
		this.curveTo(x + cw_,y + h,x + lw,y + h);
		this.lineTo(x - lw,y + h);
		this.curveTo(x - cw_,y + h,x - w_,y + h_);
		this.curveTo(x - w,y + ch_,x - w,y + lh);
		this.lineTo(x - w,y - lh);
		this.curveTo(x - w,y - ch_,x - w_,y - h_);
		this.curveTo(x - cw_,y - h,x - lw,y - h);
		this.lineTo(x + lw,y - h);
		this.curveTo(x + cw_,y - h,x + w_,y - h_);
		this.curveTo(x + w,y - ch_,x + w,y - lh);
		this.lineTo(x + w,y + lh);
		this.ClosePolygon(false);
	}
	,jeashClearLine: function() {
		this.mCurrentLine = new jeash.display.LineJob(null,-1,-1,0.0,0.0,0,1,0,256,3,3.0);
	}
	,jeashClearCanvas: function() {
		if(this.jeashSurface != null) {
			var w = this.jeashSurface.width;
			this.jeashSurface.width = w;
		}
	}
	,clear: function() {
		this.jeashClearLine();
		this.mPenX = 0.0;
		this.mPenY = 0.0;
		this.mDrawList = new Array();
		this.nextDrawIndex = 0;
		this.mPoints = [];
		this.mSolidGradient = null;
		this.mFilling = false;
		this.mFillColour = 0;
		this.mFillAlpha = 0.0;
		this.mLastMoveID = 0;
		this.jeashClearNextCycle = true;
		this.mLineJobs = [];
	}
	,jeashExpandStandardExtent: function(x,y) {
		var maxX, minX, maxY, minY;
		minX = this.jeashExtent.x;
		minY = this.jeashExtent.y;
		maxX = this.jeashExtent.width + minX;
		maxY = this.jeashExtent.height + minY;
		maxX = x > maxX?x:maxX;
		minX = x < minX?x:minX;
		maxY = y > maxY?y:maxY;
		minY = y < minY?y:minY;
		this.jeashExtent.x = minX;
		this.jeashExtent.y = minY;
		this.jeashExtent.width = maxX - minX;
		this.jeashExtent.height = maxY - minY;
	}
	,moveTo: function(inX,inY) {
		this.mPenX = inX;
		this.mPenY = inY;
		this.jeashExpandStandardExtent(inX,inY);
		if(!this.mFilling) this.ClosePolygon(false); else {
			this.AddLineSegment();
			this.mLastMoveID = this.mPoints.length;
			this.mPoints.push(new jeash.display.GfxPoint(this.mPenX,this.mPenY,0.0,0.0,0));
		}
	}
	,lineTo: function(inX,inY) {
		var pid = this.mPoints.length;
		if(pid == 0) {
			this.mPoints.push(new jeash.display.GfxPoint(this.mPenX,this.mPenY,0.0,0.0,0));
			pid++;
		}
		this.mPenX = inX;
		this.mPenY = inY;
		this.jeashExpandStandardExtent(inX,inY);
		this.mPoints.push(new jeash.display.GfxPoint(this.mPenX,this.mPenY,0.0,0.0,1));
		if(this.mCurrentLine.grad != null || this.mCurrentLine.alpha > 0) {
			if(this.mCurrentLine.point_idx0 < 0) this.mCurrentLine.point_idx0 = pid - 1;
			this.mCurrentLine.point_idx1 = pid;
		}
		if(!this.mFilling) this.ClosePolygon(false);
	}
	,curveTo: function(inCX,inCY,inX,inY) {
		var pid = this.mPoints.length;
		if(pid == 0) {
			this.mPoints.push(new jeash.display.GfxPoint(this.mPenX,this.mPenY,0.0,0.0,0));
			pid++;
		}
		this.mPenX = inX;
		this.mPenY = inY;
		this.jeashExpandStandardExtent(inX,inY);
		this.mPoints.push(new jeash.display.GfxPoint(inX,inY,inCX,inCY,2));
		if(this.mCurrentLine.grad != null || this.mCurrentLine.alpha > 0) {
			if(this.mCurrentLine.point_idx0 < 0) this.mCurrentLine.point_idx0 = pid - 1;
			this.mCurrentLine.point_idx1 = pid;
		}
	}
	,AddDrawable: function(inDrawable) {
		if(inDrawable == null) return;
		this.mDrawList.unshift(inDrawable);
	}
	,AddLineSegment: function() {
		if(this.mCurrentLine.point_idx1 > 0) this.mLineJobs.push(new jeash.display.LineJob(this.mCurrentLine.grad,this.mCurrentLine.point_idx0,this.mCurrentLine.point_idx1,this.mCurrentLine.thickness,this.mCurrentLine.alpha,this.mCurrentLine.colour,this.mCurrentLine.pixel_hinting,this.mCurrentLine.joints,this.mCurrentLine.caps,this.mCurrentLine.scale_mode,this.mCurrentLine.miter_limit));
		this.mCurrentLine.point_idx0 = this.mCurrentLine.point_idx1 = -1;
	}
	,ClosePolygon: function(inCancelFill) {
		var l = this.mPoints.length;
		if(l > 0) {
			if(l > 1) {
				if(this.mFilling && l > 2) {
					if(this.mPoints[this.mLastMoveID].x != this.mPoints[l - 1].x || this.mPoints[this.mLastMoveID].y != this.mPoints[l - 1].y) this.lineTo(this.mPoints[this.mLastMoveID].x,this.mPoints[this.mLastMoveID].y);
				}
				this.AddLineSegment();
				var drawable = { points : this.mPoints, fillColour : this.mFillColour, fillAlpha : this.mFillAlpha, solidGradient : this.mSolidGradient, bitmap : this.mBitmap, lineJobs : this.mLineJobs};
				this.AddDrawable(drawable);
			}
			this.mLineJobs = [];
			this.mPoints = [];
		}
		if(inCancelFill) {
			this.mFillAlpha = 0;
			this.mSolidGradient = null;
			this.mBitmap = null;
			this.mFilling = false;
		}
		this.jeashChanged = true;
	}
	,getContext: function() {
		try {
			return this.jeashSurface.getContext("2d");
		} catch( e ) {
			return null;
		}
	}
	,jeashAdjustSurface: function() {
		if(Reflect.field(this.jeashSurface,"getContext") == null) return;
		var width = Math.ceil(this.jeashExtent.width - this.jeashExtent.x);
		var height = Math.ceil(this.jeashExtent.height - this.jeashExtent.y);
		if(width > 5000 || height > 5000) return;
		var dstCanvas = js.Lib.document.createElement("canvas");
		var ctx = dstCanvas.getContext("2d");
		dstCanvas.width = width;
		dstCanvas.height = height;
		if(this.jeashSurface.id != null) jeash.Lib.jeashSetSurfaceId(dstCanvas,this.jeashSurface.id);
		jeash.Lib.jeashDrawToSurface(this.jeashSurface,dstCanvas);
		if(jeash.Lib.jeashIsOnStage(this.jeashSurface)) {
			jeash.Lib.jeashAppendSurface(dstCanvas);
			jeash.Lib.jeashCopyStyle(this.jeashSurface,dstCanvas);
			jeash.Lib.jeashSwapSurface(this.jeashSurface,dstCanvas);
			jeash.Lib.jeashRemoveSurface(this.jeashSurface);
		}
		this.jeashSurface = dstCanvas;
	}
	,__class__: jeash.display.Graphics
}
jeash.display.IGraphicsData = function() { }
$hxClasses["jeash.display.IGraphicsData"] = jeash.display.IGraphicsData;
jeash.display.IGraphicsData.__name__ = ["jeash","display","IGraphicsData"];
jeash.display.IGraphicsData.prototype = {
	jeashGraphicsDataType: null
	,__class__: jeash.display.IGraphicsData
}
jeash.display.GraphicsDataType = $hxClasses["jeash.display.GraphicsDataType"] = { __ename__ : ["jeash","display","GraphicsDataType"], __constructs__ : ["STROKE","SOLID","GRADIENT","PATH"] }
jeash.display.GraphicsDataType.STROKE = ["STROKE",0];
jeash.display.GraphicsDataType.STROKE.toString = $estr;
jeash.display.GraphicsDataType.STROKE.__enum__ = jeash.display.GraphicsDataType;
jeash.display.GraphicsDataType.SOLID = ["SOLID",1];
jeash.display.GraphicsDataType.SOLID.toString = $estr;
jeash.display.GraphicsDataType.SOLID.__enum__ = jeash.display.GraphicsDataType;
jeash.display.GraphicsDataType.GRADIENT = ["GRADIENT",2];
jeash.display.GraphicsDataType.GRADIENT.toString = $estr;
jeash.display.GraphicsDataType.GRADIENT.__enum__ = jeash.display.GraphicsDataType;
jeash.display.GraphicsDataType.PATH = ["PATH",3];
jeash.display.GraphicsDataType.PATH.toString = $estr;
jeash.display.GraphicsDataType.PATH.__enum__ = jeash.display.GraphicsDataType;
jeash.display.IGraphicsFill = function() { }
$hxClasses["jeash.display.IGraphicsFill"] = jeash.display.IGraphicsFill;
jeash.display.IGraphicsFill.__name__ = ["jeash","display","IGraphicsFill"];
jeash.display.IGraphicsFill.prototype = {
	jeashGraphicsFillType: null
	,__class__: jeash.display.IGraphicsFill
}
jeash.display.GraphicsFillType = $hxClasses["jeash.display.GraphicsFillType"] = { __ename__ : ["jeash","display","GraphicsFillType"], __constructs__ : ["SOLID_FILL","GRADIENT_FILL"] }
jeash.display.GraphicsFillType.SOLID_FILL = ["SOLID_FILL",0];
jeash.display.GraphicsFillType.SOLID_FILL.toString = $estr;
jeash.display.GraphicsFillType.SOLID_FILL.__enum__ = jeash.display.GraphicsFillType;
jeash.display.GraphicsFillType.GRADIENT_FILL = ["GRADIENT_FILL",1];
jeash.display.GraphicsFillType.GRADIENT_FILL.toString = $estr;
jeash.display.GraphicsFillType.GRADIENT_FILL.__enum__ = jeash.display.GraphicsFillType;
jeash.display.InterpolationMethod = $hxClasses["jeash.display.InterpolationMethod"] = { __ename__ : ["jeash","display","InterpolationMethod"], __constructs__ : ["RGB","LINEAR_RGB"] }
jeash.display.InterpolationMethod.RGB = ["RGB",0];
jeash.display.InterpolationMethod.RGB.toString = $estr;
jeash.display.InterpolationMethod.RGB.__enum__ = jeash.display.InterpolationMethod;
jeash.display.InterpolationMethod.LINEAR_RGB = ["LINEAR_RGB",1];
jeash.display.InterpolationMethod.LINEAR_RGB.toString = $estr;
jeash.display.InterpolationMethod.LINEAR_RGB.__enum__ = jeash.display.InterpolationMethod;
jeash.display.JointStyle = $hxClasses["jeash.display.JointStyle"] = { __ename__ : ["jeash","display","JointStyle"], __constructs__ : ["MITER","ROUND","BEVEL"] }
jeash.display.JointStyle.MITER = ["MITER",0];
jeash.display.JointStyle.MITER.toString = $estr;
jeash.display.JointStyle.MITER.__enum__ = jeash.display.JointStyle;
jeash.display.JointStyle.ROUND = ["ROUND",1];
jeash.display.JointStyle.ROUND.toString = $estr;
jeash.display.JointStyle.ROUND.__enum__ = jeash.display.JointStyle;
jeash.display.JointStyle.BEVEL = ["BEVEL",2];
jeash.display.JointStyle.BEVEL.toString = $estr;
jeash.display.JointStyle.BEVEL.__enum__ = jeash.display.JointStyle;
jeash.display.LineScaleMode = $hxClasses["jeash.display.LineScaleMode"] = { __ename__ : ["jeash","display","LineScaleMode"], __constructs__ : ["HORIZONTAL","NONE","NORMAL","VERTICAL"] }
jeash.display.LineScaleMode.HORIZONTAL = ["HORIZONTAL",0];
jeash.display.LineScaleMode.HORIZONTAL.toString = $estr;
jeash.display.LineScaleMode.HORIZONTAL.__enum__ = jeash.display.LineScaleMode;
jeash.display.LineScaleMode.NONE = ["NONE",1];
jeash.display.LineScaleMode.NONE.toString = $estr;
jeash.display.LineScaleMode.NONE.__enum__ = jeash.display.LineScaleMode;
jeash.display.LineScaleMode.NORMAL = ["NORMAL",2];
jeash.display.LineScaleMode.NORMAL.toString = $estr;
jeash.display.LineScaleMode.NORMAL.__enum__ = jeash.display.LineScaleMode;
jeash.display.LineScaleMode.VERTICAL = ["VERTICAL",3];
jeash.display.LineScaleMode.VERTICAL.toString = $estr;
jeash.display.LineScaleMode.VERTICAL.__enum__ = jeash.display.LineScaleMode;
jeash.display.Loader = function() {
	jeash.display.DisplayObjectContainer.call(this);
	this.contentLoaderInfo = jeash.display.LoaderInfo.create(this);
	this.name = "Loader " + jeash.display.DisplayObject.mNameID++;
};
$hxClasses["jeash.display.Loader"] = jeash.display.Loader;
jeash.display.Loader.__name__ = ["jeash","display","Loader"];
jeash.display.Loader.__super__ = jeash.display.DisplayObjectContainer;
jeash.display.Loader.prototype = $extend(jeash.display.DisplayObjectContainer.prototype,{
	content: null
	,contentLoaderInfo: null
	,mImage: null
	,mShape: null
	,load: function(request,context) {
		var parts = request.url.split(".");
		var extension = parts.length == 0?"":parts[parts.length - 1].toLowerCase();
		var transparent = true;
		this.contentLoaderInfo.url = request.url;
		this.contentLoaderInfo.contentType = (function($this) {
			var $r;
			switch(extension) {
			case "swf":
				$r = "application/x-shockwave-flash";
				break;
			case "jpg":case "jpeg":
				$r = (function($this) {
					var $r;
					transparent = false;
					$r = "image/jpeg";
					return $r;
				}($this));
				break;
			case "png":
				$r = "image/png";
				break;
			case "gif":
				$r = "image/gif";
				break;
			default:
				$r = (function($this) {
					var $r;
					throw "Unrecognized file " + request.url;
					return $r;
				}($this));
			}
			return $r;
		}(this));
		this.mImage = new jeash.display.BitmapData(0,0,transparent);
		try {
			this.contentLoaderInfo.addEventListener(jeash.events.Event.COMPLETE,this.handleLoad.$bind(this),false,2147483647);
			this.mImage.jeashLoadFromFile(request.url,this.contentLoaderInfo);
			this.content = new jeash.display.Bitmap(this.mImage);
			this.contentLoaderInfo["content"] = this.content;
			this.addChild(this.content);
		} catch( e ) {
			haxe.Log.trace("Error " + e,{ fileName : "Loader.hx", lineNumber : 90, className : "jeash.display.Loader", methodName : "load"});
			var evt = new jeash.events.IOErrorEvent(jeash.events.IOErrorEvent.IO_ERROR);
			this.contentLoaderInfo.dispatchEvent(evt);
			return;
		}
		if(this.mShape == null) {
			this.mShape = new jeash.display.Shape();
			this.addChild(this.mShape);
		}
	}
	,handleLoad: function(e) {
		this.content.jeashInvalidateBounds();
		this.content.jeashRender(null,null);
		this.contentLoaderInfo.removeEventListener(jeash.events.Event.COMPLETE,this.handleLoad.$bind(this));
	}
	,BuildBounds: function() {
		jeash.display.DisplayObjectContainer.prototype.BuildBounds.call(this);
		if(this.mImage != null) {
			var r = new jeash.geom.Rectangle(0,0,this.mImage.getWidth(),this.mImage.getHeight());
			if(r.width != 0 || r.height != 0) {
				if(this.mBoundsRect.width == 0 && this.mBoundsRect.height == 0) this.mBoundsRect = r.clone(); else this.mBoundsRect.extendBounds(r);
			}
		}
	}
	,jeashIsOnStage: function() {
		if(this.parent != null && this.parent.jeashIsOnStage() == true) return true; else return false;
	}
	,__class__: jeash.display.Loader
});
jeash.display.LoaderInfo = function() {
	jeash.events.EventDispatcher.call(this);
	this.bytesLoaded = 0;
	this.bytesTotal = 0;
	this.childAllowsParent = true;
	this.parameters = { };
};
$hxClasses["jeash.display.LoaderInfo"] = jeash.display.LoaderInfo;
jeash.display.LoaderInfo.__name__ = ["jeash","display","LoaderInfo"];
jeash.display.LoaderInfo.create = function(ldr) {
	var li = new jeash.display.LoaderInfo();
	li.loader = ldr;
	return li;
}
jeash.display.LoaderInfo.__super__ = jeash.events.EventDispatcher;
jeash.display.LoaderInfo.prototype = $extend(jeash.events.EventDispatcher.prototype,{
	bytesLoaded: null
	,bytesTotal: null
	,childAllowsParent: null
	,content: null
	,contentType: null
	,loader: null
	,parameters: null
	,url: null
	,__class__: jeash.display.LoaderInfo
});
jeash.display.MovieClip = function() {
	jeash.display.Sprite.call(this);
	this.enabled = true;
	this.mCurrentFrame = 0;
	this.mTotalFrames = 0;
	this.name = "MovieClip " + jeash.display.DisplayObject.mNameID++;
};
$hxClasses["jeash.display.MovieClip"] = jeash.display.MovieClip;
jeash.display.MovieClip.__name__ = ["jeash","display","MovieClip"];
jeash.display.MovieClip.__super__ = jeash.display.Sprite;
jeash.display.MovieClip.prototype = $extend(jeash.display.Sprite.prototype,{
	enabled: null
	,mCurrentFrame: null
	,mTotalFrames: null
	,GetTotalFrames: function() {
		return this.mTotalFrames;
	}
	,GetCurrentFrame: function() {
		return this.mCurrentFrame;
	}
	,__class__: jeash.display.MovieClip
});
jeash.display.PixelSnapping = $hxClasses["jeash.display.PixelSnapping"] = { __ename__ : ["jeash","display","PixelSnapping"], __constructs__ : ["NEVER","AUTO","ALWAYS"] }
jeash.display.PixelSnapping.NEVER = ["NEVER",0];
jeash.display.PixelSnapping.NEVER.toString = $estr;
jeash.display.PixelSnapping.NEVER.__enum__ = jeash.display.PixelSnapping;
jeash.display.PixelSnapping.AUTO = ["AUTO",1];
jeash.display.PixelSnapping.AUTO.toString = $estr;
jeash.display.PixelSnapping.AUTO.__enum__ = jeash.display.PixelSnapping;
jeash.display.PixelSnapping.ALWAYS = ["ALWAYS",2];
jeash.display.PixelSnapping.ALWAYS.toString = $estr;
jeash.display.PixelSnapping.ALWAYS.__enum__ = jeash.display.PixelSnapping;
jeash.display.Shape = function() {
	this.jeashGraphics = new jeash.display.Graphics();
	jeash.display.DisplayObject.call(this);
	this.name = "Shape " + jeash.display.DisplayObject.mNameID++;
	jeash.Lib.jeashSetSurfaceId(this.jeashGraphics.jeashSurface,this.name);
};
$hxClasses["jeash.display.Shape"] = jeash.display.Shape;
jeash.display.Shape.__name__ = ["jeash","display","Shape"];
jeash.display.Shape.__super__ = jeash.display.DisplayObject;
jeash.display.Shape.prototype = $extend(jeash.display.DisplayObject.prototype,{
	jeashGraphics: null
	,jeashGetGraphics: function() {
		return this.jeashGraphics;
	}
	,jeashGetObjectUnderPoint: function(point) {
		if(this.parent == null) return null;
		if(this.parent.mouseEnabled && jeash.display.DisplayObject.prototype.jeashGetObjectUnderPoint.call(this,point) == this) return this.parent; else return null;
	}
	,__class__: jeash.display.Shape
});
jeash.display.SpreadMethod = $hxClasses["jeash.display.SpreadMethod"] = { __ename__ : ["jeash","display","SpreadMethod"], __constructs__ : ["REPEAT","REFLECT","PAD"] }
jeash.display.SpreadMethod.REPEAT = ["REPEAT",0];
jeash.display.SpreadMethod.REPEAT.toString = $estr;
jeash.display.SpreadMethod.REPEAT.__enum__ = jeash.display.SpreadMethod;
jeash.display.SpreadMethod.REFLECT = ["REFLECT",1];
jeash.display.SpreadMethod.REFLECT.toString = $estr;
jeash.display.SpreadMethod.REFLECT.__enum__ = jeash.display.SpreadMethod;
jeash.display.SpreadMethod.PAD = ["PAD",2];
jeash.display.SpreadMethod.PAD.toString = $estr;
jeash.display.SpreadMethod.PAD.__enum__ = jeash.display.SpreadMethod;
jeash.events.Event = function(inType,inBubbles,inCancelable) {
	if(inCancelable == null) inCancelable = false;
	if(inBubbles == null) inBubbles = false;
	this.type = inType;
	this.bubbles = inBubbles;
	this.cancelable = inCancelable;
	this.jeashIsCancelled = false;
	this.jeashIsCancelledNow = false;
	this.target = null;
	this.currentTarget = null;
	this.eventPhase = jeash.events.EventPhase.AT_TARGET;
};
$hxClasses["jeash.events.Event"] = jeash.events.Event;
jeash.events.Event.__name__ = ["jeash","events","Event"];
jeash.events.Event.prototype = {
	bubbles: null
	,cancelable: null
	,eventPhase: null
	,target: null
	,currentTarget: null
	,type: null
	,jeashIsCancelled: null
	,jeashIsCancelledNow: null
	,jeashSetPhase: function(phase) {
		this.eventPhase = phase;
	}
	,jeashGetIsCancelled: function() {
		return this.jeashIsCancelled;
	}
	,jeashGetIsCancelledNow: function() {
		return this.jeashIsCancelledNow;
	}
	,jeashCreateSimilar: function(type,related,targ) {
		var result = new jeash.events.Event(type,this.bubbles,this.cancelable);
		if(targ != null) result.target = targ;
		return result;
	}
	,__class__: jeash.events.Event
}
jeash.events.MouseEvent = function(type,bubbles,cancelable,localX,localY,relatedObject,ctrlKey,altKey,shiftKey,buttonDown,delta,commandKey,clickCount) {
	if(clickCount == null) clickCount = 0;
	if(commandKey == null) commandKey = false;
	if(delta == null) delta = 0;
	if(buttonDown == null) buttonDown = false;
	if(shiftKey == null) shiftKey = false;
	if(altKey == null) altKey = false;
	if(ctrlKey == null) ctrlKey = false;
	if(localY == null) localY = 0;
	if(localX == null) localX = 0;
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = true;
	jeash.events.Event.call(this,type,bubbles,cancelable);
	this.shiftKey = shiftKey;
	this.altKey = altKey;
	this.ctrlKey = ctrlKey;
	this.bubbles = bubbles;
	this.relatedObject = relatedObject;
	this.delta = delta;
	this.localX = localX;
	this.localY = localY;
	this.buttonDown = buttonDown;
	this.commandKey = commandKey;
	this.clickCount = clickCount;
};
$hxClasses["jeash.events.MouseEvent"] = jeash.events.MouseEvent;
jeash.events.MouseEvent.__name__ = ["jeash","events","MouseEvent"];
jeash.events.MouseEvent.jeashCreate = function(type,event,local,target) {
	var jeashMouseDown = false;
	var delta = type == jeash.events.MouseEvent.MOUSE_WHEEL?(function($this) {
		var $r;
		var mouseEvent = event;
		$r = mouseEvent.wheelDelta?js.Lib.isOpera?mouseEvent.wheelDelta / 40 | 0:mouseEvent.wheelDelta / 120 | 0:mouseEvent.detail?-mouseEvent.detail | 0:null;
		return $r;
	}(this)):2;
	if(type == jeash.events.MouseEvent.MOUSE_DOWN) jeashMouseDown = event.which != null?event.which == 1:event.button != null?js.Lib.isIE && event.button == 1 || event.button == 0:false; else if(type == jeash.events.MouseEvent.MOUSE_UP) {
		if(event.which != null) {
			if(event.which == 1) jeashMouseDown = false; else if(event.button != null) {
				if(js.Lib.isIE && event.button == 1 || event.button == 0) jeashMouseDown = false; else jeashMouseDown = false;
			}
		}
	}
	var pseudoEvent = new jeash.events.MouseEvent(type,true,false,local.x,local.y,null,event.ctrlKey,event.altKey,event.shiftKey,jeashMouseDown,delta);
	pseudoEvent.stageX = jeash.Lib.jeashGetCurrent().GetStage().jeashGetMouseX();
	pseudoEvent.stageY = jeash.Lib.jeashGetCurrent().GetStage().jeashGetMouseY();
	pseudoEvent.target = target;
	return pseudoEvent;
}
jeash.events.MouseEvent.__super__ = jeash.events.Event;
jeash.events.MouseEvent.prototype = $extend(jeash.events.Event.prototype,{
	altKey: null
	,buttonDown: null
	,ctrlKey: null
	,delta: null
	,localX: null
	,localY: null
	,relatedObject: null
	,shiftKey: null
	,stageX: null
	,stageY: null
	,commandKey: null
	,clickCount: null
	,jeashCreateSimilar: function(type,related,targ) {
		var result = new jeash.events.MouseEvent(type,this.bubbles,this.cancelable,this.localX,this.localY,related == null?this.relatedObject:related,this.ctrlKey,this.altKey,this.shiftKey,this.buttonDown,this.delta,this.commandKey,this.clickCount);
		if(targ != null) result.target = targ;
		return result;
	}
	,__class__: jeash.events.MouseEvent
});
jeash.events.TouchEvent = function(type,bubbles,cancelable,localX,localY,relatedObject,ctrlKey,altKey,shiftKey,buttonDown,delta,commandKey,clickCount) {
	if(clickCount == null) clickCount = 0;
	if(commandKey == null) commandKey = false;
	if(delta == null) delta = 0;
	if(buttonDown == null) buttonDown = false;
	if(shiftKey == null) shiftKey = false;
	if(altKey == null) altKey = false;
	if(ctrlKey == null) ctrlKey = false;
	if(localY == null) localY = 0;
	if(localX == null) localX = 0;
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = true;
	jeash.events.Event.call(this,type,bubbles,cancelable);
	this.shiftKey = shiftKey;
	this.altKey = altKey;
	this.ctrlKey = ctrlKey;
	this.bubbles = bubbles;
	this.relatedObject = relatedObject;
	this.delta = delta;
	this.localX = localX;
	this.localY = localY;
	this.buttonDown = buttonDown;
	this.commandKey = commandKey;
	this.touchPointID = 0;
	this.isPrimaryTouchPoint = true;
};
$hxClasses["jeash.events.TouchEvent"] = jeash.events.TouchEvent;
jeash.events.TouchEvent.__name__ = ["jeash","events","TouchEvent"];
jeash.events.TouchEvent.jeashCreate = function(type,event,touch,local,target) {
	var evt = new jeash.events.TouchEvent(type,true,false,local.x,local.y,null,event.ctrlKey,event.altKey,event.shiftKey,false,0,null,0);
	evt.stageX = jeash.Lib.jeashGetCurrent().GetStage().jeashGetMouseX();
	evt.stageY = jeash.Lib.jeashGetCurrent().GetStage().jeashGetMouseY();
	evt.target = target;
	return evt;
}
jeash.events.TouchEvent.__super__ = jeash.events.Event;
jeash.events.TouchEvent.prototype = $extend(jeash.events.Event.prototype,{
	altKey: null
	,buttonDown: null
	,ctrlKey: null
	,delta: null
	,localX: null
	,localY: null
	,relatedObject: null
	,shiftKey: null
	,stageX: null
	,stageY: null
	,commandKey: null
	,isPrimaryTouchPoint: null
	,touchPointID: null
	,jeashCreateSimilar: function(type,related,targ) {
		var result = new jeash.events.TouchEvent(type,this.bubbles,this.cancelable,this.localX,this.localY,related == null?this.relatedObject:related,this.ctrlKey,this.altKey,this.shiftKey,this.buttonDown,this.delta,this.commandKey);
		result.touchPointID = this.touchPointID;
		result.isPrimaryTouchPoint = this.isPrimaryTouchPoint;
		if(targ != null) result.target = targ;
		return result;
	}
	,__class__: jeash.events.TouchEvent
});
jeash.display.Stage = function(width,height) {
	jeash.display.DisplayObjectContainer.call(this);
	this.jeashFocusObject = null;
	this.jeashWindowWidth = width;
	this.jeashWindowHeight = height;
	this.stageFocusRect = false;
	this.scaleMode = jeash.display.StageScaleMode.SHOW_ALL;
	this.jeashStageMatrix = new jeash.geom.Matrix();
	this.tabEnabled = true;
	this.jeashSetFrameRate(60.0);
	this.jeashSetBackgroundColour(16777215);
	this.name = "Stage";
	this.loaderInfo = jeash.display.LoaderInfo.create(null);
	this.loaderInfo.parameters.width = Std.string(this.jeashWindowWidth);
	this.loaderInfo.parameters.height = Std.string(this.jeashWindowHeight);
	this.jeashPointInPathMode = jeash.display.Graphics.jeashDetectIsPointInPathMode();
	this.jeashMouseOverObjects = [];
	this.jeashSetShowDefaultContextMenu(true);
	this.jeashTouchInfo = [];
	this.jeashFocusOverObjects = [];
	this.jeashUIEventsQueue = new Array(1000);
	this.jeashUIEventsQueueIndex = 0;
};
$hxClasses["jeash.display.Stage"] = jeash.display.Stage;
jeash.display.Stage.__name__ = ["jeash","display","Stage"];
jeash.display.Stage.__super__ = jeash.display.DisplayObjectContainer;
jeash.display.Stage.prototype = $extend(jeash.display.DisplayObjectContainer.prototype,{
	jeashWindowWidth: null
	,jeashWindowHeight: null
	,jeashTimer: null
	,jeashInterval: null
	,jeashDragObject: null
	,jeashDragBounds: null
	,jeashDragOffsetX: null
	,jeashDragOffsetY: null
	,jeashMouseOverObjects: null
	,jeashStageMatrix: null
	,jeashStageActive: null
	,jeashFrameRate: null
	,jeashBackgroundColour: null
	,jeashShowDefaultContextMenu: null
	,jeashTouchInfo: null
	,jeashFocusOverObjects: null
	,jeashUIEventsQueue: null
	,jeashUIEventsQueueIndex: null
	,jeashPointInPathMode: null
	,stageWidth: null
	,stageHeight: null
	,frameRate: null
	,quality: null
	,scaleMode: null
	,stageFocusRect: null
	,focus: null
	,backgroundColor: null
	,showDefaultContextMenu: null
	,displayState: null
	,jeashGetStageWidth: function() {
		return this.jeashWindowWidth;
	}
	,jeashGetStageHeight: function() {
		return this.jeashWindowHeight;
	}
	,jeashFocusObject: null
	,jeashDrag: function(point) {
		var p = this.jeashDragObject.parent;
		if(p != null) point = p.globalToLocal(point);
		var x = point.x + this.jeashDragOffsetX;
		var y = point.y + this.jeashDragOffsetY;
		if(this.jeashDragBounds != null) {
			if(x < this.jeashDragBounds.x) x = this.jeashDragBounds.x; else if(x > this.jeashDragBounds.get_right()) x = this.jeashDragBounds.get_right();
			if(y < this.jeashDragBounds.y) y = this.jeashDragBounds.y; else if(y > this.jeashDragBounds.get_bottom()) y = this.jeashDragBounds.get_bottom();
		}
		this.jeashDragObject.jeashSetX(x);
		this.jeashDragObject.jeashSetY(y);
	}
	,jeashCheckFocusInOuts: function(event,inStack) {
		var new_n = inStack.length;
		var new_obj = new_n > 0?inStack[new_n - 1]:null;
		var old_n = this.jeashFocusOverObjects.length;
		var old_obj = old_n > 0?this.jeashFocusOverObjects[old_n - 1]:null;
		if(new_obj != old_obj) {
			var common = 0;
			while(common < new_n && common < old_n && inStack[common] == this.jeashFocusOverObjects[common]) common++;
			var focusOut = new jeash.events.FocusEvent(jeash.events.FocusEvent.FOCUS_OUT,false,false,new_obj,false,0);
			var i = old_n - 1;
			while(i >= common) {
				this.jeashFocusOverObjects[i].dispatchEvent(focusOut);
				i--;
			}
			var focusIn = new jeash.events.FocusEvent(jeash.events.FocusEvent.FOCUS_IN,false,false,old_obj,false,0);
			var i1 = new_n - 1;
			while(i1 >= common) {
				inStack[i1].dispatchEvent(focusIn);
				i1--;
			}
			this.jeashFocusOverObjects = inStack;
			this.jeashSetFocus(new_obj);
		}
	}
	,jeashCheckInOuts: function(event,stack,touchInfo) {
		var prev = touchInfo == null?this.jeashMouseOverObjects:touchInfo.touchOverObjects;
		var events = touchInfo == null?jeash.display.Stage.jeashMouseChanges:jeash.display.Stage.jeashTouchChanges;
		var new_n = stack.length;
		var new_obj = new_n > 0?stack[new_n - 1]:null;
		var old_n = prev.length;
		var old_obj = old_n > 0?prev[old_n - 1]:null;
		if(new_obj != old_obj) {
			if(old_obj != null) old_obj.jeashFireEvent(event.jeashCreateSimilar(events[0],new_obj,old_obj));
			if(new_obj != null) new_obj.jeashFireEvent(event.jeashCreateSimilar(events[1],old_obj,new_obj));
			var common = 0;
			while(common < new_n && common < old_n && stack[common] == prev[common]) common++;
			var rollOut = event.jeashCreateSimilar(events[2],new_obj,old_obj);
			var i = old_n - 1;
			while(i >= common) {
				prev[i].dispatchEvent(rollOut);
				i--;
			}
			var rollOver = event.jeashCreateSimilar(events[3],old_obj);
			var i1 = new_n - 1;
			while(i1 >= common) {
				stack[i1].dispatchEvent(rollOver);
				i1--;
			}
			if(touchInfo == null) this.jeashMouseOverObjects = stack; else touchInfo.touchOverObjects = stack;
		}
	}
	,jeashQueueStageEvent: function(evt) {
		this.jeashUIEventsQueue[this.jeashUIEventsQueueIndex++] = evt;
	}
	,jeashProcessStageEvent: function(evt) {
		evt.stopPropagation();
		switch(evt.type) {
		case "resize":
			this.jeashOnResize(this.jeashGetStageWidth(),this.jeashGetStageHeight());
			break;
		case "mousemove":
			this.jeashOnMouse(evt,jeash.events.MouseEvent.MOUSE_MOVE);
			break;
		case "mousedown":
			this.jeashOnMouse(evt,jeash.events.MouseEvent.MOUSE_DOWN);
			break;
		case "mouseup":
			this.jeashOnMouse(evt,jeash.events.MouseEvent.MOUSE_UP);
			break;
		case "click":
			this.jeashOnMouse(evt,jeash.events.MouseEvent.CLICK);
			break;
		case "mousewheel":
			this.jeashOnMouse(evt,jeash.events.MouseEvent.MOUSE_WHEEL);
			break;
		case "dblclick":
			this.jeashOnMouse(evt,jeash.events.MouseEvent.DOUBLE_CLICK);
			break;
		case "keydown":
			var evt1 = evt;
			var keyCode = evt1.keyIdentifier != null?(function($this) {
				var $r;
				try {
					$r = jeash.ui.Keyboard.jeashConvertWebkitCode(evt1.keyIdentifier);
				} catch( e ) {
					$r = (function($this) {
						var $r;
						jeash.Lib.trace("keydown error: " + e);
						$r = evt1.keyCode;
						return $r;
					}($this));
				}
				return $r;
			}(this)):jeash.ui.Keyboard.jeashConvertMozillaCode(evt1.keyCode);
			this.jeashOnKey(keyCode,true,evt1.keyLocation,evt1.ctrlKey,evt1.altKey,evt1.shiftKey);
			break;
		case "keyup":
			var evt1 = evt;
			var keyCode = evt1.keyIdentifier != null?(function($this) {
				var $r;
				try {
					$r = jeash.ui.Keyboard.jeashConvertWebkitCode(evt1.keyIdentifier);
				} catch( e ) {
					$r = (function($this) {
						var $r;
						jeash.Lib.trace("keyup error: " + e);
						$r = evt1.keyCode;
						return $r;
					}($this));
				}
				return $r;
			}(this)):jeash.ui.Keyboard.jeashConvertMozillaCode(evt1.keyCode);
			this.jeashOnKey(keyCode,false,evt1.keyLocation,evt1.ctrlKey,evt1.altKey,evt1.shiftKey);
			break;
		case "touchstart":
			var evt1 = evt;
			evt1.preventDefault();
			var touchInfo = new jeash.display.TouchInfo();
			this.jeashTouchInfo[evt1.changedTouches[0].identifier] = touchInfo;
			this.jeashOnTouch(evt1,evt1.changedTouches[0],jeash.events.TouchEvent.TOUCH_BEGIN,touchInfo,false);
			break;
		case "touchmove":
			var evt1 = evt;
			var touchInfo = this.jeashTouchInfo[evt1.changedTouches[0].identifier];
			this.jeashOnTouch(evt1,evt1.changedTouches[0],jeash.events.TouchEvent.TOUCH_MOVE,touchInfo,true);
			break;
		case "touchend":
			var evt1 = evt;
			var touchInfo = this.jeashTouchInfo[evt1.changedTouches[0].identifier];
			this.jeashOnTouch(evt1,evt1.changedTouches[0],jeash.events.TouchEvent.TOUCH_END,touchInfo,true);
			this.jeashTouchInfo[evt1.changedTouches[0].identifier] = null;
			break;
		default:
		}
	}
	,jeashOnMouse: function(event,type) {
		var point = new jeash.geom.Point(event.clientX - jeash.Lib.mMe.__scr.offsetLeft + window.pageXOffset,event.clientY - jeash.Lib.mMe.__scr.offsetTop + window.pageYOffset);
		if(this.jeashDragObject != null) this.jeashDrag(point);
		var obj = this.jeashGetObjectUnderPoint(point);
		this.jeashSetMouseX(point.x);
		this.jeashSetMouseY(point.y);
		var stack = new Array();
		if(obj != null) obj.jeashGetInteractiveObjectStack(stack);
		if(stack.length > 0) {
			stack.reverse();
			var local = obj.globalToLocal(point);
			var evt = jeash.events.MouseEvent.jeashCreate(type,event,local,obj);
			this.jeashCheckInOuts(evt,stack);
			if(type == jeash.events.MouseEvent.MOUSE_DOWN) this.jeashCheckFocusInOuts(evt,stack);
			obj.jeashFireEvent(evt);
		} else {
			var evt = jeash.events.MouseEvent.jeashCreate(type,event,point,null);
			this.jeashCheckInOuts(evt,stack);
			if(type == jeash.events.MouseEvent.MOUSE_DOWN) this.jeashCheckFocusInOuts(evt,stack);
		}
	}
	,jeashOnTouch: function(event,touch,type,touchInfo,isPrimaryTouchPoint) {
		var point = new jeash.geom.Point(touch.pageX - jeash.Lib.mMe.__scr.offsetLeft + window.pageXOffset,touch.pageY - jeash.Lib.mMe.__scr.offsetTop + window.pageYOffset);
		var obj = this.jeashGetObjectUnderPoint(point);
		this.jeashSetMouseX(point.x);
		this.jeashSetMouseY(point.y);
		var stack = new Array();
		if(obj != null) obj.jeashGetInteractiveObjectStack(stack);
		if(stack.length > 0) {
			stack.reverse();
			var local = obj.globalToLocal(point);
			var evt = jeash.events.TouchEvent.jeashCreate(type,event,touch,local,obj);
			evt.touchPointID = touch.identifier;
			evt.isPrimaryTouchPoint = isPrimaryTouchPoint;
			this.jeashCheckInOuts(evt,stack,touchInfo);
			obj.jeashFireEvent(evt);
			var mouseType = (function($this) {
				var $r;
				switch(type) {
				case jeash.events.TouchEvent.TOUCH_BEGIN:
					$r = jeash.events.MouseEvent.MOUSE_DOWN;
					break;
				case jeash.events.TouchEvent.TOUCH_END:
					$r = jeash.events.MouseEvent.MOUSE_UP;
					break;
				default:
					$r = (function($this) {
						var $r;
						if($this.jeashDragObject != null) $this.jeashDrag(point);
						$r = jeash.events.MouseEvent.MOUSE_MOVE;
						return $r;
					}($this));
				}
				return $r;
			}(this));
			obj.jeashFireEvent(jeash.events.MouseEvent.jeashCreate(mouseType,evt,local,obj));
		} else {
			var evt = jeash.events.TouchEvent.jeashCreate(type,event,touch,point,null);
			evt.touchPointID = touch.identifier;
			evt.isPrimaryTouchPoint = isPrimaryTouchPoint;
			this.jeashCheckInOuts(evt,stack,touchInfo);
		}
	}
	,jeashOnKey: function(code,pressed,inChar,ctrl,alt,shift) {
		var event = new jeash.events.KeyboardEvent(pressed?jeash.events.KeyboardEvent.KEY_DOWN:jeash.events.KeyboardEvent.KEY_UP,true,false,inChar,code,shift || ctrl?1:0,ctrl,alt,shift);
		this.dispatchEvent(event);
	}
	,jeashOnResize: function(inW,inH) {
		this.jeashWindowWidth = inW;
		this.jeashWindowHeight = inH;
		var event = new jeash.events.Event(jeash.events.Event.RESIZE);
		event.target = this;
		this.jeashBroadcast(event);
	}
	,jeashGetBackgroundColour: function() {
		return this.jeashBackgroundColour;
	}
	,jeashSetBackgroundColour: function(col) {
		this.jeashBackgroundColour = col;
		return col;
	}
	,jeashSetFocus: function(inObj) {
		return this.jeashFocusObject = inObj;
	}
	,jeashGetFocus: function() {
		return this.jeashFocusObject;
	}
	,jeashRenderAll: function() {
		this.jeashRender(this.jeashStageMatrix,null);
	}
	,jeashSetQuality: function(inQuality) {
		this.quality = inQuality;
		return inQuality;
	}
	,jeashGetQuality: function() {
		return this.quality != null?this.quality:jeash.display.StageQuality.BEST;
	}
	,jeashGetFrameRate: function() {
		return this.jeashFrameRate;
	}
	,jeashSetFrameRate: function(speed) {
		var window = js.Lib.window;
		this.jeashInterval = 1000.0 / speed | 0;
		this.jeashUpdateNextWake();
		this.jeashFrameRate = speed;
		return speed;
	}
	,jeashUpdateNextWake: function() {
		var window = js.Lib.window;
		window.clearInterval(this.jeashTimer);
		this.jeashTimer = window.setInterval(this.jeashStageRender.$bind(this),this.jeashInterval,[]);
	}
	,jeashStageRender: function(_) {
		if(!this.jeashStageActive) {
			this.jeashOnResize(this.jeashWindowWidth,this.jeashWindowHeight);
			var event = new jeash.events.Event(jeash.events.Event.ACTIVATE);
			event.target = this;
			this.jeashBroadcast(event);
			this.jeashStageActive = true;
		}
		var _g1 = 0, _g = this.jeashUIEventsQueueIndex;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.jeashUIEventsQueue[i] != null) this.jeashProcessStageEvent(this.jeashUIEventsQueue[i]);
		}
		this.jeashUIEventsQueueIndex = 0;
		var event = new jeash.events.Event(jeash.events.Event.ENTER_FRAME);
		this.jeashBroadcast(event);
		this.jeashRenderAll();
		var event1 = new jeash.events.Event(jeash.events.Event.RENDER);
		this.jeashBroadcast(event1);
	}
	,jeashIsOnStage: function() {
		return true;
	}
	,jeashGetMouseX: function() {
		return this.mouseX;
	}
	,jeashSetMouseX: function(x) {
		this.mouseX = x;
		return x;
	}
	,jeashGetMouseY: function() {
		return this.mouseY;
	}
	,jeashSetMouseY: function(y) {
		this.mouseY = y;
		return y;
	}
	,jeashGetShowDefaultContextMenu: function() {
		return this.jeashShowDefaultContextMenu;
	}
	,jeashSetShowDefaultContextMenu: function(showDefaultContextMenu) {
		if(showDefaultContextMenu != this.jeashShowDefaultContextMenu && this.jeashShowDefaultContextMenu != null) {
			if(!showDefaultContextMenu) jeash.Lib.jeashDisableRightClick(); else jeash.Lib.jeashEnableRightClick();
		}
		this.jeashShowDefaultContextMenu = showDefaultContextMenu;
		return showDefaultContextMenu;
	}
	,jeashGetDisplayState: function() {
		return this.displayState;
	}
	,jeashSetDisplayState: function(displayState) {
		if(displayState != this.displayState && this.displayState != null) {
			switch( (displayState)[1] ) {
			case 1:
				jeash.Lib.jeashDisableFullScreen();
				break;
			case 0:
				jeash.Lib.jeashEnableFullScreen();
				break;
			}
		}
		this.displayState = displayState;
		return displayState;
	}
	,jeashGetFullScreenWidth: function() {
		return jeash.Lib.jeashFullScreenWidth();
	}
	,jeashGetFullScreenHeight: function() {
		return jeash.Lib.jeashFullScreenHeight();
	}
	,__class__: jeash.display.Stage
	,__properties__: $extend(jeash.display.DisplayObjectContainer.prototype.__properties__,{set_displayState:"jeashSetDisplayState",get_displayState:"jeashGetDisplayState",set_showDefaultContextMenu:"jeashSetShowDefaultContextMenu",get_showDefaultContextMenu:"jeashGetShowDefaultContextMenu",set_backgroundColor:"jeashSetBackgroundColour",get_backgroundColor:"jeashGetBackgroundColour",set_focus:"jeashSetFocus",get_focus:"jeashGetFocus",set_quality:"jeashSetQuality",get_quality:"jeashGetQuality",set_frameRate:"jeashSetFrameRate",get_frameRate:"jeashGetFrameRate",get_stageHeight:"jeashGetStageHeight",get_stageWidth:"jeashGetStageWidth"})
});
jeash.display.TouchInfo = function() {
	this.touchOverObjects = [];
};
$hxClasses["jeash.display.TouchInfo"] = jeash.display.TouchInfo;
jeash.display.TouchInfo.__name__ = ["jeash","display","TouchInfo"];
jeash.display.TouchInfo.prototype = {
	touchOverObjects: null
	,__class__: jeash.display.TouchInfo
}
jeash.display.StageAlign = $hxClasses["jeash.display.StageAlign"] = { __ename__ : ["jeash","display","StageAlign"], __constructs__ : ["TOP_RIGHT","TOP_LEFT","TOP","RIGHT","LEFT","BOTTOM_RIGHT","BOTTOM_LEFT","BOTTOM"] }
jeash.display.StageAlign.TOP_RIGHT = ["TOP_RIGHT",0];
jeash.display.StageAlign.TOP_RIGHT.toString = $estr;
jeash.display.StageAlign.TOP_RIGHT.__enum__ = jeash.display.StageAlign;
jeash.display.StageAlign.TOP_LEFT = ["TOP_LEFT",1];
jeash.display.StageAlign.TOP_LEFT.toString = $estr;
jeash.display.StageAlign.TOP_LEFT.__enum__ = jeash.display.StageAlign;
jeash.display.StageAlign.TOP = ["TOP",2];
jeash.display.StageAlign.TOP.toString = $estr;
jeash.display.StageAlign.TOP.__enum__ = jeash.display.StageAlign;
jeash.display.StageAlign.RIGHT = ["RIGHT",3];
jeash.display.StageAlign.RIGHT.toString = $estr;
jeash.display.StageAlign.RIGHT.__enum__ = jeash.display.StageAlign;
jeash.display.StageAlign.LEFT = ["LEFT",4];
jeash.display.StageAlign.LEFT.toString = $estr;
jeash.display.StageAlign.LEFT.__enum__ = jeash.display.StageAlign;
jeash.display.StageAlign.BOTTOM_RIGHT = ["BOTTOM_RIGHT",5];
jeash.display.StageAlign.BOTTOM_RIGHT.toString = $estr;
jeash.display.StageAlign.BOTTOM_RIGHT.__enum__ = jeash.display.StageAlign;
jeash.display.StageAlign.BOTTOM_LEFT = ["BOTTOM_LEFT",6];
jeash.display.StageAlign.BOTTOM_LEFT.toString = $estr;
jeash.display.StageAlign.BOTTOM_LEFT.__enum__ = jeash.display.StageAlign;
jeash.display.StageAlign.BOTTOM = ["BOTTOM",7];
jeash.display.StageAlign.BOTTOM.toString = $estr;
jeash.display.StageAlign.BOTTOM.__enum__ = jeash.display.StageAlign;
jeash.display.StageDisplayState = $hxClasses["jeash.display.StageDisplayState"] = { __ename__ : ["jeash","display","StageDisplayState"], __constructs__ : ["FULL_SCREEN","NORMAL"] }
jeash.display.StageDisplayState.FULL_SCREEN = ["FULL_SCREEN",0];
jeash.display.StageDisplayState.FULL_SCREEN.toString = $estr;
jeash.display.StageDisplayState.FULL_SCREEN.__enum__ = jeash.display.StageDisplayState;
jeash.display.StageDisplayState.NORMAL = ["NORMAL",1];
jeash.display.StageDisplayState.NORMAL.toString = $estr;
jeash.display.StageDisplayState.NORMAL.__enum__ = jeash.display.StageDisplayState;
jeash.display.StageQuality = function() { }
$hxClasses["jeash.display.StageQuality"] = jeash.display.StageQuality;
jeash.display.StageQuality.__name__ = ["jeash","display","StageQuality"];
jeash.display.StageQuality.prototype = {
	__class__: jeash.display.StageQuality
}
jeash.display.StageScaleMode = $hxClasses["jeash.display.StageScaleMode"] = { __ename__ : ["jeash","display","StageScaleMode"], __constructs__ : ["SHOW_ALL","NO_SCALE","NO_BORDER","EXACT_FIT"] }
jeash.display.StageScaleMode.SHOW_ALL = ["SHOW_ALL",0];
jeash.display.StageScaleMode.SHOW_ALL.toString = $estr;
jeash.display.StageScaleMode.SHOW_ALL.__enum__ = jeash.display.StageScaleMode;
jeash.display.StageScaleMode.NO_SCALE = ["NO_SCALE",1];
jeash.display.StageScaleMode.NO_SCALE.toString = $estr;
jeash.display.StageScaleMode.NO_SCALE.__enum__ = jeash.display.StageScaleMode;
jeash.display.StageScaleMode.NO_BORDER = ["NO_BORDER",2];
jeash.display.StageScaleMode.NO_BORDER.toString = $estr;
jeash.display.StageScaleMode.NO_BORDER.__enum__ = jeash.display.StageScaleMode;
jeash.display.StageScaleMode.EXACT_FIT = ["EXACT_FIT",3];
jeash.display.StageScaleMode.EXACT_FIT.toString = $estr;
jeash.display.StageScaleMode.EXACT_FIT.__enum__ = jeash.display.StageScaleMode;
jeash.events.Listener = function(inListener,inUseCapture,inPriority) {
	this.mListner = inListener;
	this.mUseCapture = inUseCapture;
	this.mPriority = inPriority;
	this.mID = jeash.events.Listener.sIDs++;
};
$hxClasses["jeash.events.Listener"] = jeash.events.Listener;
jeash.events.Listener.__name__ = ["jeash","events","Listener"];
jeash.events.Listener.prototype = {
	mListner: null
	,mUseCapture: null
	,mPriority: null
	,mID: null
	,Is: function(inListener,inCapture) {
		return Reflect.compareMethods(this.mListner,inListener) && this.mUseCapture == inCapture;
	}
	,dispatchEvent: function(event) {
		this.mListner(event);
	}
	,__class__: jeash.events.Listener
}
jeash.events.EventPhase = function() { }
$hxClasses["jeash.events.EventPhase"] = jeash.events.EventPhase;
jeash.events.EventPhase.__name__ = ["jeash","events","EventPhase"];
jeash.events.EventPhase.prototype = {
	__class__: jeash.events.EventPhase
}
jeash.events.FocusEvent = function(type,bubbles,cancelable,inObject,inShiftKey,inKeyCode) {
	jeash.events.Event.call(this,type,bubbles,cancelable);
	this.keyCode = inKeyCode;
	this.shiftKey = inShiftKey == null?false:inShiftKey;
	this.target = inObject;
};
$hxClasses["jeash.events.FocusEvent"] = jeash.events.FocusEvent;
jeash.events.FocusEvent.__name__ = ["jeash","events","FocusEvent"];
jeash.events.FocusEvent.__super__ = jeash.events.Event;
jeash.events.FocusEvent.prototype = $extend(jeash.events.Event.prototype,{
	keyCode: null
	,shiftKey: null
	,__class__: jeash.events.FocusEvent
});
jeash.events.HTTPStatusEvent = function(type,bubbles,cancelable,status) {
	if(status == null) status = 0;
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	this.status = status;
	jeash.events.Event.call(this,type,bubbles,cancelable);
};
$hxClasses["jeash.events.HTTPStatusEvent"] = jeash.events.HTTPStatusEvent;
jeash.events.HTTPStatusEvent.__name__ = ["jeash","events","HTTPStatusEvent"];
jeash.events.HTTPStatusEvent.__super__ = jeash.events.Event;
jeash.events.HTTPStatusEvent.prototype = $extend(jeash.events.Event.prototype,{
	status: null
	,__class__: jeash.events.HTTPStatusEvent
});
jeash.events.IOErrorEvent = function(type,bubbles,cancelable,inText) {
	if(inText == null) inText = "";
	jeash.events.Event.call(this,type,bubbles,cancelable);
	this.text = inText;
};
$hxClasses["jeash.events.IOErrorEvent"] = jeash.events.IOErrorEvent;
jeash.events.IOErrorEvent.__name__ = ["jeash","events","IOErrorEvent"];
jeash.events.IOErrorEvent.__super__ = jeash.events.Event;
jeash.events.IOErrorEvent.prototype = $extend(jeash.events.Event.prototype,{
	text: null
	,__class__: jeash.events.IOErrorEvent
});
jeash.events.KeyboardEvent = function(type,bubbles,cancelable,inCharCode,inKeyCode,inKeyLocation,inCtrlKey,inAltKey,inShiftKey) {
	jeash.events.Event.call(this,type,bubbles,cancelable);
	this.keyCode = inKeyCode;
	this.keyLocation = inKeyLocation == null?0:inKeyLocation;
	this.charCode = inCharCode == null?0:inCharCode;
	this.shiftKey = inShiftKey == null?false:inShiftKey;
	this.altKey = inAltKey == null?false:inAltKey;
	this.ctrlKey = inCtrlKey == null?false:inCtrlKey;
};
$hxClasses["jeash.events.KeyboardEvent"] = jeash.events.KeyboardEvent;
jeash.events.KeyboardEvent.__name__ = ["jeash","events","KeyboardEvent"];
jeash.events.KeyboardEvent.__super__ = jeash.events.Event;
jeash.events.KeyboardEvent.prototype = $extend(jeash.events.Event.prototype,{
	keyCode: null
	,charCode: null
	,keyLocation: null
	,ctrlKey: null
	,altKey: null
	,shiftKey: null
	,__class__: jeash.events.KeyboardEvent
});
jeash.events.ProgressEvent = function(type,bubbles,cancelable,bytesLoaded,bytesTotal) {
	if(bytesTotal == null) bytesTotal = 0;
	if(bytesLoaded == null) bytesLoaded = 0;
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	jeash.events.Event.call(this,type,bubbles,cancelable);
	this.bytesLoaded = bytesLoaded;
	this.bytesTotal = bytesTotal;
};
$hxClasses["jeash.events.ProgressEvent"] = jeash.events.ProgressEvent;
jeash.events.ProgressEvent.__name__ = ["jeash","events","ProgressEvent"];
jeash.events.ProgressEvent.__super__ = jeash.events.Event;
jeash.events.ProgressEvent.prototype = $extend(jeash.events.Event.prototype,{
	bytesLoaded: null
	,bytesTotal: null
	,__class__: jeash.events.ProgressEvent
});
jeash.filters = {}
jeash.filters.BitmapFilter = function() { }
$hxClasses["jeash.filters.BitmapFilter"] = jeash.filters.BitmapFilter;
jeash.filters.BitmapFilter.__name__ = ["jeash","filters","BitmapFilter"];
jeash.filters.BitmapFilter.prototype = {
	clone: function() {
		throw "Implement in subclass. BitmapFilter::clone";
		return null;
	}
	,jeashApplyFilter: function(surface) {
	}
	,__class__: jeash.filters.BitmapFilter
}
jeash.geom = {}
jeash.geom.ColorTransform = function(inRedMultiplier,inGreenMultiplier,inBlueMultiplier,inAlphaMultiplier,inRedOffset,inGreenOffset,inBlueOffset,inAlphaOffset) {
	this.redMultiplier = inRedMultiplier == null?1.0:inRedMultiplier;
	this.greenMultiplier = inGreenMultiplier == null?1.0:inGreenMultiplier;
	this.blueMultiplier = inBlueMultiplier == null?1.0:inBlueMultiplier;
	this.alphaMultiplier = inAlphaMultiplier == null?1.0:inAlphaMultiplier;
	this.redOffset = inRedOffset == null?0.0:inRedOffset;
	this.greenOffset = inGreenOffset == null?0.0:inGreenOffset;
	this.blueOffset = inBlueOffset == null?0.0:inBlueOffset;
	this.alphaOffset = inAlphaOffset == null?0.0:inAlphaOffset;
	this.color = 0;
};
$hxClasses["jeash.geom.ColorTransform"] = jeash.geom.ColorTransform;
jeash.geom.ColorTransform.__name__ = ["jeash","geom","ColorTransform"];
jeash.geom.ColorTransform.prototype = {
	alphaMultiplier: null
	,alphaOffset: null
	,blueMultiplier: null
	,blueOffset: null
	,color: null
	,greenMultiplier: null
	,greenOffset: null
	,redMultiplier: null
	,redOffset: null
	,__class__: jeash.geom.ColorTransform
}
jeash.geom.Matrix = function(in_a,in_b,in_c,in_d,in_tx,in_ty) {
	this.a = in_a == null?1.0:in_a;
	this.b = in_b == null?0.0:in_b;
	this.c = in_c == null?0.0:in_c;
	this.d = in_d == null?1.0:in_d;
	this.tx = in_tx == null?0.0:in_tx;
	this.ty = in_ty == null?0.0:in_ty;
};
$hxClasses["jeash.geom.Matrix"] = jeash.geom.Matrix;
jeash.geom.Matrix.__name__ = ["jeash","geom","Matrix"];
jeash.geom.Matrix.prototype = {
	a: null
	,b: null
	,c: null
	,d: null
	,tx: null
	,ty: null
	,clone: function() {
		return new jeash.geom.Matrix(this.a,this.b,this.c,this.d,this.tx,this.ty);
	}
	,invert: function() {
		var norm = this.a * this.d - this.b * this.c;
		if(norm == 0) {
			this.a = this.b = this.c = this.d = 0;
			this.tx = -this.tx;
			this.ty = -this.ty;
		} else {
			norm = 1.0 / norm;
			var a1 = this.d * norm;
			this.d = this.a * norm;
			this.a = a1;
			this.b *= -norm;
			this.c *= -norm;
			var tx1 = -this.a * this.tx - this.c * this.ty;
			this.ty = -this.b * this.tx - this.d * this.ty;
			this.tx = tx1;
		}
		return this;
	}
	,transformPoint: function(inPos) {
		return new jeash.geom.Point(inPos.x * this.a + inPos.y * this.c + this.tx,inPos.x * this.b + inPos.y * this.d + this.ty);
	}
	,translate: function(inDX,inDY) {
		this.tx += inDX;
		this.ty += inDY;
	}
	,rotate: function(inTheta) {
		var cos = Math.cos(inTheta);
		var sin = Math.sin(inTheta);
		var a1 = this.a * cos - this.b * sin;
		this.b = this.a * sin + this.b * cos;
		this.a = a1;
		var c1 = this.c * cos - this.d * sin;
		this.d = this.c * sin + this.d * cos;
		this.c = c1;
		var tx1 = this.tx * cos - this.ty * sin;
		this.ty = this.tx * sin + this.ty * cos;
		this.tx = tx1;
	}
	,concat: function(m) {
		var a1 = this.a * m.a + this.b * m.c;
		this.b = this.a * m.b + this.b * m.d;
		this.a = a1;
		var c1 = this.c * m.a + this.d * m.c;
		this.d = this.c * m.b + this.d * m.d;
		this.c = c1;
		var tx1 = this.tx * m.a + this.ty * m.c + m.tx;
		this.ty = this.tx * m.b + this.ty * m.d + m.ty;
		this.tx = tx1;
	}
	,mult: function(m) {
		var result = new jeash.geom.Matrix();
		result.a = this.a * m.a + this.b * m.c;
		result.b = this.a * m.b + this.b * m.d;
		result.c = this.c * m.a + this.d * m.c;
		result.d = this.c * m.b + this.d * m.d;
		result.tx = this.tx * m.a + this.ty * m.c + m.tx;
		result.ty = this.tx * m.b + this.ty * m.d + m.ty;
		return result;
	}
	,toMozString: function() {
		return "matrix(" + this.a.toFixed(4) + ", " + this.b.toFixed(4) + ", " + this.c.toFixed(4) + ", " + this.d.toFixed(4) + ", " + this.tx.toFixed(4) + "px, " + this.ty.toFixed(4) + "px)";
	}
	,toString: function() {
		return "matrix(" + this.a.toFixed(4) + ", " + this.b.toFixed(4) + ", " + this.c.toFixed(4) + ", " + this.d.toFixed(4) + ", " + this.tx.toFixed(4) + ", " + this.ty.toFixed(4) + ")";
	}
	,__class__: jeash.geom.Matrix
}
jeash.geom.Point = function(inX,inY) {
	this.x = inX == null?0.0:inX;
	this.y = inY == null?0.0:inY;
};
$hxClasses["jeash.geom.Point"] = jeash.geom.Point;
jeash.geom.Point.__name__ = ["jeash","geom","Point"];
jeash.geom.Point.prototype = {
	x: null
	,y: null
	,clone: function() {
		return new jeash.geom.Point(this.x,this.y);
	}
	,get_length: function() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
	,normalize: function(thickness) {
		if(this.x == 0 && this.y == 0) this.x = thickness; else {
			var norm = thickness / Math.sqrt(this.x * this.x + this.y * this.y);
			this.x *= norm;
			this.y *= norm;
		}
	}
	,__class__: jeash.geom.Point
}
jeash.geom.Rectangle = function(inX,inY,inWidth,inHeight) {
	if(inHeight == null) inHeight = 0.;
	if(inWidth == null) inWidth = 0.;
	if(inY == null) inY = 0.;
	if(inX == null) inX = 0.;
	this.x = inX;
	this.y = inY;
	this.width = inWidth;
	this.height = inHeight;
};
$hxClasses["jeash.geom.Rectangle"] = jeash.geom.Rectangle;
jeash.geom.Rectangle.__name__ = ["jeash","geom","Rectangle"];
jeash.geom.Rectangle.prototype = {
	x: null
	,y: null
	,width: null
	,height: null
	,get_left: function() {
		return this.x;
	}
	,set_left: function(l) {
		this.width -= l - this.x;
		this.x = l;
		return l;
	}
	,right: null
	,get_right: function() {
		return this.x + this.width;
	}
	,set_right: function(r) {
		this.width = r - this.x;
		return r;
	}
	,get_top: function() {
		return this.y;
	}
	,set_top: function(t) {
		this.height -= t - this.y;
		this.y = t;
		return t;
	}
	,bottom: null
	,get_bottom: function() {
		return this.y + this.height;
	}
	,set_bottom: function(b) {
		this.height = b - this.y;
		return b;
	}
	,get_topLeft: function() {
		return new jeash.geom.Point(this.x,this.y);
	}
	,set_topLeft: function(p) {
		this.x = p.x;
		this.y = p.y;
		return p.clone();
	}
	,get_size: function() {
		return new jeash.geom.Point(this.width,this.height);
	}
	,set_size: function(p) {
		this.width = p.x;
		this.height = p.y;
		return p.clone();
	}
	,get_bottomRight: function() {
		return new jeash.geom.Point(this.x + this.width,this.y + this.height);
	}
	,set_bottomRight: function(p) {
		this.width = p.x - this.x;
		this.height = p.y - this.y;
		return p.clone();
	}
	,clone: function() {
		return new jeash.geom.Rectangle(this.x,this.y,this.width,this.height);
	}
	,transform: function(m) {
		var tx0 = m.a * this.x + m.c * this.y;
		var tx1 = tx0;
		var ty0 = m.b * this.x + m.d * this.y;
		var ty1 = tx0;
		var tx = m.a * (this.x + this.width) + m.c * this.y;
		var ty = m.b * (this.x + this.width) + m.d * this.y;
		if(tx < tx0) tx0 = tx;
		if(ty < ty0) ty0 = ty;
		if(tx > tx1) tx1 = tx;
		if(ty > ty1) ty1 = ty;
		tx = m.a * (this.x + this.width) + m.c * (this.y + this.height);
		ty = m.b * (this.x + this.width) + m.d * (this.y + this.height);
		if(tx < tx0) tx0 = tx;
		if(ty < ty0) ty0 = ty;
		if(tx > tx1) tx1 = tx;
		if(ty > ty1) ty1 = ty;
		tx = m.a * this.x + m.c * (this.y + this.height);
		ty = m.b * this.x + m.d * (this.y + this.height);
		if(tx < tx0) tx0 = tx;
		if(ty < ty0) ty0 = ty;
		if(tx > tx1) tx1 = tx;
		if(ty > ty1) ty1 = ty;
		return new jeash.geom.Rectangle(tx0 + m.tx,ty0 + m.ty,tx1 - tx0,ty1 - ty0);
	}
	,extendBounds: function(r) {
		var dx = this.x - r.x;
		if(dx > 0) {
			this.x -= dx;
			this.width += dx;
		}
		var dy = this.y - r.y;
		if(dy > 0) {
			this.y -= dy;
			this.height += dy;
		}
		if(r.get_right() > this.get_right()) this.set_right(r.get_right());
		if(r.get_bottom() > this.get_bottom()) this.set_bottom(r.get_bottom());
	}
	,__class__: jeash.geom.Rectangle
	,__properties__: {set_bottom:"set_bottom",get_bottom:"get_bottom",set_right:"set_right",get_right:"get_right"}
}
jeash.geom.Transform = function(inParent) {
	this.mObj = inParent;
};
$hxClasses["jeash.geom.Transform"] = jeash.geom.Transform;
jeash.geom.Transform.__name__ = ["jeash","geom","Transform"];
jeash.geom.Transform.prototype = {
	matrix: null
	,mObj: null
	,jeashGetMatrix: function() {
		return this.mObj.jeashGetMatrix();
	}
	,jeashSetMatrix: function(inMatrix) {
		return this.mObj.jeashSetMatrix(inMatrix);
	}
	,GetPixelBounds: function() {
		return this.mObj.getBounds(jeash.Lib.jeashGetStage());
	}
	,GetColorTransform: function() {
		return new jeash.geom.ColorTransform();
	}
	,SetColorTransform: function(inColorTransform) {
		return inColorTransform;
	}
	,__class__: jeash.geom.Transform
	,__properties__: {set_matrix:"jeashSetMatrix",get_matrix:"jeashGetMatrix"}
}
jeash.geom.Vector3D = function(x,y,z,w) {
	if(w == null) w = 0.;
	if(z == null) z = 0.;
	if(y == null) y = 0.;
	if(x == null) x = 0.;
	this.w = w;
	this.x = x;
	this.y = y;
	this.z = z;
};
$hxClasses["jeash.geom.Vector3D"] = jeash.geom.Vector3D;
jeash.geom.Vector3D.__name__ = ["jeash","geom","Vector3D"];
jeash.geom.Vector3D.distance = function(pt1,pt2) {
	var x = pt2.x - pt1.x;
	var y = pt2.y - pt1.y;
	var z = pt2.z - pt1.z;
	return Math.sqrt(x * x + y * y + z * z);
}
jeash.geom.Vector3D.getX_AXIS = function() {
	return new jeash.geom.Vector3D(1,0,0);
}
jeash.geom.Vector3D.getY_AXIS = function() {
	return new jeash.geom.Vector3D(0,1,0);
}
jeash.geom.Vector3D.getZ_AXIS = function() {
	return new jeash.geom.Vector3D(0,0,1);
}
jeash.geom.Vector3D.prototype = {
	length: null
	,getLength: function() {
		return Math.abs(jeash.geom.Vector3D.distance(this,new jeash.geom.Vector3D()));
	}
	,getLengthSquared: function() {
		return Math.abs(jeash.geom.Vector3D.distance(this,new jeash.geom.Vector3D())) * Math.abs(jeash.geom.Vector3D.distance(this,new jeash.geom.Vector3D()));
	}
	,w: null
	,x: null
	,y: null
	,z: null
	,__class__: jeash.geom.Vector3D
	,__properties__: {get_length:"getLength"}
}
jeash.media = {}
jeash.media.SoundChannel = function() { }
$hxClasses["jeash.media.SoundChannel"] = jeash.media.SoundChannel;
jeash.media.SoundChannel.__name__ = ["jeash","media","SoundChannel"];
jeash.media.SoundChannel.__super__ = jeash.events.EventDispatcher;
jeash.media.SoundChannel.prototype = $extend(jeash.events.EventDispatcher.prototype,{
	soundTransform: null
	,__setSoundTransform: function(v) {
		return this.soundTransform = v;
	}
	,__class__: jeash.media.SoundChannel
	,__properties__: {set_soundTransform:"__setSoundTransform"}
});
jeash.net = {}
jeash.net.URLLoader = function() { }
$hxClasses["jeash.net.URLLoader"] = jeash.net.URLLoader;
jeash.net.URLLoader.__name__ = ["jeash","net","URLLoader"];
jeash.net.URLLoader.__super__ = jeash.events.EventDispatcher;
jeash.net.URLLoader.prototype = $extend(jeash.events.EventDispatcher.prototype,{
	data: null
	,dataFormat: null
	,load: function(request) {
		if(request.contentType == null) {
			switch( (this.dataFormat)[1] ) {
			case 0:
				request.requestHeaders.push(new jeash.net.URLRequestHeader("Content-Type","application/octet-stream"));
				break;
			default:
				request.requestHeaders.push(new jeash.net.URLRequestHeader("Content-Type","application/x-www-form-urlencoded"));
			}
		} else request.requestHeaders.push(new jeash.net.URLRequestHeader("Content-Type",request.contentType));
		this.requestUrl(request.url,request.method,request.data,request.requestHeaders);
	}
	,onData: function(_) {
		var content = this.getData();
		if(Std["is"](content,ArrayBuffer)) this.data = jeash.utils.ByteArray.jeashOfBuffer(content); else this.data = Std.string(content);
		var evt = new jeash.events.Event(jeash.events.Event.COMPLETE);
		evt.currentTarget = this;
		this.dispatchEvent(evt);
	}
	,onError: function(msg) {
		var evt = new jeash.events.IOErrorEvent(jeash.events.IOErrorEvent.IO_ERROR);
		evt.text = msg;
		evt.currentTarget = this;
		this.dispatchEvent(evt);
	}
	,onOpen: function() {
		var evt = new jeash.events.Event(jeash.events.Event.OPEN);
		evt.currentTarget = this;
		this.dispatchEvent(evt);
	}
	,onStatus: function(status) {
		var evt = new jeash.events.HTTPStatusEvent(jeash.events.HTTPStatusEvent.HTTP_STATUS,false,false,status);
		evt.currentTarget = this;
		this.dispatchEvent(evt);
	}
	,onProgress: function(event) {
		var evt = new jeash.events.ProgressEvent(jeash.events.ProgressEvent.PROGRESS);
		evt.currentTarget = this;
		evt.bytesLoaded = event.loaded;
		evt.bytesTotal = event.total;
		this.dispatchEvent(evt);
	}
	,registerEvents: function(subject) {
		var self = this;
		if(typeof XMLHttpRequestProgressEvent != "undefined") subject.addEventListener("progress",this.onProgress.$bind(this),false);
		subject.onreadystatechange = function() {
			if(subject.readyState != 4) return;
			var s = (function($this) {
				var $r;
				try {
					$r = subject.status;
				} catch( e ) {
					$r = null;
				}
				return $r;
			}(this));
			if(s == undefined) s = null;
			if(s != null) self.onStatus(s);
			if(s != null && s >= 200 && s < 400) self.onData(subject.response); else switch(s) {
			case null: case undefined:
				self.onError("Failed to connect or resolve host");
				break;
			case 12029:
				self.onError("Failed to connect to host");
				break;
			case 12007:
				self.onError("Unknown host");
				break;
			default:
				self.onError("Http Error #" + subject.status);
			}
		};
	}
	,requestUrl: function(url,method,data,requestHeaders) {
		var xmlHttpRequest = new XMLHttpRequest();
		this.registerEvents(xmlHttpRequest);
		var uri = "";
		switch(true) {
		case Std["is"](data,jeash.utils.ByteArray):
			var data1 = data;
			switch( (this.dataFormat)[1] ) {
			case 0:
				uri = data1.jeashGetBuffer();
				break;
			default:
				uri = data1.readUTFBytes(data1.length);
			}
			break;
		case Std["is"](data,Dynamic):
			var data1 = data;
			var _g = 0, _g1 = Reflect.fields(data1);
			while(_g < _g1.length) {
				var p = _g1[_g];
				++_g;
				if(uri.length != 0) uri += "&";
				uri += StringTools.urlEncode(p) + "=" + StringTools.urlEncode(Reflect.field(data1,p));
			}
			break;
		default:
			if(data != null) uri = data.toString();
		}
		try {
			if(method == "GET" && uri != null) {
				var question = url.split("?").length <= 1;
				xmlHttpRequest.open(method,url + (question?"?":"&") + uri,true);
				uri = "";
			} else xmlHttpRequest.open(method,url,true);
		} catch( e ) {
			this.onError(e.toString());
			return;
		}
		switch( (this.dataFormat)[1] ) {
		case 0:
			xmlHttpRequest.responseType = "arraybuffer";
			break;
		default:
		}
		var _g = 0;
		while(_g < requestHeaders.length) {
			var header = requestHeaders[_g];
			++_g;
			xmlHttpRequest.setRequestHeader(header.name,header.value);
		}
		xmlHttpRequest.send(uri);
		this.onOpen();
		this.getData = function() {
			return xmlHttpRequest.response;
		};
	}
	,getData: function() {
	}
	,__class__: jeash.net.URLLoader
});
jeash.net.URLLoaderDataFormat = $hxClasses["jeash.net.URLLoaderDataFormat"] = { __ename__ : ["jeash","net","URLLoaderDataFormat"], __constructs__ : ["BINARY","TEXT","VARIABLES"] }
jeash.net.URLLoaderDataFormat.BINARY = ["BINARY",0];
jeash.net.URLLoaderDataFormat.BINARY.toString = $estr;
jeash.net.URLLoaderDataFormat.BINARY.__enum__ = jeash.net.URLLoaderDataFormat;
jeash.net.URLLoaderDataFormat.TEXT = ["TEXT",1];
jeash.net.URLLoaderDataFormat.TEXT.toString = $estr;
jeash.net.URLLoaderDataFormat.TEXT.__enum__ = jeash.net.URLLoaderDataFormat;
jeash.net.URLLoaderDataFormat.VARIABLES = ["VARIABLES",2];
jeash.net.URLLoaderDataFormat.VARIABLES.toString = $estr;
jeash.net.URLLoaderDataFormat.VARIABLES.__enum__ = jeash.net.URLLoaderDataFormat;
jeash.net.URLRequest = function(inURL) {
	if(inURL != null) this.url = inURL;
	this.requestHeaders = [];
};
$hxClasses["jeash.net.URLRequest"] = jeash.net.URLRequest;
jeash.net.URLRequest.__name__ = ["jeash","net","URLRequest"];
jeash.net.URLRequest.prototype = {
	url: null
	,requestHeaders: null
	,method: null
	,data: null
	,contentType: null
	,__class__: jeash.net.URLRequest
}
jeash.net.URLRequestHeader = function(name,value) {
	this.name = name;
	this.value = value;
};
$hxClasses["jeash.net.URLRequestHeader"] = jeash.net.URLRequestHeader;
jeash.net.URLRequestHeader.__name__ = ["jeash","net","URLRequestHeader"];
jeash.net.URLRequestHeader.prototype = {
	name: null
	,value: null
	,__class__: jeash.net.URLRequestHeader
}
jeash.text = {}
jeash.text.Font = function() { }
$hxClasses["jeash.text.Font"] = jeash.text.Font;
jeash.text.Font.__name__ = ["jeash","text","Font"];
jeash.text.Font.jeashFontData = null;
jeash.text.Font.jeashOfResource = function(name) {
	var data = haxe.Resource.getString(name);
	if(data == null) throw "Resource data for string '" + name + "' not found.";
	jeash.text.Font.jeashFontData[name] = haxe.Resource.getString(name);
}
jeash.text.Font.prototype = {
	fontName: null
	,jeashGlyphData: null
	,jeashSetFontName: function(name) {
		this.fontName = name;
		if(jeash.text.Font.jeashFontData[this.fontName] == null) try {
			jeash.text.Font.jeashOfResource(name);
		} catch( e ) {
			jeash.Lib.trace("Glyph data for font '" + name + "' does not exist, defaulting to '" + "Bitstream_Vera_Sans" + "'.");
			this.fontName = "Bitstream_Vera_Sans";
		}
		this.jeashGlyphData = haxe.Unserializer.run(jeash.text.Font.jeashFontData[this.fontName]);
		return name;
	}
	,__class__: jeash.text.Font
	,__properties__: {set_fontName:"jeashSetFontName"}
}
jeash.text.FontStyle = $hxClasses["jeash.text.FontStyle"] = { __ename__ : ["jeash","text","FontStyle"], __constructs__ : ["REGULAR","ITALIC","BOLD_ITALIC","BOLD"] }
jeash.text.FontStyle.REGULAR = ["REGULAR",0];
jeash.text.FontStyle.REGULAR.toString = $estr;
jeash.text.FontStyle.REGULAR.__enum__ = jeash.text.FontStyle;
jeash.text.FontStyle.ITALIC = ["ITALIC",1];
jeash.text.FontStyle.ITALIC.toString = $estr;
jeash.text.FontStyle.ITALIC.__enum__ = jeash.text.FontStyle;
jeash.text.FontStyle.BOLD_ITALIC = ["BOLD_ITALIC",2];
jeash.text.FontStyle.BOLD_ITALIC.toString = $estr;
jeash.text.FontStyle.BOLD_ITALIC.__enum__ = jeash.text.FontStyle;
jeash.text.FontStyle.BOLD = ["BOLD",3];
jeash.text.FontStyle.BOLD.toString = $estr;
jeash.text.FontStyle.BOLD.__enum__ = jeash.text.FontStyle;
jeash.text.FontType = $hxClasses["jeash.text.FontType"] = { __ename__ : ["jeash","text","FontType"], __constructs__ : ["EMBEDDED","DEVICE"] }
jeash.text.FontType.EMBEDDED = ["EMBEDDED",0];
jeash.text.FontType.EMBEDDED.toString = $estr;
jeash.text.FontType.EMBEDDED.__enum__ = jeash.text.FontType;
jeash.text.FontType.DEVICE = ["DEVICE",1];
jeash.text.FontType.DEVICE.toString = $estr;
jeash.text.FontType.DEVICE.__enum__ = jeash.text.FontType;
jeash.text.TextFormatAlign = $hxClasses["jeash.text.TextFormatAlign"] = { __ename__ : ["jeash","text","TextFormatAlign"], __constructs__ : ["LEFT","RIGHT","JUSTIFY","CENTER"] }
jeash.text.TextFormatAlign.LEFT = ["LEFT",0];
jeash.text.TextFormatAlign.LEFT.toString = $estr;
jeash.text.TextFormatAlign.LEFT.__enum__ = jeash.text.TextFormatAlign;
jeash.text.TextFormatAlign.RIGHT = ["RIGHT",1];
jeash.text.TextFormatAlign.RIGHT.toString = $estr;
jeash.text.TextFormatAlign.RIGHT.__enum__ = jeash.text.TextFormatAlign;
jeash.text.TextFormatAlign.JUSTIFY = ["JUSTIFY",2];
jeash.text.TextFormatAlign.JUSTIFY.toString = $estr;
jeash.text.TextFormatAlign.JUSTIFY.__enum__ = jeash.text.TextFormatAlign;
jeash.text.TextFormatAlign.CENTER = ["CENTER",3];
jeash.text.TextFormatAlign.CENTER.toString = $estr;
jeash.text.TextFormatAlign.CENTER.__enum__ = jeash.text.TextFormatAlign;
jeash.ui = {}
jeash.ui.Keyboard = function() { }
$hxClasses["jeash.ui.Keyboard"] = jeash.ui.Keyboard;
jeash.ui.Keyboard.__name__ = ["jeash","ui","Keyboard"];
jeash.ui.Keyboard.jeashConvertWebkitCode = function(code) {
	switch(code.toLowerCase()) {
	case "backspace":
		return jeash.ui.Keyboard.BACKSPACE;
	case "tab":
		return jeash.ui.Keyboard.TAB;
	case "enter":
		return jeash.ui.Keyboard.ENTER;
	case "shift":
		return jeash.ui.Keyboard.SHIFT;
	case "control":
		return jeash.ui.Keyboard.CONTROL;
	case "capslock":
		return jeash.ui.Keyboard.CAPS_LOCK;
	case "escape":
		return jeash.ui.Keyboard.ESCAPE;
	case "space":
		return jeash.ui.Keyboard.SPACE;
	case "pageup":
		return jeash.ui.Keyboard.PAGE_UP;
	case "pagedown":
		return jeash.ui.Keyboard.PAGE_DOWN;
	case "end":
		return jeash.ui.Keyboard.END;
	case "home":
		return jeash.ui.Keyboard.HOME;
	case "left":
		return jeash.ui.Keyboard.LEFT;
	case "right":
		return jeash.ui.Keyboard.RIGHT;
	case "up":
		return jeash.ui.Keyboard.UP;
	case "down":
		return jeash.ui.Keyboard.DOWN;
	case "insert":
		return jeash.ui.Keyboard.INSERT;
	case "delete":
		return jeash.ui.Keyboard.DELETE;
	case "numlock":
		return jeash.ui.Keyboard.NUMLOCK;
	case "break":
		return jeash.ui.Keyboard.BREAK;
	}
	if(code.indexOf("U+") == 0) return Std.parseInt("0x" + code.substr(3));
	throw "Unrecognised key code: " + code;
	return 0;
}
jeash.ui.Keyboard.jeashConvertMozillaCode = function(code) {
	switch(code) {
	case jeash.ui.Keyboard.DOM_VK_BACK_SPACE:
		return jeash.ui.Keyboard.BACKSPACE;
	case jeash.ui.Keyboard.DOM_VK_TAB:
		return jeash.ui.Keyboard.TAB;
	case jeash.ui.Keyboard.DOM_VK_RETURN:
		return jeash.ui.Keyboard.ENTER;
	case jeash.ui.Keyboard.DOM_VK_ENTER:
		return jeash.ui.Keyboard.ENTER;
	case jeash.ui.Keyboard.DOM_VK_SHIFT:
		return jeash.ui.Keyboard.SHIFT;
	case jeash.ui.Keyboard.DOM_VK_CONTROL:
		return jeash.ui.Keyboard.CONTROL;
	case jeash.ui.Keyboard.DOM_VK_CAPS_LOCK:
		return jeash.ui.Keyboard.CAPS_LOCK;
	case jeash.ui.Keyboard.DOM_VK_ESCAPE:
		return jeash.ui.Keyboard.ESCAPE;
	case jeash.ui.Keyboard.DOM_VK_SPACE:
		return jeash.ui.Keyboard.SPACE;
	case jeash.ui.Keyboard.DOM_VK_PAGE_UP:
		return jeash.ui.Keyboard.PAGE_UP;
	case jeash.ui.Keyboard.DOM_VK_PAGE_DOWN:
		return jeash.ui.Keyboard.PAGE_DOWN;
	case jeash.ui.Keyboard.DOM_VK_END:
		return jeash.ui.Keyboard.END;
	case jeash.ui.Keyboard.DOM_VK_HOME:
		return jeash.ui.Keyboard.HOME;
	case jeash.ui.Keyboard.DOM_VK_LEFT:
		return jeash.ui.Keyboard.LEFT;
	case jeash.ui.Keyboard.DOM_VK_RIGHT:
		return jeash.ui.Keyboard.RIGHT;
	case jeash.ui.Keyboard.DOM_VK_UP:
		return jeash.ui.Keyboard.UP;
	case jeash.ui.Keyboard.DOM_VK_DOWN:
		return jeash.ui.Keyboard.DOWN;
	case jeash.ui.Keyboard.DOM_VK_INSERT:
		return jeash.ui.Keyboard.INSERT;
	case jeash.ui.Keyboard.DOM_VK_DELETE:
		return jeash.ui.Keyboard.DELETE;
	case jeash.ui.Keyboard.DOM_VK_NUM_LOCK:
		return jeash.ui.Keyboard.NUMLOCK;
	default:
		return code;
	}
}
jeash.ui.Keyboard.prototype = {
	__class__: jeash.ui.Keyboard
}
jeash.utils = {}
jeash.utils.ByteArray = function(len) {
	if(len == null) len = 8192;
	this.position = 0;
	this.length = 0;
	var buffer = new ArrayBuffer(len);
	this.data = new DataView(buffer);
	this.byteView = new Uint8Array(buffer);
	this.bigEndian = false;
};
$hxClasses["jeash.utils.ByteArray"] = jeash.utils.ByteArray;
jeash.utils.ByteArray.__name__ = ["jeash","utils","ByteArray"];
jeash.utils.ByteArray.jeashOfBuffer = function(buffer) {
	var bytes = new jeash.utils.ByteArray(buffer.byteLength);
	bytes.data = new DataView(buffer);
	bytes.byteView = new Uint8Array(buffer);
	return bytes;
}
jeash.utils.ByteArray.prototype = {
	data: null
	,byteView: null
	,bigEndian: null
	,position: null
	,length: null
	,jeashGetBytesAvailable: function() {
		return this.length - this.position;
	}
	,jeashResizeBuffer: function(len) {
		var initLength = this.byteView.length;
		var resized = new Uint8Array(len);
		resized.set(this.byteView);
		this.data = new DataView(resized.buffer);
		this.byteView = resized;
	}
	,readUTFBytes: function(len) {
		var value = "";
		var fcc = String.fromCharCode;
		var max = this.position + len;
		if(max >= this.byteView.length) this.jeashResizeBuffer(max);
		while(this.position < max) {
			var c = this.data.getUint8(this.position++);
			if(c < 128) {
				if(c == 0) break;
				value += fcc(c);
			} else if(c < 224) value += fcc((c & 63) << 6 | this.data.getUint8(this.position++) & 127); else if(c < 240) {
				var c2 = this.data.getUint8(this.position++);
				value += fcc((c & 31) << 12 | (c2 & 127) << 6 | this.data.getUint8(this.position++) & 127);
			} else {
				var c2 = this.data.getUint8(this.position++);
				var c3 = this.data.getUint8(this.position++);
				value += fcc((c & 15) << 18 | (c2 & 127) << 12 | c3 << 6 & 127 | this.data.getUint8(this.position++) & 127);
			}
		}
		return value;
	}
	,jeashGetEndian: function() {
		if(this.bigEndian == true) return jeash.utils.Endian.BIG_ENDIAN; else return jeash.utils.Endian.LITTLE_ENDIAN;
	}
	,jeashSetEndian: function(endian) {
		if(endian == jeash.utils.Endian.BIG_ENDIAN) this.bigEndian = true; else this.bigEndian = false;
		return endian;
	}
	,jeashGetBuffer: function() {
		return this.data.buffer;
	}
	,__class__: jeash.utils.ByteArray
}
jeash.utils.Endian = $hxClasses["jeash.utils.Endian"] = { __ename__ : ["jeash","utils","Endian"], __constructs__ : ["BIG_ENDIAN","LITTLE_ENDIAN"] }
jeash.utils.Endian.BIG_ENDIAN = ["BIG_ENDIAN",0];
jeash.utils.Endian.BIG_ENDIAN.toString = $estr;
jeash.utils.Endian.BIG_ENDIAN.__enum__ = jeash.utils.Endian;
jeash.utils.Endian.LITTLE_ENDIAN = ["LITTLE_ENDIAN",1];
jeash.utils.Endian.LITTLE_ENDIAN.toString = $estr;
jeash.utils.Endian.LITTLE_ENDIAN.__enum__ = jeash.utils.Endian;
var js = {}
js.Boot = function() { }
$hxClasses["js.Boot"] = js.Boot;
js.Boot.__name__ = ["js","Boot"];
js.Boot.__unhtml = function(s) {
	return s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
}
js.Boot.__trace = function(v,i) {
	var msg = i != null?i.fileName + ":" + i.lineNumber + ": ":"";
	msg += js.Boot.__string_rec(v,"");
	var d = document.getElementById("haxe:trace");
	if(d != null) d.innerHTML += js.Boot.__unhtml(msg) + "<br/>"; else if(typeof(console) != "undefined" && console.log != null) console.log(msg);
}
js.Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ != null || o.__ename__ != null)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__ != null) {
				if(o.length == 2) return o[0];
				var str = o[0] + "(";
				s += "\t";
				var _g1 = 2, _g = o.length;
				while(_g1 < _g) {
					var i = _g1++;
					if(i != 2) str += "," + js.Boot.__string_rec(o[i],s); else str += js.Boot.__string_rec(o[i],s);
				}
				return str + ")";
			}
			var l = o.length;
			var i;
			var str = "[";
			s += "\t";
			var _g = 0;
			while(_g < l) {
				var i1 = _g++;
				str += (i1 > 0?",":"") + js.Boot.__string_rec(o[i1],s);
			}
			str += "]";
			return str;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			return "???";
		}
		if(tostr != null && tostr != Object.toString) {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) { ;
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js.Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
}
js.Boot.__interfLoop = function(cc,cl) {
	if(cc == null) return false;
	if(cc == cl) return true;
	var intf = cc.__interfaces__;
	if(intf != null) {
		var _g1 = 0, _g = intf.length;
		while(_g1 < _g) {
			var i = _g1++;
			var i1 = intf[i];
			if(i1 == cl || js.Boot.__interfLoop(i1,cl)) return true;
		}
	}
	return js.Boot.__interfLoop(cc.__super__,cl);
}
js.Boot.__instanceof = function(o,cl) {
	try {
		if(o instanceof cl) {
			if(cl == Array) return o.__enum__ == null;
			return true;
		}
		if(js.Boot.__interfLoop(o.__class__,cl)) return true;
	} catch( e ) {
		if(cl == null) return false;
	}
	switch(cl) {
	case Int:
		return Math.ceil(o%2147483648.0) === o;
	case Float:
		return typeof(o) == "number";
	case Bool:
		return o === true || o === false;
	case String:
		return typeof(o) == "string";
	case Dynamic:
		return true;
	default:
		if(o == null) return false;
		return o.__enum__ == cl || cl == Class && o.__name__ != null || cl == Enum && o.__ename__ != null;
	}
}
js.Boot.__init = function() {
	js.Lib.isIE = typeof document!='undefined' && document.all != null && typeof window!='undefined' && window.opera == null;
	js.Lib.isOpera = typeof window!='undefined' && window.opera != null;
	Array.prototype.copy = Array.prototype.slice;
	Array.prototype.insert = function(i,x) {
		this.splice(i,0,x);
	};
	Array.prototype.remove = Array.prototype.indexOf?function(obj) {
		var idx = this.indexOf(obj);
		if(idx == -1) return false;
		this.splice(idx,1);
		return true;
	}:function(obj) {
		var i = 0;
		var l = this.length;
		while(i < l) {
			if(this[i] == obj) {
				this.splice(i,1);
				return true;
			}
			i++;
		}
		return false;
	};
	Array.prototype.iterator = function() {
		return { cur : 0, arr : this, hasNext : function() {
			return this.cur < this.arr.length;
		}, next : function() {
			return this.arr[this.cur++];
		}};
	};
	if(String.prototype.cca == null) String.prototype.cca = String.prototype.charCodeAt;
	String.prototype.charCodeAt = function(i) {
		var x = this.cca(i);
		if(x != x) return undefined;
		return x;
	};
	var oldsub = String.prototype.substr;
	String.prototype.substr = function(pos,len) {
		if(pos != null && pos != 0 && len != null && len < 0) return "";
		if(len == null) len = this.length;
		if(pos < 0) {
			pos = this.length + pos;
			if(pos < 0) pos = 0;
		} else if(len < 0) len = this.length + len - pos;
		return oldsub.apply(this,[pos,len]);
	};
	Function.prototype["$bind"] = function(o) {
		var f = function() {
			return f.method.apply(f.scope,arguments);
		};
		f.scope = o;
		f.method = this;
		return f;
	};
}
js.Boot.prototype = {
	__class__: js.Boot
}
js.Lib = function() { }
$hxClasses["js.Lib"] = js.Lib;
js.Lib.__name__ = ["js","Lib"];
js.Lib.isIE = null;
js.Lib.isOpera = null;
js.Lib.document = null;
js.Lib.window = null;
js.Lib.prototype = {
	__class__: js.Lib
}
var nme = {}
nme.installer = {}
nme.installer.Assets = function() { }
$hxClasses["nme.installer.Assets"] = nme.installer.Assets;
nme.installer.Assets.__name__ = ["nme","installer","Assets"];
nme.installer.Assets.getBitmapData = function(id,useCache) {
	if(useCache == null) useCache = true;
	switch(id) {
	case "img/ship.png":
		return ((function($this) {
			var $r;
			var $t = ApplicationMain.loaders.get("img/ship.png").contentLoaderInfo.content;
			if(Std["is"]($t,jeash.display.Bitmap)) $t; else throw "Class cast error";
			$r = $t;
			return $r;
		}(this))).bitmapData;
	}
	return null;
}
nme.installer.Assets.prototype = {
	__class__: nme.installer.Assets
}
js.Boot.__res = {}
js.Boot.__init();
{
	var d = Date;
	d.now = function() {
		return new Date();
	};
	d.fromTime = function(t) {
		var d1 = new Date();
		d1["setTime"](t);
		return d1;
	};
	d.fromString = function(s) {
		switch(s.length) {
		case 8:
			var k = s.split(":");
			var d1 = new Date();
			d1["setTime"](0);
			d1["setUTCHours"](k[0]);
			d1["setUTCMinutes"](k[1]);
			d1["setUTCSeconds"](k[2]);
			return d1;
		case 10:
			var k = s.split("-");
			return new Date(k[0],k[1] - 1,k[2],0,0,0);
		case 19:
			var k = s.split(" ");
			var y = k[0].split("-");
			var t = k[1].split(":");
			return new Date(y[0],y[1] - 1,y[2],t[0],t[1],t[2]);
		default:
			throw "Invalid date format : " + s;
		}
	};
	d.prototype["toString"] = function() {
		var date = this;
		var m = date.getMonth() + 1;
		var d1 = date.getDate();
		var h = date.getHours();
		var mi = date.getMinutes();
		var s = date.getSeconds();
		return date.getFullYear() + "-" + (m < 10?"0" + m:"" + m) + "-" + (d1 < 10?"0" + d1:"" + d1) + " " + (h < 10?"0" + h:"" + h) + ":" + (mi < 10?"0" + mi:"" + mi) + ":" + (s < 10?"0" + s:"" + s);
	};
	d.prototype.__class__ = $hxClasses["Date"] = d;
	d.__name__ = ["Date"];
}
{
	Math.__name__ = ["Math"];
	Math.NaN = Number["NaN"];
	Math.NEGATIVE_INFINITY = Number["NEGATIVE_INFINITY"];
	Math.POSITIVE_INFINITY = Number["POSITIVE_INFINITY"];
	$hxClasses["Math"] = Math;
	Math.isFinite = function(i) {
		return isFinite(i);
	};
	Math.isNaN = function(i) {
		return isNaN(i);
	};
}
{
	String.prototype.__class__ = $hxClasses["String"] = String;
	String.__name__ = ["String"];
	Array.prototype.__class__ = $hxClasses["Array"] = Array;
	Array.__name__ = ["Array"];
	var Int = $hxClasses["Int"] = { __name__ : ["Int"]};
	var Dynamic = $hxClasses["Dynamic"] = { __name__ : ["Dynamic"]};
	var Float = $hxClasses["Float"] = Number;
	Float.__name__ = ["Float"];
	var Bool = $hxClasses["Bool"] = Boolean;
	Bool.__ename__ = ["Bool"];
	var Class = $hxClasses["Class"] = { __name__ : ["Class"]};
	var Enum = { };
	var Void = $hxClasses["Void"] = { __ename__ : ["Void"]};
}
{
	Xml.Element = "element";
	Xml.PCData = "pcdata";
	Xml.CData = "cdata";
	Xml.Comment = "comment";
	Xml.DocType = "doctype";
	Xml.Prolog = "prolog";
	Xml.Document = "document";
}
haxe.Resource.content = [];
{
	if(typeof document != "undefined") js.Lib.document = document;
	if(typeof window != "undefined") {
		js.Lib.window = window;
		js.Lib.window.onerror = function(msg,url,line) {
			var f = js.Lib.onerror;
			if(f == null) return false;
			return f(msg,[url + ":" + line]);
		};
	}
}
js["XMLHttpRequest"] = window.XMLHttpRequest?XMLHttpRequest:window.ActiveXObject?function() {
	try {
		return new ActiveXObject("Msxml2.XMLHTTP");
	} catch( e ) {
		try {
			return new ActiveXObject("Microsoft.XMLHTTP");
		} catch( e1 ) {
			throw "Unable to create XMLHttpRequest object.";
		}
	}
}:(function($this) {
	var $r;
	throw "Unable to create XMLHttpRequest object.";
	return $r;
}(this));
jeash.display.DisplayObject.mNameID = 0;
com.damir.haxteroids.AsteroidManager.startHeight = 80;
com.damir.haxteroids.AsteroidManager.startWidth = 80;
com.damir.haxteroids.Bullet.speed = 4.0;
com.damir.haxteroids.InputState.keyState = new Array();
com.damir.haxteroids.Ship.rotationSpeed = 5.0 * Math.PI / 180.0;
com.damir.haxteroids.Ship.maxVelocity = 200.0;
com.damir.haxteroids.Ship.friction = 0.2;
haxe.Unserializer.DEFAULT_RESOLVER = Type;
haxe.Unserializer.BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";
haxe.Unserializer.CODES = null;
jeash.Lib.VENDOR_HTML_TAG = "data-";
jeash.Lib.HTML_DIV_EVENT_TYPES = ["resize","mouseup","mouseover","mouseout","mousemove","mousedown","mousewheel","dblclick","click"];
jeash.Lib.HTML_WINDOW_EVENT_TYPES = ["keyup","keypress","keydown","resize"];
jeash.Lib.HTML_TOUCH_EVENT_TYPES = ["touchstart","touchmove","touchend"];
jeash.Lib.JEASH_IDENTIFIER = "haxe:jeash";
jeash.Lib.DEFAULT_WIDTH = 500;
jeash.Lib.DEFAULT_HEIGHT = 500;
jeash.display.BitmapData.mNameID = 0;
jeash.display.Graphics.RADIAL = 1;
jeash.display.Graphics.END_NONE = 0;
jeash.display.Graphics.END_ROUND = 256;
jeash.display.Graphics.END_SQUARE = 512;
jeash.display.Graphics.CORNER_ROUND = 0;
jeash.display.Graphics.CORNER_MITER = 4096;
jeash.display.Graphics.CORNER_BEVEL = 8192;
jeash.display.Graphics.PIXEL_HINTING = 16384;
jeash.display.Graphics.SCALE_NONE = 0;
jeash.display.Graphics.SCALE_VERTICAL = 1;
jeash.display.Graphics.SCALE_HORIZONTAL = 2;
jeash.display.Graphics.SCALE_NORMAL = 3;
jeash.display.Graphics.MOVE = 0;
jeash.display.Graphics.LINE = 1;
jeash.display.Graphics.CURVE = 2;
jeash.display.Graphics.JEASH_MAX_DIMENSION = 5000;
jeash.events.Event.ACTIVATE = "activate";
jeash.events.Event.ADDED = "added";
jeash.events.Event.ADDED_TO_STAGE = "addedToStage";
jeash.events.Event.COMPLETE = "complete";
jeash.events.Event.ENTER_FRAME = "enterFrame";
jeash.events.Event.OPEN = "open";
jeash.events.Event.REMOVED = "removed";
jeash.events.Event.REMOVED_FROM_STAGE = "removedFromStage";
jeash.events.Event.RENDER = "render";
jeash.events.Event.RESIZE = "resize";
jeash.events.MouseEvent.CLICK = "click";
jeash.events.MouseEvent.DOUBLE_CLICK = "doubleClick";
jeash.events.MouseEvent.MOUSE_DOWN = "mouseDown";
jeash.events.MouseEvent.MOUSE_MOVE = "mouseMove";
jeash.events.MouseEvent.MOUSE_OUT = "mouseOut";
jeash.events.MouseEvent.MOUSE_OVER = "mouseOver";
jeash.events.MouseEvent.MOUSE_UP = "mouseUp";
jeash.events.MouseEvent.MOUSE_WHEEL = "mouseWheel";
jeash.events.MouseEvent.ROLL_OUT = "rollOut";
jeash.events.MouseEvent.ROLL_OVER = "rollOver";
jeash.events.TouchEvent.TOUCH_BEGIN = "touchBegin";
jeash.events.TouchEvent.TOUCH_END = "touchEnd";
jeash.events.TouchEvent.TOUCH_MOVE = "touchMove";
jeash.events.TouchEvent.TOUCH_OUT = "touchOut";
jeash.events.TouchEvent.TOUCH_OVER = "touchOver";
jeash.events.TouchEvent.TOUCH_ROLL_OUT = "touchRollOut";
jeash.events.TouchEvent.TOUCH_ROLL_OVER = "touchRollOver";
jeash.display.Stage.jeashMouseChanges = [jeash.events.MouseEvent.MOUSE_OUT,jeash.events.MouseEvent.MOUSE_OVER,jeash.events.MouseEvent.ROLL_OUT,jeash.events.MouseEvent.ROLL_OVER];
jeash.display.Stage.jeashTouchChanges = [jeash.events.TouchEvent.TOUCH_OUT,jeash.events.TouchEvent.TOUCH_OVER,jeash.events.TouchEvent.TOUCH_ROLL_OUT,jeash.events.TouchEvent.TOUCH_ROLL_OVER];
jeash.display.Stage.DEFAULT_FRAMERATE = 60.0;
jeash.display.Stage.UI_EVENTS_QUEUE_MAX = 1000;
jeash.display.StageQuality.BEST = "best";
jeash.events.Listener.sIDs = 1;
jeash.events.EventPhase.CAPTURING_PHASE = 0;
jeash.events.EventPhase.AT_TARGET = 1;
jeash.events.EventPhase.BUBBLING_PHASE = 2;
jeash.events.FocusEvent.FOCUS_IN = "FOCUS_IN";
jeash.events.FocusEvent.FOCUS_OUT = "FOCUS_OUT";
jeash.events.HTTPStatusEvent.HTTP_STATUS = "httpStatus";
jeash.events.IOErrorEvent.IO_ERROR = "IO_ERROR";
jeash.events.KeyboardEvent.KEY_DOWN = "KEY_DOWN";
jeash.events.KeyboardEvent.KEY_UP = "KEY_UP";
jeash.events.ProgressEvent.PROGRESS = "progress";
jeash.text.Font.DEFAULT_FONT_NAME = "Bitstream_Vera_Sans";
jeash.ui.Keyboard.A = 65;
jeash.ui.Keyboard.BACKSPACE = 8;
jeash.ui.Keyboard.TAB = 9;
jeash.ui.Keyboard.ENTER = 13;
jeash.ui.Keyboard.SHIFT = 16;
jeash.ui.Keyboard.CONTROL = 17;
jeash.ui.Keyboard.CAPS_LOCK = 18;
jeash.ui.Keyboard.ESCAPE = 27;
jeash.ui.Keyboard.SPACE = 32;
jeash.ui.Keyboard.PAGE_UP = 33;
jeash.ui.Keyboard.PAGE_DOWN = 34;
jeash.ui.Keyboard.END = 35;
jeash.ui.Keyboard.HOME = 36;
jeash.ui.Keyboard.LEFT = 37;
jeash.ui.Keyboard.RIGHT = 39;
jeash.ui.Keyboard.UP = 38;
jeash.ui.Keyboard.DOWN = 40;
jeash.ui.Keyboard.INSERT = 45;
jeash.ui.Keyboard.DELETE = 46;
jeash.ui.Keyboard.NUMLOCK = 144;
jeash.ui.Keyboard.BREAK = 19;
jeash.ui.Keyboard.DOM_VK_BACK_SPACE = 8;
jeash.ui.Keyboard.DOM_VK_TAB = 9;
jeash.ui.Keyboard.DOM_VK_RETURN = 13;
jeash.ui.Keyboard.DOM_VK_ENTER = 14;
jeash.ui.Keyboard.DOM_VK_SHIFT = 16;
jeash.ui.Keyboard.DOM_VK_CONTROL = 17;
jeash.ui.Keyboard.DOM_VK_CAPS_LOCK = 20;
jeash.ui.Keyboard.DOM_VK_ESCAPE = 27;
jeash.ui.Keyboard.DOM_VK_SPACE = 32;
jeash.ui.Keyboard.DOM_VK_PAGE_UP = 33;
jeash.ui.Keyboard.DOM_VK_PAGE_DOWN = 34;
jeash.ui.Keyboard.DOM_VK_END = 35;
jeash.ui.Keyboard.DOM_VK_HOME = 36;
jeash.ui.Keyboard.DOM_VK_LEFT = 37;
jeash.ui.Keyboard.DOM_VK_UP = 38;
jeash.ui.Keyboard.DOM_VK_RIGHT = 39;
jeash.ui.Keyboard.DOM_VK_DOWN = 40;
jeash.ui.Keyboard.DOM_VK_INSERT = 45;
jeash.ui.Keyboard.DOM_VK_DELETE = 46;
jeash.ui.Keyboard.DOM_VK_NUM_LOCK = 144;
jeash.utils.ByteArray.BYTE_ARRAY_BUFFER_SIZE = 8192;
js.Lib.onerror = null;
ApplicationMain.main();
})()
//@ sourceMappingURL=HaXteroids.js.map