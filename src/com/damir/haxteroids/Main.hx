package com.damir.haxteroids;

import haxe.Log;
import nme.display.Sprite;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.Lib;
import nme.ui.Keyboard;

/**
 * ...
 * @author Damir Veapi
 */

class Main extends Sprite 
{
	
	public function new() 
	{
		super();
		#if iphone
		Lib.current.stage.addEventListener(Event.RESIZE, init);
		#else
		addEventListener(Event.ADDED_TO_STAGE, init);
		//addEventListener(Event.RESIZE, onResize);
		#end
	}

	var ship : Ship;
	var asteroidManager : AsteroidManager;
	
	private function init(e) 
	{
		// entry point
		ship = new Ship(stage.stageWidth / 2, stage.stageHeight / 2);
		asteroidManager = new AsteroidManager(1, this);
		
		addChild(ship);
		//addChild(asteroid);
		
		Lib.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		Lib.current.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		
		addEventListener(Event.ENTER_FRAME, update);
		
	}
	
	static public function main() 
	{
		Log.setColor(0xffffff);
		var stage = Lib.current.stage;
		stage.scaleMode = nme.display.StageScaleMode.NO_SCALE;
		stage.align = nme.display.StageAlign.TOP_LEFT;
		
		Lib.current.addChild(new Main());
		
		trace("main");
	}
	
	private var deltaTime:Int;
	private var previousTime:Int;
	
	private function update(e):Void 
	{
		deltaTime = Lib.getTimer() - previousTime;
		previousTime += deltaTime;
		
		ship.update(deltaTime);
		asteroidManager.update();
		
		//collisions
		asteroidManager.hitTest(ship);
	}
	
	private function onKeyUp(e:KeyboardEvent):Void 
	{
		InputState.keyState[e.keyCode] = false;
	}
	
	private function onKeyDown(e:KeyboardEvent):Void 
	{
		trace("KeyCode: " + e.keyCode);
		
		InputState.keyState[e.keyCode] = true;
	}

	
	private function onResize(e)
	{

	}
	
	
}
