package com.damir.haxteroids;

import nme.Assets;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.events.Event;
import nme.geom.Matrix;
import nme.geom.Point;
import nme.geom.Transform;
import nme.geom.Vector3D;
import nme.Lib;
import nme.ui.Keyboard;


/**
 * ...
 * @author Damir Veapi
 */

class Ship extends Sprite
{
	private var sprite : Bitmap;
	
	private var velocity : Vector3D;
	private var acceleration : Float;
	
	public var bullet : Bullet;
	
	private static var rotationSpeed = 5.0*Math.PI/180.0;
	private static var maxVelocity = 200.0;
	private static var maxAcceleration = 5.0;
	private static var friction = 0.2;
	
//private var transformMatrix : Matrix;
	private var mat:Matrix;

	public function new(x: Float, y:Float)
	{
		super();
		
		sprite = new Bitmap(Assets.getBitmapData("img/ship.png"));
		
		velocity = new Vector3D();
		acceleration = 0.0;
		
		bullet = new Bullet();
		addEventListener(Event.ADDED_TO_STAGE, onAdded);
		
		this.x = x;
		this.y = y;
		
		trace("X: " + x + " this.x: " + this.x);
		
		addChild(sprite);
		sprite.x = sprite.width / -2;
		sprite.y = sprite.height / -2;
		
		mat = new Matrix(1, 0, 0, 1, x, y);
		
		//mat.translate(x, y);
		
		transform.matrix = mat;
	}
	
	private function onAdded(e:Event):Void 
	{
		this.parent.addChild(bullet);
		removeEventListener(Event.ADDED_TO_STAGE, onAdded);
	}
	
	public function turnLeft()
	{
		mat.translate(-x, -y);
		mat.rotate(-rotationSpeed);
		mat.translate(x, y);
		
		transform.matrix = mat;
	}
	
	public function turnRight()
	{
		mat.translate(-x, -y);
		mat.rotate(rotationSpeed);
		mat.translate(x, y);
		
		transform.matrix = mat;
	}
	
	public function accelerate() 
	{	
		acceleration = -2;
		trace("Accel: " + acceleration);
	}
	
	public function decelerate() 
	{
		acceleration = 1;
	}
	
	public function update(deltaTime:Int) 
	{
		acceleration = 0;
		
		handleInput();
		
		var deltaTimeInSeconds = deltaTime / 1000.0;
		
		//velocity.y += acceleration;
	/*	trace("Accel: " + acceleration);
		if (velocity.y != 0) {
			trace("Pre-friction: " + velocity.y);
			velocity.y -= Util.sign(velocity.y) * friction;
			trace("Post-friction: " + velocity.y);			
		}*/
		//velocity.subtract(Vector3D(0, friction));
		
		//var xVel = velocity.x * mat.a + velocity.y * mat.c;
		//var yVel = velocity.x * mat.b + velocity.y * mat.d;
/*		trace("Accel: " + acceleration);
		trace("AccelX: " + acceleration * mat.c);
		trace("AccelY: " + acceleration * mat.d);
		trace("Fric: " + Math.min((velocity.y - 0.1) * 10, 0));*/
		velocity.x += (acceleration * mat.c);
		if (velocity.x != 0) {
			velocity.x -= Util.sign(velocity.x) * friction;
		}
		
		velocity.y += (acceleration * mat.d);
		if (velocity.y != 0) {
			velocity.y -= Util.sign(velocity.y) * friction;
		}
		//velocity.length = Math.min(Math.abs(velocity.length), maxVelocity) * Util.sign(velocity.y);
		
		if (velocity.length > maxVelocity)
		{
			velocity.x = Util.sign(velocity.x) * maxVelocity * mat.b;
			velocity.y = Util.sign(velocity.y) * maxVelocity * mat.d;
		}
		//trace("Velocity: " + velocity.toString());
		
		mat.translate(velocity.x*deltaTimeInSeconds, velocity.y*deltaTimeInSeconds);
		
		if (mat.tx < 0) {
			mat.tx = stage.stageWidth;
		} else if (mat.tx > stage.stageWidth) {
			mat.tx = 0;
		}
		
		if (mat.ty < 0) {
			mat.ty = stage.stageHeight;
		} else if (mat.ty > stage.stageHeight) {
			mat.ty = 0;
		}
		
		transform.matrix = mat;
		//x += velocity.x*deltaTimeInSeconds;
		//y += velocity.y*deltaTimeInSeconds;
		
		if (bullet.visible) {
			bullet.update();
		}
	}
	
	private function fire()
	{
		if (!bullet.visible) {
			var direction = new Point(-mat.c, -mat.d);
			bullet.fire(x - width/2 * mat.c, y - height/2 * mat.d, direction);
		}
	}
	
	private function handleInput() 
	{
		if(InputState.keyState[Keyboard.LEFT]) {
			turnLeft();
		}
			
		if(InputState.keyState[Keyboard.RIGHT]) {
			turnRight();
		}
				
		if(InputState.keyState[Keyboard.UP]) {
			accelerate();
		}
				
		if(InputState.keyState[Keyboard.DOWN]) {
			decelerate();
		}
		
		if (InputState.keyState[Keyboard.ENTER]) {
			trace("ENTER");
			fire();
		}
	}
	
}