package com.damir.haxteroids;
import nme.display.Sprite;
import nme.geom.Vector3D;
import nme.ui.Keyboard;

/**
 * ...
 * @author Damir Veapi
 */

class Asteroid extends Sprite
{
	public var asteroidWidth: Float;
	public var asteroidHeight: Float;
	
	private var velocity : Vector3D;
	private var rotationSpeed : Float;
	
	public var area : Float;
	
	public var manager : AsteroidManager;
	
	public function new(x:Float, y:Float, width: Float = 10, height: Float = 10) 
	{
		super();
		
		graphics.beginFill(0xffffff);
		graphics.drawRect( -width / 2, -height / 2, width, height);
		/*graphics.endFill();
		graphics.moveTo( -width / 2, -height / 2);
		graphics.lineTo( -width / 2, height / 2);
		graphics.lineTo( width / 2, height / 2);
		graphics.lineTo( width / 2, -height / 2);
		graphics.lineTo( -width / 2, -height / 2);*/
		
		this.x = x;
		this.y = y;
		
		asteroidWidth = width;
		asteroidHeight = height;
		
		velocity = new Vector3D(Math.random() * 6 - 3, Math.random() * 6 - 3);
		rotationSpeed = Math.random() * 4 - 2;
	}
	
	public function update()
	{		
		handleInput();

		x += velocity.x;
		y += velocity.y;
		rotation = (rotation + rotationSpeed) % 360;
		
		// If the object has been deleted from screen return
		if(parent == null)
			return;
			
		//Otherwise, screen wrap
		if (x < 0) {
			x = stage.stageWidth;
		} else if (x > stage.stageWidth) {
			x = 0;
		}
		
		if (y < 0) {
			y = stage.stageHeight;
		} else if (y > stage.stageHeight) {
			y = 0;
		}
	}
	
	private function handleInput() 
	{
		if (InputState.keyState[Keyboard.A])
		{
			destroy();
		}
	}
	
	private function destroy() 
	{
		manager.destroy(this);
		//AsteroidFactory.destroy(this):
	}
	
}