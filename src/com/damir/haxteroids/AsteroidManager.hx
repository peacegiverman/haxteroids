package com.damir.haxteroids;
import com.damir.haxteroids.Ship;
import nme.display.DisplayObjectContainer;
import nme.display.Sprite;
import nme.Lib;

/**
 * ...
 * @author Damir Veapi
 */

class AsteroidManager 
{
	private var asteroids : Array<Asteroid>;
	
	private static var startHeight : Float = 80;
	private static var startWidth : Float = 80;
	
	private var screen : DisplayObjectContainer;
	public function new(count : Int, container : DisplayObjectContainer) 
	{
		asteroids = new Array<Asteroid>();
		
		screen = container;
		
		for (i in 0...count) {
			asteroids.push(new Asteroid(
				Math.random() * Lib.current.stage.stageWidth,
				Math.random() * Lib.current.stage.stageHeight,
				startHeight,
				startWidth
				)
			);
			
			asteroids[i].manager = this;
			
			screen.addChild(asteroids[i]);
		}
	}
	
	public function update() 
	{
		var length = asteroids.length;
		
		for(i in 0...length) {
			asteroids[i].update();
		}
	}
	
	public function destroy(asteroid : Asteroid)
	{
		//var screen : DisplayObjectContainer = asteroid.parent;
		asteroids.remove(asteroid);
		screen.removeChild(asteroid);
		
		if (asteroid.asteroidWidth < 40 || asteroid.asteroidHeight < 40) {
			return;
		}
		
		asteroids.push(new Asteroid(
			asteroid.x + 10, 
			asteroid.y + 10, 
			Math.max(asteroid.asteroidWidth, asteroid.asteroidHeight) / 2, 
			Math.min(asteroid.asteroidWidth, asteroid.asteroidHeight)
			)
		);
		asteroids.push(new Asteroid(
			asteroid.x - 10, 
			asteroid.y - 10, 
			Math.min(asteroid.asteroidWidth, asteroid.asteroidHeight), 
			Math.max(asteroid.asteroidWidth, asteroid.asteroidHeight) / 2
			)
		);
		
		asteroids[asteroids.length - 2].manager = this;
		screen.addChild(asteroids[asteroids.length - 2]);
		
		asteroids[asteroids.length - 1].manager = this;
		screen.addChild(asteroids[asteroids.length - 1]);
	}
	
	public function hitTest(ship : Ship) 
	{
		for(asteroid in asteroids)
		{
			if (ship.bullet.visible && ship.bullet.hitTestObject(asteroid)) {
				ship.bullet.destroy();
				destroy(asteroid);
			}
			if (ship.hitTestObject(asteroid)) {
				trace("HIT");
			}
		}
	}
}