package com.damir.haxteroids;

import nme.display.Sprite;
import nme.geom.Point;
import nme.geom.Vector3D;
import nme.Lib;

/**
 * ...
 * @author Damir Veapi
 */

class Bullet extends Sprite
{
	private var velocity : Point;
	
	private static var speed : Float = 4.0;
	
	public function new(x: Float = 0, y: Float = 0, ?direction : Point)
	{
		super();
		
		this.x = x;
		this.y = y;
		
		graphics.beginFill(0xff0000);
		graphics.drawRect( -1.5, -4, 3, 8);
		
		if(direction != null) {
			velocity = direction;
			velocity.normalize(speed);
		} else {
			velocity = new Point();
		}
		
		visible = false;
	}
	
	public function update()
	{
		x += velocity.x;
		y += velocity.y;
		
		if (x < 0 || x > stage.stageWidth ||
			y < 0 || y > stage.stageHeight) 
		{
			visible = false;
		}
	}
	
	public function fire(x: Float = 0, y: Float = 0, direction : Point)
	{	
		this.x = x;
		this.y = y;
		
		velocity = direction;
		velocity.normalize(speed);
		
		if (velocity.x != 0) {
			trace("rotation " + Math.acos(1 / velocity.x));
			rotation = (Math.atan2(velocity.y, velocity.x) * 180 / Math.PI) + 90;
		} else {
			rotation = 0;
		}
		visible = true;
		trace("FIRE");
	}
	
	public function destroy()
	{
		visible = false;
	}
	
}